# Intro #

Universe is a Storytelling application and Real Time Strategy browser game.

![aristaeus-36-alpha.png](https://bitbucket.org/repo/BaaKB5/images/3201560497-aristaeus-36-alpha.png)

In the game you can create your own solar system and write stories about it in a logbook specifically tailored to chronological storytelling. At the same time you can develop your home planet's colony by building buildings and star-bases. These will lead you on to build ships to colonize your own solar system and reach beyond them to the stars.

A more [detailed description can be found here](https://olddutchman.wordpress.com/2016/02/07/universe-intro/), while the game's instructions and [full range of options will be available here]().

Note: This application is a multi-host application and runs on NODEJS and LAMP, and therefore is most suitable being hosted on any Linux system.

# Installation #

## [DOWNLOAD THE VIRTUALBOX-READY IMAGE](https://db.tt/YeaNaXhj) ##

You can install the game by downloading and running the pre-prepared virtualbox image. Instructions for [installing virtualbox here](http://www.wikihow.com/Install-VirtualBox). Instructions on how to [import the virtualbox-ready image can be found here](https://www.maketecheasier.com/import-export-ova-files-in-virtualbox/)

## SET UP YOUR CLIENT ##

__- You need to edit all your hosts files.__

If you are running the virtualbox or installing this  locally and not hosting it on an actual hosting platform, you will need to point your hosts file to the right places. Your hosts file is located (in windows:) in..

    C:\Windows\System32\drivers\etc\

Get the IP address from the virtualbox. You'll need to log in. __Your username as well as all passwords are 'universe'.__

    universe@universe:~/$ ifconfig

    eth0      Link encap:Ethernet  HWaddr 08:00:27:57:9e:a7
          __inet addr:192.168.0.20__  Bcast:192.168.0.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe57:9ea7/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:2400425 errors:0 dropped:0 overruns:0 frame:0
          TX packets:2029016 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000
          RX bytes:321885648 (321.8 MB)  TX bytes:603224023 (603.2 MB)

Put this in your hosts file (replace xxx's with your IP address):

    xxx.xxx.xxx.xxx    universe dev.universe api.universe worker.universe 

And then you're done! Navigate to __dev.universe__ and start playing your game!

## SET IT UP YOURSELF ##

You can also set everything up yourself from source. Universe is extremely scalable. Each part of the application needs its own host to function properly and each can be scaled, considering the mysql database is at the center of the application. More info on the set-up of the application [here](http://olddutchman.wordpress.com/)


The actual web-application consists of 5 parts:
  - a mysql database
  - a worker
  - a socket
  - an api
  - a frontend www service to display the data for the user.

You need some knowledge of server management, editing config files, setting up apache services to get through the following. 

    Server:
      - 1 cpu
      - 1024mb memory
      - Ubuntu Server with LAMP services installed.
    Client:
      - Any WebGL enabled browser
 

### Pre-Installation Notes: ###
  
Below are some tips for the installation and their corresponding commands on an ubuntu distro.

__- Make sure your OS has LAMP installed and ready.__

    tasksel

__- Update your OS.__

    sudo apt-get update
    sudo apt-get dist-upgrade

__- Make sure you've installed LAMP, nodejs and git__

    sudo apt-get install nodejs npm git

__- Make sure you have enabled mod_rewrite for apache2.__
    
    sudo a2enmod rewrite
    sudo service apache2 restart

### Installation Steps ###

__- Clone the game into a directory__

    cd <to>/<project>/<dir>
    git --git-dir=/dev/null clone https://cgraamans@bitbucket.org/cgraamans/universe.git
    rm -rf <to>/<project>/<dir>/universe/.git

__- Make a database with a user__

    mysql -uroot -p
    CREATE USER 'universe'@'localhost' IDENTIFIED BY 'password';
    CREATE DATABASE universe;
    GRANT ALL PRIVILEGES ON universe.* TO universe;
    quit

__- Import universe.init.sql and universe.data.sql into that database__

    cd <to>/<project>/<dir>/universe
    mysql -uroot -p universe < init/sql/universe.init.sql
    mysql -uroot -p universe < init/sql/universe.data.sql

__- Set up apache.__

You will need to point each part of the application to their respective hosts:
  
    www.universe.com --> /www/
    api.universe.com --> /api/
    worker.universe.com --> /worker/

    NOTE:
    You can separate the socket service and the texture service out. See _www/assets/js/opts.js_.

There is an example apache2 file included in the repository for testing and home hosting purposes;

    sudo cp /<to>/<project>/<dir>/universe/init/apache2/001-universe.conf /etc/apache2/sites-available
    sudo ln -s /etc/apache2/sites-available/001-universe.conf /etc/apache2/sites-enabled/001-universe.conf
    sudo ln -s /<to>/<project>/<dir>/universe/www /var/www/universe.api
    sudo ln -s /<to>/<project>/<dir>/universe/www /var/www/universe.worker
    sudo service apache2 restart

__- Set up the application.__

Edit the following files and grant them database access
	
1) _api/nodejs/lib/options.js_:

	  sql: {
	  	  host:'localhost',
	      user:'databaseuser',
		  password:'databaseuserpassword',
		  database:'databasename'
	  },

2) _api/application/config/database.php_ and _worker/application/config/database.php_:

    $db['default'] = array(
      'dsn' => 'db',
      'hostname' => 'localhost',
      'username' => 'databaseuser',
      'password' => 'databaseuserpassword',
      'database' => 'databasename',
      'dbdriver' => 'mysqli',
      'dbprefix' => '',
      'pconnect' => FALSE,
      'db_debug' => (ENVIRONMENT !== 'production'),
      'cache_on' => FALSE,
      'cachedir' => '',
      'char_set' => 'utf8',
      'dbcollat' => 'utf8_general_ci',
      'swap_pre' => '',
      'encrypt' => FALSE,
      'compress' => FALSE,
      'stricton' => FALSE,
      'failover' => array(),
      'save_queries' => TRUE
    );

3) Set up the application base urls in _www/application/config/config.php_ and _api/application/config/config.php_ and _worker/application/config/config.php_

    $config['base_url'] = 'http://www.universe';
    $config['base_url'] = 'http://api.universe';
    $config['base_url'] = 'http://worker.universe';

4) Edit the location variables in _www/assets/js/opts.js_ to suit your needs. Note that changes to the websocket port and location have to be mirrored in _api/nodejs/app.js_

    loc: {
      api:'http://api.universe',
      websocket:'http://api.universe:8080',
      textures:'http://api.universe/data/textures/',
    },

5) Install the NodeJS required packages

    cd <to>/<project>/<dir>/universe/api/nodejs
    npm install

__- Start up the worker and socket services.__

There are two upstart configuration files included: universe-socket and universe-worker. To install these copy them to your /etc/init directory and edit them. Change all paths to suit your needs.

    sudo cp /<to>/<your>/<project>/universe/init/upstart/*.conf /etc/init/
    sudo service universe-worker start
    sudo service universe-socket start

You can also run these manually.

Socket:

    cd /<to>/<your>/<project>/universe/api/nodejs
    nodejs app.js

Worker:

    cd /<to>/<your>/<project>/universe/worker/
    php index.php worker/run

# Troubleshooting #

## Virtualbox ##

Getting the virtualbox working can be a pain. Make sure you...
  
  - Try setting your networking adapter to 'bridged' to give it a separate address on the router.
  - Make sure your hosts file (On Windows: C:\WINDOWS\System32\drivers\etc\hosts) is pointing to the right URL if you are running this locally.

More information on setting up a programming environment can be found [here](http://olddutchman.wordpress.com/), for those interested or seeking help.

## The Application ##

# License #

The MIT License (MIT)

Copyright (c) 2015 H.C.Graamans

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.