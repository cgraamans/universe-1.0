<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Main extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		if (is_object($this->user_data) ) {

			// redirect('/node/'.$this->user_data->home);
			
		}

        $this->template_data['js']['pre'][] = '<script src="/assets/js/app/main.js"></script>';
		$this->template_data['content']['main'] = $this->load->view('page/main','',true);
		
		// Load Template
		$this->load->view('template',$this->template_data);

  	}

  	public function logout() {

      delete_cookie('user');

      if ($this->session->has_userdata('user_data') !== false) {
      	$this->session->unset_userdata('user_data');
      }
      redirect('/');
  	}

}
