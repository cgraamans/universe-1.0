<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class System extends MY_Controller {


	public function __construct() {

		parent::__construct();

		// SHADERS & TEMPLATES
		$this->template_data['templates'][] = $this->load->view('page/shaders/tmpl-shaders','',TRUE);

	}

	public function index()	{

		$this->template_data['content']['main'] = $this->load->view('page/system','',TRUE);

		// apps
		$this->template_data['js']['pre'][] = '<script src="/assets/js/app/system.js"></script>';
		
		// Load Template
		$this->load->view('template',$this->template_data);

	}


}
