<?php 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
/* Variables */

  protected $template_data = array(
    'title'=>"Universe - The Game",
    'modals'=>array(),
    'content'=>array(
      'header'=>'',
      'main'=>'',
      'footer'=>''
    ),
    'js'=>array(
      'pre'=>array(),
      'post'=>array()
    ),
    'css'=>array(),
    'templates'=>array()
  );

  protected $user_data = array();
  
  /* 

  Constructor 

  */
  public function __construct() {

    parent::__construct();

    // IP and Ban check
    $this->userIP = $this->_get_ip($this->input->ip_address());
    if ($this->userIP === false) {

      $this->session->set_flashdata('error_type', 'IP Address Lookup');
      $this->session->set_flashdata('error', 'No valid IP address detected for your client.');

      redirect('error');

    } else {
      
      $cookie_user = get_cookie('user');
      if ($cookie_user != NULL) {

        $this->user_data = json_decode($cookie_user);
        $this->session->set_userdata('user_data',$this->user_data);

      }

      $this->template_data['templates'][] = $this->load->view('page/mustache/tmpl-mustache','',TRUE);
      $this->template_data['modals'][] = $this->load->view('page/modals/modal-process','',true);

    }


    if ($this->session->has_userdata('user_data') !== false) {
      $this->user_data = $this->session->userdata('user_data');

      $this->template_data['css'][] = '/assets/css/nav.css';
      $this->template_data['content']['header'] = $this->load->view('page/nav/nav-auth',array('name'=>$this->user_data->name),true);
      $this->template_data['templates'][] = $this->load->view('page/mustache/tmpl-mustache-user','',TRUE);

      $this->template_data['js']['pre'][] = '<script src="/assets/js/app/user.js"></script>';


    } else {

      $this->template_data['content']['header'] = $this->load->view('page/nav/nav','',true);
      $this->template_data['modals'][] = $this->load->view('page/modals/modal-login','',true);
      $this->template_data['modals'][] = $this->load->view('page/modals/modal-register','',true);
      $this->template_data['js']['pre'][] = '<script src="/assets/js/app/auth.js"></script>';

    }
  
  } // __CONSTRUCT
  
  /*
    Function:
      get IP address (even if invalid)

  */
  private function _get_ip() {

    $ip = $this->input->ip_address();
    if ($ip == "0.0.0.0") {
      $ip = $_SERVER['REMOTE_ADDR'];

    }

    return $ip;
  }
  
} // CLASS

?>