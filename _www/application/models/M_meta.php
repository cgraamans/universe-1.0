<?php

Class M_meta extends CI_Model {

/* Meta Functions */

  public function getBan($ip,$uid=false) {

    $return = false;

    if (($uid !== false) && (is_numeric($uid))) {

      $this->db->select('id,insdate,reason');
      $this->db->from('bans');
      $this->db->where('user_id',$uid);

      $query = $this->db->get();
      if($query->num_rows() > 0) {
        
        $res = $query->result();
        $return = $res[0];

      }

    } else {


      $whereArr = array();

      $host = gethostbyaddr($ip);
      if ($host !== false) {

        $whereArr[] = "'$host' LIKE CONCAT('%', hostname)";
        $whereArr[] = "hostname = '$host'";
      
      }

      $whereArr[] = "'$ip' LIKE CONCAT(ip, '%')";
      $whereArr[] = "ip = '$ip'";

      $where = "";
      foreach($whereArr as $key=>$val) {
        if ($key == 0) {
          $where .= 'WHERE ';
        } else {
          $where .= 'OR ';
        }

        $where .= $val . " ";

      }

      $sql = "SELECT ip, hostname, insdate, reason
        FROM bans
        $where
        ORDER BY insdate DESC
      ";
            
      $query = $this->db->query($sql);
      
      if ($query->num_rows() > 0) {

        $res = $query->result();
        $return = $res[0];

      }
      
    }

    return $return;

  }

  public function __destruct() {  
      $this->db->close();  
  }  

// END CLASS
}

?>