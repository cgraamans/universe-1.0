<div id="main" class="default-page">
	<div class="cover box">

		<img src="/assets/img/logo.png" />
		<a class="scrollTo scrollToFloating" data-to="main-content" href="#main-content"><i class="fa fa-chevron-down"></i></a>

	</div>
	<div id="main-content" class="box box1">
		<div class="row">
			<div class="col-md-3">
				<img src="/assets/img/icons/quote.png" style="width:100%">
			</div>
			<div class="col-md-9">

				<h3 style="font-family:ACapt;">Universe - The Game</h3>

				Universe - The game aims to be a vehicle for your own scifi experience in storytelling and adventure. <br><br>

				Solar systems are created for the you to tell tall tales about. Events will happen regularly and keep the solar system developing (comets, anomalies, bandits, scallywags.. other players), creating a vibrant bit of space you can call your own for your imagination and storytelling.<br><br>
		
				When you are ready to see more you can create buildings and ships on your home plane and start colonizing nearby systems to generate more income and upgrading their your ships with some FTL drives.<br><br>
	
				Once FTL drives have been bolted your the ships, exploration of whole new systems becomes possible through cluster ‘points’, which are the closest stars. Exploration of one of these can lead you to a brand new solar system or perhaps even to a new cluster! Beware of pirates, though!

			</div>
		</div>
		<div class="row" style="height:100%">
			<div class="col-md-5">
			</div>
			<div class="col-md-1">
				<a class="scrollTo" data-to="main-content-vid"><i class="fa fa-youtube-play"></i></a>
			</div>
			<div class="col-md-1">
				<a class="scrollTo" data-to="main-content" href="#main-content"><i class="fa fa-chevron-down"></i></a>
			</div>
			<div class="col-md-1">
				<a class="scrollTo" data-to="main-content" href="#main-content"><i class="fa fa-chevron-down"></i></a>
			</div>
			<div class="col-md-4">
			</div>
		</div>
	</div>
	<div id="main-content-vid" class="box box2">
		lalalala
		<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	</div>


	<!--			<iframe width="560" height="315" src="https://www.youtube.com/embed/VlznMz_sYHY" frameborder="0" allowfullscreen></iframe> -->
