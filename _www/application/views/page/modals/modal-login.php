<div class="modal fade" id="modal-login" role="dialog" aria-labelledby="modal-login-label">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-body">

		      	<h2 id="modal-login-label" class='pull-left'>Login</h2>
		        <button type="button" class="close pull-right" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        
		        <div class='clearfix'></div>
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-10 main-box-register">

								<form id="login">
									<div class="form-group row">
										<label for="reg_name" class="col-sm-2 control-label">Name</label>
										<div class="col-sm-5">
											<input type="text" class="form-control" name="name" id="reg_name" placeholder="Your Name">
										</div>
									</div>
									<div class="form-group row">
										<label for="reg_password" class="col-sm-2 control-label">Password</label>
										<div class="col-sm-5">
											<input type="password" class="form-control" name="password" id="reg_password" placeholder="Password">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-offset-2 col-sm-5">
											<button type="submit" class="btn btn-primary">Sign Up</button>
										</div>
									</div>				
								</form>

							</div>
						</div>
					</div>
				</div>
				       
      		</div>
    	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->