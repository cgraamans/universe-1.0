<div class="modal fade" id="modal-process" role="dialog" aria-labelledby="modal-process-label">
	<div class="modal-dialog">
		<div class="modal-content">
      		<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-2">
								<img src="/assets/img/icons/preloader.gif" />
							</div>
							<div class="col-md-10" id="main-box-process-content">
								<h3 id="modal-process-label"></h3>
								<span id='modal-process-text'></span>
							</div>
						</div>
					</div>
				</div>
				       
      		</div>
    	</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->