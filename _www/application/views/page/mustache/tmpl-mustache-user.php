<!-- CHAT MENU -->

<script id="tmpl-chat-line" type="x-tmpl-mustache">
	<div class="chat-line-selector" data-user="{{u}}">
		{{ddt.h}}:{{ddt.m}}:{{ddt.s}} &lt;{{u}}&gt; {{m}}
	</div>
</script>

<script id="tmpl-chat-button" type="x-tmpl-mustache">
	<a id="chat-menu-toggle"><i class="fa fa-comments-o"></i></a>
</script>

<script id="tmpl-chat-container" type="x-tmpl-mustache">
	<div id="chat-box" class="inline-topnav">
		<div id="chat-box-nav">
		    <ul class="nav nav-tabs">
				{{#channels}}
					<li {{active}}><a data-toggle="tab" href="#chat-channel-{{name}}" title="{{name}} Chat"><i class="fa fa-dot-circle-o"></i></a></li>
				{{/channels}}
		    </ul>
	    </div>
	    <div class="tab-content chat-content">
			{{#channels}}

		        <div id="chat-channel-{{name}}" class="chat-box-channel tab-pane fade in active ">
	    
		        	{{#log}}
		        		{{ddt.h}}:{{ddt.m}}:{{ddt.s}} &lt;{{u}}&gt; {{m}}<br>
		        	{{/log}}
		
		        </div>

			{{/channels}}
			<form id="chat-form">
				<input type='text'/>
			</form>
	    </div>
		

	</div>
</script>

<!-- NAVS --> 

<script id="tmpl-u-nav-box" type="x-tmpl-mustache">
	<div id="{{id}}" class="inline-topnav">
		<div id="{{id}}-data">

		</div>
	</div>
</script>

<script id="tmpl-u-nav-nodes-data" type="x-tmpl-mustache">
	<div class="row" style="margin-bottom:5px;">
		<div class="col-md-12">

			<a href="/node/{{stub}}" title="{{name}}">
				<div class="row">
					<div class="col-md-4">
						<img src="{{texture}}" title="{{name}}" style="border-radius:5px;"/>
					</div>
					<div class="col-md-8" style="font-size:2em;font-family:ACapt;">
						{{name}}
					</div>
				</div>
			</a>

		</div>
	</div>
</script>

<script id="tmpl-u-nav-stats-data" type="x-tmpl-mustache">

	<div class="row" style="margin-bottom:5px;">
		<div class="col-md-4">
			<img src="{{texture}}" title="{{title}}" style="border-radius:5px;"/>
		</div>
		<div class="col-md-4" style="font-size:1.5em;">
			{{title}}
		</div>
		<div class="col-md-4">
			{{value}}
		</div>				
	</div>

</script>

<script id="tmpl-u-nav-stats-credits" type="x-tmpl-mustache">
	 <a id="nav-stats-toggle">{{xp}} <span class="" style="font-family:ACapt;font-size:0.9em;">xp</span> - {{credits}} <i class="fa fa-credit-card"></i></a>
</script>


<!-- STORYBOARD ACTIONS -->


<script id="tmpl-u-storyboard-edit" type="x-tmpl-mustache">

	<form class="storyboard-form-txt storyboard-form-edit" data-id="{{id}}">

		<div class="row">

			<div class="col-md-9">
			
				<textarea>{{text}}</textarea>
				<button type="submit" class="btn btn-primary btn-xs">Submit your story</button>
			
			</div>
			<div class="col-md-3">

				<span class="storyboard-form-info">

					The Storyboard is meant as a log of your system, a place where you can write your own stories.<br>
					<ul>
						<li>Every <strong>word</strong> you write will give you credits.</li>
						<li>Posting is limited to <strong>once every 5 minutes</strong>.</li>
						<li>You can submit up to <strong>10000</strong> characters a time.</li>
						<li>You can use <a class='show-markdown'>markdown</a>.</li>
					</ul>

				</span>

			</div>

		</div>

	</form>

</script>


<script id="tmpl-u-storyboard-add" type="x-tmpl-mustache">

	<form id="storyboard-form-add" class="storyboard-form-txt">

		<div class="row">

			<div class="col-md-9">
					
					<textarea placeholder="Writing is an exploration. You start from nothing and learn as you go."></textarea>
					<button type="submit" class="btn btn-primary">Submit your story</button>
			
			</div>
			<div class="col-md-3">
				<span class="storyboard-form-info">
					The Storyboard is meant as a log of your system, a place where you can write your own stories.<br>
					<ul>
						<li>Every word you write will give you credits,</li>
						<li>Posting is limited to <strong>once every 5 minutes</strong>.</li>
						<li>You can submit up to <strong>10000</strong> characters a time.</li>
						<li>You can use <a class='show-markdown'>markdown</a>.</li>
					</ul>
				</span>
			</div>
		</div>

	</form>


</script>

<!-- ACTIONS MENU -->
<script id="tmpl-u-actions" type="x-tmpl-mustache">

	<div id="action-box-{{identifyer}}" class="system-menu-left">
		<div class="action-box"></div>
	</div>	

</script>

<script id="tmpl-u-actions-box-btn" type="x-tmpl-mustache">

	<a data-trigger="{{identifyer}}" data-extended="false" class="action-box-btn" style="top:{{offset}}"><i class="fa {{icon}}"></i></a>

</script>

<script id="tmpl-u-actions-box-node-settings" type="x-tmpl-mustache">

	<div class="row">
		<div class="col-md-12">
			<h2 style="color:#FFF;font-family:ACapt;font-size:4em;">Settings</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="font-size:0.8em;">
			Change the name of your planet and upload your own textures. You can also revert the changes back to their defaults if you don't like what you've created. 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" style="border-bottom:3px solid #23527c;text-align:center;font-size:0.7em;margin-bottom:20px;padding-top:10px;">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a class="action-collapser" style="cursor:pointer;font-size:1.5em;" data-tgt="action-change-node-name"><i class="fa fa-pencil"></i> Rename</a><br>
			<small style="display:block;color:#23527c;margin-left:25px;">Rename your planet</small>
		</div>
	</div>

	<div class="row" id="action-change-node-name" style="display:none;">
		<div class="col-md-12">

			<div class="row" style="margin-top:20px";>
				<div class="col-md-12">

					<div class="row">

						<form id="action-node-rename">	
							<div class="col-md-10">
								<input type="text" style="border-radius:3px;height:42px;font-family:ACapt;font-size:24px;width:100%;color:#000;" placeholder="{{current.name}}">
							</div>
							<div class="col-md-2">
								<button type="submit" style="border-radius:3px;width:42px;height:42px;" class="btn btn-primary">S</button>
							</div>
						</form>

					</div>

				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="border-bottom:1px dotted #FFF;margin-top:20px;margin-bottom:20px;"></div>
			</div>

		</div>
	</div>

	<div class="row">
		<div class="col-md-12">

			<a class="action-collapser" data-tgt="action-change-node-map" style="cursor:pointer;font-size:1.5em;"><i class="fa fa-cloud-upload"></i> Retexture</a><br>
			<small style="display:block;color:#23527c;margin-left:25px;">Upload a new texture for your planet</small>
		
		</div>
	</div>
	<div class="row" id="action-change-node-map" style="display:none;">
		<div class="col-md-12">
			
			<form id='action-node-map'>

				<div class="row">
					<div class="col-md-12">
						<img src="{{img.src}}" style="width:100%;display:block;">
					</div>
				</div>

				<div class="row" style="margin-top:20px;">
					<div class="col-md-10">
						<input type="file" id='action-node-map-file' name="texture-file" style="width:100%;height:42px;padding-top:10px;" required/><br>
					</div>
					<div class="col-md-2">
						<button type="submit" style="border-radius:3px;width:42px;height:42px;" class="btn btn-primary">S</button>
					</div>					
				</div>
				<div class="row">
					<div class="col-md-12" style="border-bottom:1px dotted #FFF;margin-top:20px;margin-bottom:20px;"></div>
				</div>

			</form>		


		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<a class="action-collapser" data-tgt="action-revert-node-settings" style="cursor:pointer;font-size:1.5em;"><i class="fa fa-recycle"></i> Reset</a><br>
			<small style="display:block;color:#23527c;margin-left:25px;">Reset all planet settings to their defaults</small>		
		</div>
	</div>
	<div class="row" id="action-revert-node-settings" style="display:none;">
	</div>

</script>


<script id="tmpl-u-actions-box-node-shipyard" type="x-tmpl-mustache">

	<div class="row">
		<div class="col-md-12">
			<h2 style="color:#FFF;font-family:ACapt;font-size:4em;">Shipyard</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			Build ships to explore and defend your local system, or start looking further abroad by discovering new stars and conquering/colonizing their planets.
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<span style="border-bottom:3px solid #23527c;text-align:center;font-size:0.7em;margin-bottom:20px;padding-top:10px;display:block;width:100%;"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 style="color:#FFF;font-family:ACapt;font-size:1.5em;">Launch-ready ships</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="actions-box-node-shipyard-launchlist">
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<span style="border-bottom:3px solid #23527c;text-align:center;font-size:0.7em;margin-bottom:20px;padding-top:10px;display:block;width:100%;"></span>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h3 style="color:#FFF;font-family:ACapt;font-size:1.5em;">Ship hulls</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="actions-box-node-shipyard-hulllist">
		</div>
	</div>

</script>

<script id="tmpl-u-actions-box-node-buildings" type="x-tmpl-mustache">
	<div class="row">
		<div class="col-md-12">
			<h2 style="color:#FFF;font-family:ACapt;font-size:4em;">Buildings</h2>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			Build buildings to make credits or develop endeavours such as science, industry, agriculture or military. 
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<span style="border-bottom:3px solid #23527c;text-align:center;font-size:0.7em;margin-top:20px;padding-top:10px;display:block;width:100%;"></span>
				<span class="bbe-title"><i class="fa fa-credit-card"></i>{{stats.crd}} - <i class="fa fa-tree"></i>{{stats.agr}} - <i class="fa fa-industry"></i>{{stats.ind}} - <i class="fa fa-bolt"></i>{{stats.mil}} - <i class="fa fa-flask"></i>{{stats.sci}}</span>
			<span style="border-bottom:3px solid #23527c;text-align:center;font-size:0.7em;margin-bottom:20px;padding-top:10px;display:block;width:100%;"></span>
		</div>
	</div>
	<div id="actions-box-node-buildings-listing">
		<div class="row">
			<div class="col-md-12" >

				{{#buildings}}

					<div class="row" style="margin-bottom:25px;font-size:1em">
						<div class="col-md-2">
							<img style="margin-left:3px;width:100%" src='{{tdir}}{{icon}}' />

						</div>
						<div class="col-md-3">

							<span class="bbe-title">{{name}}</span>
							<span>{{description}}</span>

						</div>	
						<div class="col-md-2">

							<span class='bbe-minititle'>Income</span>
							{{#stats}}
								{{value}} {{key}}
							{{/stats}}

						</div>
						<div class="col-md-2">

							<span class='bbe-minititle'>Costs</span>
							{{#costs}}
								<span>
									{{cost}} {{type}}
								</span>
							{{/costs}}

						</div>
						<div class="col-md-3">

							<span class="bbe-minititle">Owned</span>
							<span class="bbe-amount bbe-title">{{amount}}</span>
							<a class="action-buildings-buy-expand" data-id="{{id}}">buy more</a>

						</div>
					</div>

				{{/buildings}}

			</div>
		</div>
	</div>

</script>

<script id="tmpl-u-actions-box-node-buildings-buy-more" type="x-tmpl-mustache">

	<form class="action-buildings-btn-buy-form" data-id="{{id}}">

		<div class="row">
			<div class="col-md-3">
				<a class="action-buildings-btn action-buildings-destroy" title="Destroy {{name}}" data-id="{{id}}"><i class="fa fa-minus"></i></a>
			</div>
			<div class="col-md-6">
				<span class="bbm-lister bbm-lister-{{id}}">0</span>
			</div>
			<div class="col-md-3">
				<a class="action-buildings-btn action-buildings-purchase" title="Purchase" data-id="{{id}}"><i class="fa fa-plus"></i></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12" style="text-align:center;" class="action-buildings-btn-buyer">
				<button class="btn btn-xs btn-primary" type="submit">Do it.!</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 action-buildings-btn-buy-costinger action-buildings-btn-buy-costinger-{{id}}"></div>
		</div>
	</form>

</script>
