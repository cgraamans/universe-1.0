<script id="tmpl-stats-star" type="x-tmpl-mustache">

	<div class="stats-list">
		<table>
			<tr>
				<td width="33%">Type</td><td width="10%"> </td><td>{{type}}</td>
			</tr>
			<tr>
				<td width="33%">Size</td><td width="10%"> </td><td>{{r0}}</td>
			</tr>
			<tr>
				<td width="33%">Mass</td><td width="10%"> </td><td>{{mass}}</td>
			</tr>
			<tr>
				<td width="33%">Temperature</td><td width="10%"> </td><td>{{K}}K</td>
			</tr>
			<tr>
				<td width="33%">Spectral</td><td width="10%"> </td><td>{{spectral}}</td>
			</tr>
			<tr>
				<td width="33%">Luminosity</td><td width="10%"> </td><td>{{lum}}</td>
			</tr>
		</table>
		{{#users}}
			<a style="color:yellow;" title="{{.}}"><i class="fa fa-child"></i></a>
		{{/users}}
	</div>

</script>

<script id="tmpl-stats-node" type="x-tmpl-mustache">

	<div class="stats-list">
		<table>
			<tr>
				<td width="33%">Type</td><td width="10%"> </td><td>{{type}}</td>
			</tr>
			<tr>
				<td width="33%">R0</td><td width="10%"> </td><td>{{type_size_Ro}}</td>
			</tr>
			<tr>
				<td width="33%">Atm</td><td width="10%"> </td><td>{{type_atmosphere}}</td>
			</tr>
			<tr>
				<td width="33%">Surface</td><td width="10%"> </td><td>{{type_surface}}</td>
			</tr>
			<tr>
				<td width="33%">AU</td><td width="10%"> </td><td>{{au}}</td>
			</tr>
		</table>
		{{#users}}
			<a style="color:yellow;" title="{{name}}"><i class="fa fa-child"></i></a>
		{{/users}}
	</div>

</script>

<script id="tmpl-storyboard-item" type="x-tmpl-mustache">
	<div class="storyboard-item story-{{id}}">
		<div class="row">
			<div class="col-md-6" style="font-family:ACapt;font-size:1.75em;color:white;">
				{{name_star}} {{name_node}} - YEAR {{cluster_year}}
			</div>
			<div class="col-md-3"></div>
			<div class="col-md-3">
				<span style="font-family:ACapt;font-size:1.25em;color:white;display:block;">By {{name}}</span>{{{icons}}}
			</div>
		</div>
		<div class="row" style="min-height:200px;">
			<div class="col-md-12 story-text">{{{text}}}</div>
		</div>
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-6" class="story-error">{{error}}</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" style=""></div>
			<div class="col-md-2"></div>
		</div>
	</div>
</script>

<script id="tmpl-storyboard-userIcons" type="x-tmpl-mustache">
	<button class="btn btn-warning btn-xs storyboard-item-edit" title="Edit" data-id="{{id}}">E</button> <button class="btn btn-warning btn-xs storyboard-item-del" title="Delete" data-id="{{id}}">D</button> 
</script>

<script id="tmpl-storyboard" type="x-tmpl-mustache">
	<div id="story-border" style="position:relative;width:100%;height:100%;">
		<div id="storyboard-header">

			<div class="row">
				<div class="col-md-8">
					<h2 style="color:#FFF;font-family:ACapt;font-size:4em;text-shadow: 4px 3px 2px rgba(150, 150, 150, 0.67);">Storyboard</h2>
				</div>
				<div class="col-md-4">
					<div class="row">
						<div class="col-md-12 storyboard-header-name">
							{{name}}
						</div>
					</div>
					<div class="row">
						<div class="col-md-12" id="storyboard-icons" style="text-align:right;">
							<a id="storyboard-open-menu" style="cursor:pointer;"><i class="fa fa-lg fa-ellipsis-h"></i></a> 
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12" style="border-bottom:3px solid #337ab7;"></div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div id="storyboard-form">
						<div class="storyboard-menu" style="display:none;">
							<form id="storyboard-form-menu">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="storyboard-data"></div>
	</div>
</script>

<script id="tmpl-stats-cluster" type="x-tmpl-mustache">

	<div class="system-info-list" style="position:relative;width:100%;color:green;">
		{{#users}}
			<a style="color:yellow;" title="{{name}}"><i class="fa fa-child"></i></a>
		{{/users}}
	</div>

</script>

<script id="tmpl-stats-nav" type="x-tmpl-mustache">
	<a id="stats-prv" class="stats-nav-nav" data-tgt="{{pg.prv.stub}}" title="{{pg.prv.name}}"><i class="fa fa-1 fa-arrow-left"></i></a></div>
	<a id="stats-nxt" class="stats-nav-nav" data-tgt="{{pg.nxt.stub}}" title="{{pg.nxt.name}}"><i class="fa fa-1 fa-arrow-right"></i></a></div>
	{{#parent}}
		<a href="{{loc}}" class="stats-nav-nav" title="{{name}} ({{type}})"><i class="fa fa-1 fa-level-up"></i></a>
	{{/parent}}
	<div class="stats-nav-txt">
		<a href="{{stub}}">{{name}}</a>
	</div>
	<a id="stats-min" class="stats-nav-nav stats-nav-navr"><i class="fa fa-1 fa-minus"></i></a></div>
	<div style="clear:both;"></div>

</script>

<script id="tmpl-stats" type="x-tmpl-mustache">

	<div id="stats-yr">{{place}} <span>year {{year}}</span></div>
	<div id="stats-nav"></div>
	<div id="stats-box"></div>

</script>