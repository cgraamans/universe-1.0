<!-- http://codepen.io/ajaypatelaj/pen/prHjD -->

<div class="navbar navbar-default" role="navigation">

    <div class="container-fluid">

        <div style="height:100%;">
            
            <a href="/" id="topLogo"><img src="/assets/img/logo.png" height="24"/></a>

            <ul class="nav navbar-nav navbar-right">

                <li><a href="#" title="Nodes" id="node-menu-toggle"><i class="fa fa fa-sun-o"></i></a></li>

                <li><a href="#" title="Events" id="stats[-menu-toggle"><i class="fa fa-newspaper-o"></i></a></li>

                <li><a href="#" title="Stats" id="stats[-menu-toggle"><i class="fa fa-bookmark"></i></a></li>                

                <li><a href="#" title="Users" id="user-menu-toggle"><i class="fa fa-users"></i></a></li>

                <li><a href="#" title="Chat" id="chat-menu-toggle"><i class="fa fa-commenting-o"></i></a></li>

                <li><a href="#" title="Notifications"><i class="fa fa-sticky-note-o"></i></a></li>

                <li><a href="#" title="Mail"><i class="fa fa-envelope"></i></a></li>
                <li class="nav-name"> Welcome, <?= $name ?></li>

                <li><a class="dropdown-toggle" data-toggle="dropdown" title="Settings" href="#"><i class="fa fa-cogs"></i></a>
                    <ul class="dropdown-menu multi-level">
                        <li><a href="#">User Settings</a></li>
                        <li><a href="/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
            <div id="nav-credits"></div>
        </div>

    </div>

</div>