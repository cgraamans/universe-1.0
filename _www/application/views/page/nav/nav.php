<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <a id="topLogo" href="/"><img src="/assets/img/logo.png" height="24"/></a>
        <ul class="nav navbar-nav navbar-right" style="margin-right:30px">
            <li><a data-toggle="modal" data-target="#modal-login" role="button"><i class="fa fa-sign-in"></i> Login</a></li>
            <li><a data-toggle="modal" data-target="#modal-register" role="button"><i class="fa fa-user-plus"></i> Register</a>
            </li>
        </ul>            
    </div>
</div>