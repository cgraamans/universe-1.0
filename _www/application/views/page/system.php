
<audio id="system-audio" loop>
  <source src="/assets/music/this_house.mp3">
  <source src="/assets/music/this_house.ogg">
</audio>

<!-- Main System -->
<div class='three-display default-page'>
	
	<div id="render-box"></div>
	<div id="storyboard"></div>
	<div id="system-menu-righttop"></div>
	<div id="system-menu-rightbottom" class='nav-box'></div>

</div>