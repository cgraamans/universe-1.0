<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="/assets/js/require.js" data-main="/assets/js/init"></script>

    <?php

      foreach($js['pre'] as $js_pre) {

        echo $js_pre;

      }

    ?>

    <link rel="shortcut icon" href="/assets/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="/assets/images/favicon.ico" type="image/x-icon">
    
    <link href="/assets/vendor/bootstrap-3.3.5-dist/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" href="/assets/vendor/font-awesome/css/font-awesome.min.css">
    
    <link rel="stylesheet" type="text/css" href="/assets/css/init.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/core.css">

    <?php

      foreach($css as $css_line) {

        echo '<link rel="stylesheet" type="text/css" href="'.$css_line.'">';

      }

    ?>    

      <?php

        foreach($templates as $tmpl) {

          echo $tmpl;
          
        }
      
      ?>
      
    <title><?= $title ?></title>
	
  </head>
  <body>


      <?php

        foreach($modals as $modal) {

          echo $modal;
          
        }
      
      ?>



    <div class="container-fluid">


      <?= $content['header'] ?>

      <?= $content['main'] ?>

      <?= $content['footer'] ?>
      
    </div>

    <?php
      foreach($js['post'] as $js_post) {
        echo $js_post;
      }
    ?>
    
  
    
  </body>
</html>	