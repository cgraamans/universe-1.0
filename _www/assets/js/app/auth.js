requirejs(['init'],function() {

	requirejs(['global','jquery','jscookie','bootstrap'],function(gl,$,jscookie,b){

		var iface = global.opt.iface;

		$(document).ready(function(){

			// Setup Bootstrap Stuff
		    $(".modal").on('shown', function() {
		        $(this).find("[autofocus]:first").focus();
		    });

			$(iface.modals.proc.box).modal({ show: false});
			
			// Register submit
			$(iface.auth.register).submit(function(event){

				event.preventDefault();

				var passData = {
					method: "POST",
					dataType: 'json',
				};

				$(iface.modals.proc.box).modal('show');
				$(iface.modals.proc.box).find(iface.modals.proc.text).text('Registering Username');

				var userReg = passData;
				userReg.data = $(this).serialize();
				userReg.url = global.opt.loc.api+"/auth/register/user";

				$.ajax(userReg).done(function(a) {

					$(iface.modals.proc.box).find(iface.modals.proc.text).text('Registering Home System');

					var starReg = passData;

					starReg.url = global.opt.loc.api+"/auth/register/star";

					a.action = 'register';
					starReg.data = a;

					$.ajax(starReg).done(function(cc) {

						$(iface.modals.proc.box).find(iface.modals.proc.text).html('Home System Registered.<br><small><a href="/node/'+cc.redirect+'">Redirecting to: /node/'+cc.redirect+'</a></small>');

						user = {
							home: cc.redirect,
							name: a.name,
							key: a.key
						}
						jscookie.set('user', user, { expires: 2 });
						
						window.location.replace("/node/"+cc.redirect);

					}).error(function(d,e){
						$(iface.modals.proc.box).find(iface.modals.proc.text).html("Error:<br>"+ d.responseText);
						console.log(d);
						console.log(e);
					});

				}).error(function(a,bb){
					$(iface.modals.proc.box).find(iface.modals.proc.text).html("Error:<br>"+ a.responseText);
					console.log(a);
					console.log(bb);
				});

			});

			// Login submit
			$(iface.auth.login).submit(function(event){		

				event.preventDefault();

				var login = $(this).serialize();

				$.ajax({
					method: "POST",
					url: global.opt.loc.api+"/auth/login",
					data: login

				}).done(function(a) {

					jscookie.set('user', a, { expires: 2 });
					// window.location.replace("/node/"+a.home);

				}).error(function(a,b){
					console.log(a);
					console.log(b);
				});

			});


		});


	});

});


	/*

	


		console.log(g);


		// Login submit
		$('#login').submit(function(event){		
			event.preventDefault();

			var login = $(this).serialize();

			$.ajax({
				method: "POST",
				url: "http://api.universe/auth/login",
				data: login
			}).done(function(a) {

				Cookies.set('user_data', a, { expires: 7 });
				window.location.replace("/node/"+a.home);

			}).error(function(a,b){
				console.log(a);
				console.log(b);
			});

		});


	});

});


*/