define({

	init: function($,jscookie,opt){

		var audio_mute = jscookie.get('audioOff');
		if (typeof audio_mute === 'undefined') {
    		
			$('#system-audio').trigger('play');
			console.log('play');
		} else {

			console.log(audio_mute);

			if (audio_mute == 0) {

				$('#system-audio').trigger('play');

			} else {
				
				$('#system-audio').trigger('pause');

			}

		}

	},

	defaults: function(inf,$,jscookie,Mustache,global,scene,callback) {

		var that = this;

		// buttons init & display
		that.buttons.init(inf,$,jscookie);
		that.buttons.audio(inf,$,jscookie);
		that.buttons.tracks(inf,$,jscookie);
		that.buttons.storyboard.init(global.socket,inf,$,jscookie,Mustache);
		// that.buttons.labels(inf,$,jscookie);

		// stats window init & display
		var stats = this.stats.init($,Mustache,global);
		if (typeof stats !== 'undefined') {

			global.three.opt.inf.stats = stats;
			var pag = this.stats.paginate(global.three.obj.meta.focus,global.three.opt.inf.stats.nav);			
			if (typeof pag !== 'undefined') {

				var navs = that.stats.nav(global.three.opt.inf.stats.nav,pag);

	  			var renderedNav = Mustache.render($(global.three.opt.inf.tmpl.stats_nav).html(),navs);
	  			$(global.three.opt.inf.dom.stats_nav).html(renderedNav);

	  			this.stats.box(navs.id,$,Mustache,global);
	  			that.stats.prime(global,$,Mustache,scene);
		
			}

		}
		callback();

	},

	stats: {

		init: function($,Mustache,global) {

			var obj = global.three.obj; 

			var boxData = [];
			var navData = [];

			var template = $(global.three.opt.inf.tmpl.stats_box).html();

  			var placement;
  			var yearplace = obj.origin.cluster.year;
			switch(obj.meta.type) {

				case 'star':
					navData.push({
						id:'star',
						stub:'/star/'+obj.origin.star.stub,
						name:obj.origin.star.name,
						parent: [{
							loc: '/cluster/'+obj.origin.cluster.name,
							name:obj.origin.cluster.name,
							type:'Cluster',
						}]
					});

					boxData.push({
						id: 'star',
						data: obj.origin.star,
					});

					placement = obj.origin.star.name;

					console.log('TODO: fix navData in interface for clusterpoints')

					break;
				case 'node':

					var hasParent = {
						loc:'/star/'+obj.origin.star.stub,
						name:obj.origin.star.name,
						type:'Star',						
					};

					if (obj.origin.node.type == 'moon') {
						hasParent = {
							loc: '/node/'+obj.origin.node.parent_stub,
							name: obj.origin.node.parent,
							type: 'Planet',
						};
					}

					navData.push({
						id:obj.origin.node.stub,
						stub:'/node/'+obj.origin.node.stub,
						name:obj.origin.node.name,
						parent: [hasParent],
					});

					boxData.push({
						id:obj.origin.node.stub,
						data: obj.origin.node,
					});

					placement = obj.origin.star.name + ' ' + obj.origin.node.name;

					break;
				case 'cluster':

					placement = obj.origin.cluster.name;

					break;

			}

  			var rendered = Mustache.render(template,{place:placement,year:yearplace});
  			$(global.three.opt.inf.dom.stats).html(rendered);

			obj.nodes.forEach(function(nodule){

				if ((nodule.type == 'rock') || (nodule.type == 'gas') || (nodule.type == 'moon')) {

					var pushee = {
						id:nodule.stub,
						stub:'/node/'+nodule.stub,
						name:nodule.name,
						parent:[],					
					};

					navData.push(pushee);

					boxData.push({
						id:nodule.stub,
						data: nodule,
					});

				}

			});

			return {nav:navData,box:boxData};

		},

		paginate: function(focus,nav) {


			var pagination = {
				prv:0,
				nxt:0,
				current:0
			};
			if (focus !== false) {

				var focal = nav.map(function(x) {return x.id; }).indexOf(focus);

				if ((typeof focal !== 'undefined') && (isNaN(focal) === false)) {
					pagination.current = focal;
				}

			}

			// pagination
			var isLong = nav.length;

			if (isLong > 0) {

				if (pagination.current === 0) {

					pagination.prv = isLong -1;
					if (isLong > 1) {

						pagination.nxt = pagination.current+1;

					}
					
				} else {
					if (pagination.current === parseInt(isLong-1)) {

						pagination.nxt = 0;
						pagination.prv = pagination.current-1;

					} else {

						pagination.nxt = pagination.current+1;
						pagination.prv = pagination.current-1;

					}
				}

			}

			return pagination;

		},
		
		prime: function(global,$,Mustache,scene) {
			

			$(document).ready(function(){


				$('body').on('click','a#stats-nxt',function(){

					var dir = $(this).data('tgt');
					global.three.inf.stats.box(dir,$,Mustache,global,scene);
					window.location.hash = '#'+dir;

				});

				$('body').on('click','a#stats-prv',function(){

					var dir = $(this).data('tgt');
					global.three.inf.stats.box(dir,$,Mustache,global,scene);
					window.location.hash = '#'+dir;

				});

				
				$('body').on('click','a#stats-min',function(){

					$('#stats-box').animate({
						height:'toggle'
					});

				});

			});

		},

		nav: function(nav,pagination) {
			
			var data = nav[pagination.current];

			var prv_data = nav[pagination.prv];
			var nxt_data = nav[pagination.nxt];

			data.pg = {
				prv: {
					stub:prv_data.id,
					name:prv_data.name,
				},
				nxt: {
					stub:nxt_data.id,
					name:nxt_data.name,
				}
			};

			return data;	

		},

		box: function(dir,$,Mustache,global) {

			var that = this;

			var paginate = that.paginate(dir,global.three.opt.inf.stats.nav);
			
			var navT = $(global.three.opt.inf.tmpl.stats_nav).html();
			var navD = that.nav(global.three.opt.inf.stats.nav,paginate);
			

			var boxD = global.three.opt.inf.stats.box[paginate.current]

			var boxTmpl = global.three.opt.inf.tmpl.stats_box_node;
			if (navD.id == 'star') {
				boxTmpl = global.three.opt.inf.tmpl.stats_box_star;
			}
			if(navD.id == 'cluster') {
				boxTmpl = global.three.opt.inf.tmpl.stats_box_cluster;
			}
			var boxT = $(boxTmpl).html();

  			var renderedNav = Mustache.render(navT,navD);
  			$(global.three.opt.inf.dom.stats_nav).html(renderedNav);

			var object = scene.getObjectByName(navD.id);
			if (typeof object !== 'undefined') {

				object.add(camera);
				camera.fov = 1;
			
			}

  			var renderedBox = Mustache.render(boxT,boxD.data);
  			$(global.three.opt.inf.dom.stats_box).html(renderedBox);


		}

	},

	buttons: {

		init: function(inf,$,jscookie) {


			var noTracks = jscookie.get('noTracks');
			if (typeof noTracks !== 'undefined') {

				if (noTracks == 1) {

					// hide tracks
					var tracks = scene.getObjectByName(inf.obj.tracks,true);
					if (typeof tracks !== 'undefined') {

						tracks.visible = false;

					}

				}

			}

			// The click functions
			$(document).ready(function(){

				$('body').on('mouseover','.inf-button',function(){
					
					$(this).addClass('inf-button-over');

				});

				$('body').on('mouseout','.inf-button',function(){
					
					$(this).removeClass('inf-button-over');

				});

			});

		},

		audio: function(inf,$,jscookie) {

			var audioOff = jscookie.get('audioOff');
			if (typeof audioOff !== 'undefined') {

				if (audioOff == 0) {

					$(inf.dom.buttons).append('<a id="music-muter" class="inf-button" title="toggle music"><i class="fa fa-lg fa-volume-up"></i></a>');

				} else {

					$(inf.dom.buttons).append('<a id="music-muter" class="inf-button" title="toggle music silent"><i class="fa fa-lg fa-volume-off color-red"></i></a>');

				}

			} else {

				$(inf.dom.buttons).append('<a id="music-muter" class="inf-button" title="toggle music"><i class="fa fa-lg fa-volume-up"></i></a>');
				audioOff = 0;
			}
			
			$(document).ready(function(){


			    $('body').on('click','a#music-muter',function(){

			    	
			    	if (audioOff == 1) {

			    		// audio on
			    		$(this).html('<i class="fa fa-lg fa-volume-up"></i>');
			    		$('#system-audio').trigger('play');
			    		audioOff = 0;
			    		jscookie.set('audioOff',0);
			    	
			    	} else {
			    		
			    		// audio off
			    		$(this).html('<i class="fa fa-lg fa-volume-off color-red"></i>');
			    		$('#system-audio').trigger('pause');
			    		audioOff = 1;
			    		jscookie.set('audioOff',1);
			    	}

		    	});


	    	});

		},

		tracks: function(inf,$,jscookie) {

			var noTracks = jscookie.get('noTracks');
			if (typeof noTracks !== 'undefined') {

				if (noTracks == 1) {

					$(inf.dom.buttons).append('<a id="track-toggle" class="inf-button" title="toggle node-tracks noTracks"><i class="fa fa-lg fa-circle-o-notch color-red"></i></a>');

				} else {

					$(inf.dom.buttons).append('<a id="track-toggle" class="inf-button" title="toggle node-tracks"><i class="fa fa-lg fa-circle-o-notch"></i></a>');

				}
				

			} else {

				$(inf.dom.buttons).append('<a id="track-toggle" class="inf-button" title="toggle node-tracks"><i class="fa fa-lg fa-circle-o-notch"></i></a>');
				noTracks = 0;
			}
			
			$(document).ready(function(){

			    $('body').on('click','a#track-toggle',function(){

			    	if (noTracks == 1) {

						// show tracks
						var tracks = scene.getObjectByName(inf.obj.tracks,true);
						if (typeof tracks !== 'undefined') {

							tracks.visible = true;
				    		jscookie.set('noTracks',0);
							noTracks = 0;
							$(this).find('i').removeClass('color-red');

						}			    		


		    		} else {

		    			// hide tracks
						var tracks = scene.getObjectByName(inf.obj.tracks,true);
						if (typeof tracks !== undefined) {

							tracks.visible = false;
							noTracks = 1;
							$(this).find('i').addClass('color-red');
				    		jscookie.set('noTracks',1);
						
						}
			    	
			    	}

		    	});


	    	});

		},

		storyboard: {

			init: function(socket,inf,$,jscookie,Mustache) {

				var that = this;

				// render mustache template first
				var boxS = $(global.three.opt.inf.tmpl.storyboard_box).html();

				var boxSData = {}
				switch(global.three.obj.meta.type) {
					case 'star':
						boxSData.name = global.three.obj.origin.star.name;
						break;
					case 'node':
						boxSData.name = global.three.obj.origin.star.name + ' - ' +  global.three.obj.origin.node.name 
						break;
					case 'cluster':
						boxSData.name = global.three.obj.origin.cluster.name;
						break;
				}

	  			var renderedSbox = Mustache.render(boxS,boxSData);
	  			$(global.three.opt.inf.dom.story_box).html(renderedSbox);			

	  			if (global.three.obj.meta.login == true) {
	  				$(global.three.opt.inf.dom.story_icons).prepend('<a id="storyboard-open-add" style="cursor:pointer;"><i class="fa fa-lg fa-plus"></i></a> ');
	  			}

				$(inf.dom.buttons).append('<a id="storyboard-toggle" class="inf-button" title="toggle storyboard" style="margin-top:30px;"><i class="fa fa-lg fa-book color-red"></i></a>');

				$(global.three.opt.inf.dom.story_data).enscroll().css("width","100%");

				$(document).ready(function(){

				    $('body').on('click','a#storyboard-toggle',function(){

				    	if ($(inf.dom.story_box).is(":visible")) {

				    		$(this).html('<i class="fa fa-lg fa-book color-red"></i>');
				    		$(inf.dom.story_box).hide();
				    	
				    	} else {
				    		
				    		socket.emit('storyboard',{id:global.three.obj.meta,user:global.user.call()});

				    		$(this).html('<i class="fa fa-lg fa-book"></i>');
							$(inf.dom.story_box).show();

				    	}

			    	});


		    	});



			},

			trigger: function() {



			},

		},

	},

	user: {

		init: function(global,scene,$,Mustache,jscookie,callback) {

			var ud = {ok:false};
			if (typeof (global.three.obj.meta.login) !== 'undefined') {

				if (global.three.obj.meta.login === true) {

					if (typeof (global.user.data) !== 'undefined') {

						var gto = global.three.obj;
						var loadWindows = [];

						if (gto.meta.type == 'star') {
							
							var isStarUser = false;
							gto.nodes.forEach(function(nd){

								if (typeof nd.users != 'undefined') {

									if ((nd.users !== false) && (nd.users.length > 0)) {

										nd.users.forEach(function(td){

											if (td.isUser === true) {

												isStarUser = true;

											}

										});

									}

								}
							
							});

							if (isStarUser !== false) {

								var rendered = Mustache.render($(global.user.data.storyboard.tmpl.form).html());
								$(global.three.opt.inf.dom.story_form).prepend(rendered);
								$(global.user.data.storyboard.dom.form).hide();

							}

						} 

						if (gto.meta.type == 'node') {

							var isNodeUser = false;
							var hasYard = false;
							if (typeof gto.origin.node.users != 'undefined') { 

								if ((gto.origin.node.users !== false) && (gto.origin.node.users.length > 0)) {

									gto.origin.node.users.forEach(function(td){

										console.log(td);

										if (td.isUser === true) {

											isNodeUser = true;
											if (td.hasYard === true) {
												hasYard = true;
											}

										}

									});

								}

							}
							
							if (hasYard === true) {

								loadWindows.push({id:'node_shipyard',data:gto.meta.request});

							}

							if (isNodeUser !== false) {

								loadWindows.push({id:'node_buildings',data:gto.meta.request});
								loadWindows.push({id:'node_settings',data:gto.meta.request});

								var rendered = Mustache.render($(global.user.data.storyboard.tmpl.form).html());

								$(global.three.opt.inf.dom.story_form).prepend(rendered);
								$(global.user.data.storyboard.dom.form).hide();

							}

						}
						
						if (loadWindows.length > 0) {

							loadWindows.forEach(function(identifyer,idx) {

								var globaltemplate = $(global.user.data.actions.dom.mustache.global).html();
					  			var globalrendered = Mustache.render(globaltemplate,{identifyer:identifyer.id});									

					  			$(global.dom.container).append(globalrendered);

					  			var icon = global.user.data.actions.dom.buttons.icons[identifyer.id];
					  			if (typeof icon !== 'undefined') {

						  			var btnData = {
						  				identifyer:identifyer.id,
						  				offset:(idx*global.user.data.actions.dom.buttons.size)+'px',
						  				icon:icon,
						  			};

									var btntemplate = $(global.user.data.actions.dom.mustache.buttons).html();
						  			var btnrendered = Mustache.render(btntemplate,btnData);									

						  			$("#action-box-"+identifyer.id).append(btnrendered);

						  			var targetAction = {
						  				id:identifyer.id,
						  				type:identifyer.id,
						  				data:identifyer.data,
						  				user:global.user.call(),
						  			};

						  			global.socket.emit('action',targetAction);

					  			}									

							});

							// document ready code here.
							$('body').on('click','.action-box-btn',function(){

								var trigger = $(this).data('trigger');
								var extended = $(this).data('extended');

								if (extended !== false) {

									$('#action-box-'+trigger).animate({
										left: "-=33%"
									},{
										complete:function(){
											$('#action-box-'+trigger).css('z-index',3);
										}
									});
									$(this).data('extended',false);

								} else {

									$('#action-box-'+trigger).animate({
										left: "+=33%"
									},{
										start:function(){
											$('#action-box-'+trigger).css('z-index',30);
										}
									});
									$(this).data('extended',true);

								}

							});

							callback();

						} else {

							callback();

						}

					}


				} else {

					callback('Not logged in.');

				}

			} else {

				callback('No login specified.');

			}

		},

	},

});
