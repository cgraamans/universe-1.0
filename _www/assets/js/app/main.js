var localRun = {
	scrollto: function(id,$) {

		$('#main').animate({
			scrollTop: $('#'+id).offset().top
		},'fast');

	}

};

requirejs(['init'],function() {

	requirejs(['global','jquery','jscookie','bootstrap','enscroll'],function(gl,$,jscookie,b){

		// Init enscroll
		$('#main').enscroll().css('width', '100%');
		$('body').on('click','.scrollTo',function(e) {
			
			e.preventDefault();

			var to = $(this).data('to');
			if (typeof to !== 'undefined') {
				localRun.scrollto(to,$);
			}
			
		});

		


	});

});