
requirejs(['init'],function() {


/*

ToDo
  - Replace modal with more discrete screen in background. Green text. Slightly seethrough.
  - Remove bootstrap (if not used)

*/
	requirejs(['global','jquery','bootstrap','jscookie','conf.system','mustache','app/interface','three','threep/init','micromarkdown','enscroll'],function(g,$,bootstrap,jscookie,sys_conf,Mustache,inf,THREE,threep,micromarkdown,enscroll) {

		// Crossorigin enabled!
		THREE.ImageUtils.crossOrigin = '';

		var initialRequest = {

			u:global.user.call(),
			type:false,
			origin:false,
			cluster:false,
			focus:false,
		
		};

		global.three = {

			render:{

				gyro:[],
				rotate:[],

			},
			obj:false,
			opt:sys_conf,
			inf:inf,
		};

		global.three.inf.init($,jscookie,global.three.opt);

		var modalbox = global.opt.iface.modals.proc;
    	$(modalbox.box).modal({ show: true });
		$(modalbox.text).html("<h3>Rendering System</h3>");

		var pathArray = window.location.pathname.split('/');
	    if (typeof(pathArray) !== 'undefined') {

	    	if ((pathArray.length == 3) && ( sys_conf.valid.ctrl.indexOf(pathArray[1]) > -1 )) {

	    		$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Priming Telescopes.</span>");

	    		initialRequest.type = pathArray[1];
				if(window.location.hash) {

	    			var typeArr = pathArray[2].split('#');
	    			initialRequest.origin = typeArr[0];
    				initialRequest.focus = window.location.hash.substring(1);

				} else {

	    			initialRequest.origin = pathArray[2];	
	    		
	    		}

	    	} else {

	    		$(modalbox.text).append("<span class='"+modalbox.span_err+"'> - Error: Invalid URL.</span>");

	    	}

    	} else {
    		
    		$(modalbox.text).append("<span class='"+modalbox.span_err+"'> - Error: No type specified.</span>");
    	
    	}

    	$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Scanning for signals from '"+initialRequest.origin+"'.</span>");
 		global.socket.emit(initialRequest.type,initialRequest);
		
 		/*

 			STORYBOARD SOCKET RETURN

 		*/
 		global.socket.on('storyboard_data',function(sb){

 			$(global.three.opt.inf.dom.story_data).html('');


 			if (sb.ok === true) {

 				sb.results.forEach(function(ra){

 					if (ra.editable == true) {

 						ra.icons = Mustache.render($(global.three.opt.inf.tmpl.storyboard_user_icons).html(),{id:ra.id});

					}

					ra.text = micromarkdown.parse(ra.text);

 					var rendered = Mustache.render($(global.three.opt.inf.tmpl.storyboard_item).html(),ra);
					$(global.three.opt.inf.dom.story_data).append(rendered);

 				});

 			}

 			
		
 		});

 		/*

			MAIN SOCKET RETURN

 		*/
		global.socket.on(initialRequest.type+'_data',function(systemData) {
		
			$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Signal Found. Classifying.</span>");

			if (typeof systemData !== 'undefined') {

				if (systemData.ok === true) {


					$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Stellar Target Acquired.</span>");

					// Start Working on the scene.
					var  renderer, scene, camera;
					var that = this;

					global.three.obj = systemData.result;
					global.three.obj.meta.focus = initialRequest.focus;


					var clusterForCookie = global.three.obj.origin.cluster.name;
					var clusterCookie = jscookie.get('user_cluster');
					if (typeof clusterCookie !== 'undefined') {
						var cC = JSON.parse(clusterCookie);
						if (cC.cluster != clusterForCookie) {
							jscookie.set('user_cluster',{cluster:clusterForCookie});		
						}
					} else {
						jscookie.set('user_cluster',{cluster:clusterForCookie});
					}

					

					// require controls  and threep after three
					requirejs(['OrbitalControls','threep/run'],function(oc,tR){

						threep.run = tR;

						var initThreeP = threep.init('render-box',function(){

							$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Space Program Initiated..</span>");	

							// '/assets/img/textures/skybox/c1.jpg'
							var initSkyBox = threep.func.skybox(this.scene,global.opt.loc.textures+'skybox/skybox'+global.three.obj.origin.cluster.rand_bg+'.jpg',function(){

								$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Probe Launched.</span>");

								//Prepare Nav Box (on every page)

								if (global.three.obj.meta.type === "star") {

									var star = threep.run.star(global.three.obj.origin.star,global.three.opt,function(){

										var starObj = global.three.obj.origin.star;

										$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Star Located.</span>");

									
									});

								}

								if (global.three.obj.meta.type === "node") {

									var node = threep.run.node(global.three.obj.origin.node,global.three.opt,function(){

										$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Zooming in on object.</span>");


									});

								}

								//run nodes
								// - nodes
								//   -subnodes
								//	 -ships

								//	 -fleets
								if (global.three.obj.nodes.length > 0) {

									var nodes = threep.run.nodes(global.three.obj.nodes,global.three.opt,function() {

										$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Orbitals found.</span>");

										global.three.obj.nodes.forEach(function(tree){

											if ((tree.subnodes !== false) && (tree.subnodes.length > 0)) {

												tree = threep.run.subnodes(tree,global.three.opt);													

											}

										});

										// standard interface modules
										global.three.inf.defaults(global.three.opt.inf,$,jscookie,Mustache,global,scene,function() {

											$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Views Initiated.</span>");

										});

										global.three.inf.user.init(global,scene,$,Mustache,jscookie,function(err){

											if (err) {
	    		
	    										$(modalbox.text).append("<span class='"+modalbox.span_err+"'> - Error: Bad user interface.</span>");
	    										console.log(err);

											} else {

												$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Actions available.</span>");
												$(modalbox.box).modal('hide');

											}

										});

									});

								} else {

									// standard interface modules
									global.three.inf.defaults(global.three.opt.inf,$,jscookie,Mustache,global,scene,function() {

										$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Views Initiated.</span>");
										$(modalbox.box).modal('hide');

									});

									global.three.inf.user.init(global,scene,$,Mustache,jscookie,function(err) {

										if (err) {
    		
    										$(modalbox.text).append("<span class='"+modalbox.span_err+"'> - Error: Bad user interface.</span>");
    										console.log(err);

										} else {

											$(modalbox.text).append("<span class='"+modalbox.span_pop+"'> - Actions available.</span>");
											$(modalbox.box).modal('hide');

										}

									});

								}
									
							});

						});

					});

				} else {

					$(modalbox.text).append("<span class='"+modalbox.span_err+"'> - "+systemData.result+"</span>");

				}
			
			}
		
		});


		$(document).ready(function(){

			$('#modal-process').modal({ show: false});			

		});

		$( document ).ready(function() {

			var sysmenuExp = {
				showTracks:1,
				showLabels:1
			};
			var showTracks = 1;
			var showLabels = 1;

			$('#system_container').on('mouseover',".menu-right-tab",function(res){

				$('.menu-left').css('z-index',20);
				$(('#'+$(this).data('target'))).css('z-index',100);

			});

			$('#system_container').on('click',".menu-right-tab",function(res){

				var thisStep = 0;
				var thisTarget = $(this).data('target');

				if (thisTarget in sysmenuExp) {
					thisStep = sysmenuExp[thisTarget].thisStep;
				} else {
					sysmenuExp[thisTarget] = {thisStep:0};
				}


				if (thisStep == 0) {
					$(('#'+$(this).data('target'))).animate({
						left: "+=250",
					}, 300, function() {
						sysmenuExp[thisTarget].thisStep = 1;	
					});

				} else {
					$('#'+thisTarget).animate({
						left: "-=250",
					}, 300, function() {
						sysmenuExp[thisTarget].thisStep = 0;
					});
				}

			});

			var bgColorSelObj = $(".system-node-select").css("background-color");

			$('#system_container').on('mouseover',".system-node-select",function(res){

				var id = $(this).data("id");
				$(this).css('background-color', '#547089');

				var object = scene.getObjectByName(id);
				if (typeof object !== 'undefined') {
					object.parent.add(camera);
					camera.fov = 1;
				
				}

				$(".system-node-select").click(function(){

					window.location.href = "/node/"+id;

				});

			}).mouseout(function(){
				$(this).css('background-color', bgColorSelObj);
			});


			$('#system_container').on('mouseover',".system-menu-left-nav-up",function() {

				$(".system-menu-left-contents").animate({scrollTop:-200});

			});


			$('#system_container').on('mouseover',".system-menu-left-nav-down",function() {

				$(".system-menu-left-contents").animate({scrollTop:200});
				
			});

		});


	});

});