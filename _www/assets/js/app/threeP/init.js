define({

	init: function(elem,callback) {

		var that = this;
	    var container = document.getElementById( elem );

	    // Renderer
	    renderer = new THREE.WebGLRenderer( { clearColor: 0x000000, antialias: true, alpha: true } );
	    renderer.setSize($('#'+elem).innerWidth(), $('#'+elem).innerHeight() );

	    container.appendChild( renderer.domElement );

	    // Scene and Camera setup
	    scene = new THREE.Scene();
	    scene.selectable = [];

	    camera = new THREE.PerspectiveCamera( 75, $('#'+elem).innerWidth() / $('#'+elem).innerHeight(), 1, 480000 );
	    camera.position.z = 250;
		camera.position.y = 3000;
		camera.lookAt(new THREE.Vector3(0,0,0));

	    controls = new THREE.OrbitControls(camera,container);
	    controls.maxDistance =120000;

	    global.three.render.start = Date.now();

		this.animate();

	    callback();


	},

	animate: function()	{

	    requestAnimationFrame(this.animate.bind(this));
	    controls.update();
	    this.render();

	},


	render: function() {

		var that = this;

		if (typeof global.three !== 'undefined') {

			// run lookAts for planes
			global.three.render.gyro.forEach(function(aa){

				var obj = scene.getObjectByName(aa,true);
				obj.lookAt(camera.position);
				
			});

			var star = scene.getObjectByName( 'star', true );
			if (typeof star !== 'undefined') {
				
				var star_core = scene.getObjectByName('star_CORE',true);
				if (typeof star_core !== 'undefined') {

					star_core.rotation.y += 0.2 * Math.PI /180;
				
				}
				var shaderTiming = (Date.now() - global.three.render.start )/ 1000;
					star.sunUniforms.time.value = shaderTiming;
					star.haloUniforms.time.value = shaderTiming;
					star.solarflareUniforms.time.value = shaderTiming;

			}

			if (global.three.obj.meta.type == 'node') {

				var node_core = scene.getObjectByName(global.three.obj.origin.node.stub+'-obj',true);
				if (typeof node_core !== 'undefined') {

					node_core.rotation.y += global.three.obj.origin.node.rotation_adjusted;

				}

				var node_atm = scene.getObjectByName(global.three.obj.origin.node.stub+'-atm',true);
				if (typeof node_atm !== 'undefined') {

					node_atm.rotation.y += global.three.obj.origin.node.rotation_adjusted * 1.5;

				}

			}

			// move all nodes
			if (global.three.obj.nodes.length > 0) {

				global.three.obj.nodes.forEach(function(node){

					if (typeof node.nodeObj !== 'undefined') {

						node = that.func.render.node(node,global.three.opt.defaults.speed.nodes);

					}

				});

			}

		}

	    camera.updateMatrixWorld();
	    renderer.render( scene, camera );

	},
	func: {

		render: {

			node:function(node,speed) {

				var that = this;

				if ((node.type === 'rock') || (node.type === 'gas') || (node.type === 'moon')) {

		    		var radians = node.orb_angle * (Math.PI / 180);
		    		
					node.orb_angle -= global.three.opt.defaults.speed.nodes * (1/node.au);

		   			node.nodeObj.position.x = Math.cos(radians) * (node.auXY);
		   			node.nodeObj.position.z = Math.sin(radians) * (node.auUV);
		   			node.nodeObj.position.y = Math.cos(radians) * node.orb_incl;
		   			
		   			node.nodeObj.children.forEach(function(meshLook){
	   				
		   				if (meshLook.name == node.stub+'-obj') {

		   					meshLook.rotation.y += node.rotation_adjusted;	
		   				
		   				}

		   			});
		   			

		   			if (node.subnodes !== false) {

		   				node.subnodes.forEach(function(subnodule){

		   					subnodule = that.node(subnodule,(speed/3));

		   				});

		   			}

	   			} else {

					node.nodeObj.rotation.y += global.three.opt.defaults.speed.nodes * (1/node.au) * (Math.PI / 180);		   			

	   			}

	   			return node;

			}

		},

		skybox: function(sc,textureURI,callback) {

		    // Skybox
		    var geometry = new THREE.SphereGeometry(240000, 60, 40);
		    var uniforms = {

		      texture: { 
		      	type: 't', 
		      	value: THREE.ImageUtils.loadTexture(textureURI) 
		      }

		    };

		    var material = new THREE.ShaderMaterial({

		      uniforms:       uniforms,
		      vertexShader:   document.getElementById('sky-vertex').textContent,
		      fragmentShader: document.getElementById('sky-fragment').textContent
		    
		    });

		    var skyBox = new THREE.Mesh(geometry, material);
		    skyBox.scale.set(-1, 1, 1);
		    skyBox.rotation.order = 'XZY';
		    skyBox.renderDepth = 1000.0;
		    
		    sc.add(skyBox);

		    callback();

		},


	} // func

});