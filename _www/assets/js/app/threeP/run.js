	define({

		star: function(star,opt,callback) {

			var that = this;

			// Lighting
		    var light = new THREE.PointLight('#'+star.color,1,0,0);
		        light.position.set(0, 0, 0);			        
		        scene.add(light);

			var hemLight = new THREE.HemisphereLight(0xffe5bb, 0xFFBF00, 0.1);
			scene.add(hemLight);

			var starbox = new THREE.Object3D();

			var starSize = star.r0 * opt.defaults.size.star;
			star.size_scaled = starSize;
			var starSphereTexture = global.opt.loc.textures+'stars/'+star.color+'.jpg';

			// Textures for main star
			var sunTexture = THREE.ImageUtils.loadTexture(starSphereTexture);
				sunTexture.anisotropy = 1;
				sunTexture.wrapS = sunTexture.wrapT = THREE.RepeatWrapping;
			var sunColorLookupTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+"star_colorshift.png");
			var starColorGraph = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'star_color_modified.png');
			
			// Halo vars
			var sunHaloTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'sun_halo.png');
			var sunHaloColorTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'halo_colorshift.png');

			var solarflareTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'solarflare.png');
			var sunCoronaTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'corona.png');

			var sunUniforms = {
				texturePrimary:   { type: "t", value: sunTexture },
				textureColor:   { type: "t", value: sunColorLookupTexture },
				textureSpectral: { type: "t", value: starColorGraph },
				time: 			{ type: "f", value: 0 },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var solarflareUniforms = {
				texturePrimary:   { type: "t", value: solarflareTexture },
				time: 			{ type: "f", value: 0 },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var haloUniforms = {
				texturePrimary:   { type: "t", value: sunHaloTexture },
				textureColor:   { type: "t", value: sunHaloColorTexture },
				time: 			{ type: "f", value: 0 },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var coronaUniforms = {
				texturePrimary:   { type: "t", value: sunCoronaTexture },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			// Sphere
			//
			var sunShaderMaterial = new THREE.ShaderMaterial( {
				uniforms: sunUniforms,
				vertexShader: document.getElementById('shader-vertex-starsurface').textContent,
				fragmentShader: document.getElementById('shader-fragment-starsurface').textContent,
			});

			var sunSphere = new THREE.Mesh( new THREE.SphereGeometry(starSize, 64, 64), sunShaderMaterial);
			sunSphere.name = 'star_CORE';

			starbox.add(sunSphere);

			var planeSize = starSize*Math.PI;
			var planeGeo = new THREE.PlaneGeometry( planeSize, planeSize );

			// sunHalo
			//
			var sunHaloMaterial = new THREE.ShaderMaterial(
				{
					uniforms:haloUniforms,
					vertexShader:   document.getElementById('shader-vertex-default').textContent,
					fragmentShader: document.getElementById('shader-fragment-starhalo').textContent,
					blending: THREE.AdditiveBlending,
					depthTest: 		true,
					depthWrite: 	false,
					transparent: true,
				}
			);

			var sunHalo = new THREE.Mesh(planeGeo,sunHaloMaterial);
			sunHalo.position.set( 0, 0, 0 );
			sunHalo.name = 'star_HALO';

			sunHalo.lookAt(camera.position);
			global.three.render.gyro.push(sunHalo.name);

			starbox.add(sunHalo);

			// Corona
			//
			var starGlowMat = new THREE.ShaderMaterial(
				{
					map: sunCoronaTexture,
					uniforms: coronaUniforms,
					blending: THREE.AdditiveBlending,
					fragmentShader: document.getElementById('shader-fragment-corona').textContent,
					vertexShader:document.getElementById('shader-vertex-default').textContent,
					transparent: true,
					depthTest: true,
					depthWrite: false,
				}
			);

			var sunGlow = new THREE.Mesh( planeGeo, starGlowMat );
			sunGlow.position.set( 0, 0, 0 );
			sunGlow.scale.x = sunGlow.scale.y = sunGlow.scale.z = sunGlow.scale.x *5;
			sunGlow.name = 'star_GLOW';

			global.three.render.gyro.push(sunGlow.name);
			sunGlow.lookAt(camera.position);

			starbox.add(sunGlow);

			starbox.coronaUniforms = coronaUniforms;
			starbox.sunUniforms = sunUniforms;
			starbox.haloUniforms = haloUniforms;
			starbox.solarflareUniforms = solarflareUniforms;

			//
			// Clusterpoints Init
			//
			var starPoint = new THREE.Object3D();
			starPoint.name = 'clusterpoints'
		    var starPointTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/starpointsprite.jpg');
		    star.cluster_points.forEach(function(cp){
			    var starPointSprite = new THREE.Sprite(

			    	new THREE.SpriteMaterial({
			      		map: starPointTexture,
			      		blending: THREE.AdditiveBlending,
			      		color: ('#'+(Math.random()*0xFFFFFF<<0).toString(16))
			    	})

			   	);

				starPointSprite.position.x = cp.x * opt.defaults.distance.cpoints;
				starPointSprite.position.y = cp.y * opt.defaults.distance.cpoints;
				starPointSprite.position.z = cp.z * opt.defaults.distance.cpoints;

			    starPointSprite.scale.x = opt.defaults.size.cpoints;
			    starPointSprite.scale.y = opt.defaults.size.cpoints;
			    starPointSprite.scale.z = opt.defaults.size.cpoints;
			    starPoint.add(starPointSprite);

		    });
		    star.cluster_point_obj = starPoint;

		    // lensflare
		    flareTextures = [];
		    flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare0.png'));
		    flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare1.png'));
		    flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare2.png'));

		    var flares = that.func.create.lensflare.make(0,0,0,flareTextures,'#'+star.color);

		    scene.add(starPoint);

			starbox.name = 'star';

			star.nodeObj = starbox;
			scene.add(star.nodeObj);

			callback();

		},

		node: function(obj,opt,callback) {

			var that = this;

		    var light = new THREE.PointLight('#'+'ffffff',1,0,0);
		        light.position.set(30000, 0, 0);			        
		        scene.add(light);

			var hemLight = new THREE.HemisphereLight(0xffe5bb, 0xFFBF00, 0.1);
			scene.add(hemLight);

			if ((obj.type === 'rock') || (obj.type === 'gas') || (obj.type === 'moon')) {

				obj = that.func.node(obj,opt,opt.defaults.size.star,opt.defaults.distance.star);

				// Ships and fleets go here
				if (typeof (obj.ships) !== 'undefined') {

					if ((obj.ships.length > 0) && (obj.ships !== false)) {

						obj = that.func.ships.init(obj,'node',opt,obj.type_size_Ro_adjusted,opt.defaults.distance.star,that);

					}

				}

				if ((obj.type_atmosphere != null) && (obj.type_atmosphere_density > 0)) {

					var atmosphere = that.func.create.atmospherics(obj.type_atmosphere,opt,obj.type_atmosphere_density,obj.type_size_Ro_adjusted);

					atmosphere.name = obj.stub+'-atm';
					obj.nodeObj.add(atmosphere);

				}

				scene.add(obj.nodeObj);
			
			}

			callback();

		},

		nodes: function(nodes,opt,callback) {


			var that = this;

			var ellipseContainer = new THREE.Object3D();
			ellipseContainer.name = opt.inf.obj.tracks;
			scene.add(ellipseContainer);

			var AUadj = 1;
			var objReferenceName;
			if (typeof (global.three.obj.meta.type) !== 'undefined') {

				if ((global.three.obj.meta.type == 'star') && (typeof(global.three.obj.origin.star.size_scaled) !== 'undefined')) {

					AUadj = global.three.obj.origin.star.size_scaled;
					objReferenceName = global.three.obj.origin.star.name;

				}
				if ((global.three.obj.meta.type == 'node') && (typeof(global.three.obj.origin.node.type_size_Ro_adjusted) !== 'undefined')) {

					AUadj = global.three.obj.origin.node.type_size_Ro_adjusted;
					objReferenceName = global.three.obj.origin.node.name;

				}

			}

			nodes.forEach(function(obj){

				obj = that.noder(obj,opt,opt.defaults.size.nodes,AUadj,opt.defaults.distance.star,true);

				// obj.nodeObj.transparent = false;

				scene.add(obj.nodeObj);

				// Add Ellipse to Ellipse Container.
				obj.nodeEllipse = that.func.create.ellipse(obj.auXY,obj.auUV);
				obj.nodeEllipse.rotation.x = 90 * Math.PI / 180; // flip!

				// add label to ellipse
				ellipseContainer.add(obj.nodeEllipse);


				obj.nodeLabel = that.func.create.objlabel.populate(objReferenceName + ' - ' + obj.name,{fontface:'ACapt'});
				obj.nodeObj.add(obj.nodeLabel);
				obj.nodeLabel.position.y = obj.type_size_Ro_adjusted + 2;


			});

			callback();

		},

		subnodes: function(parent,opt,distance) {

			var that = this;

			var orbMultiplyer = opt.defaults.orbitalMultiplyer;

			var AUadj = parent.type_size_Ro;
			if (typeof(parent.type_size_Ro_adjusted) !== 'undefined') {

				AUadj = parent.type_size_Ro_adjusted;

			}

			if (typeof distance !== 'undefined') {
			
				distance = opt.defaults.distance.subnodes;
			
			} else {

				distance = opt.defaults.distance.nodes;
			
			}
			
			parent.subnodes.forEach(function(obj){

				obj = that.noder(obj,opt,opt.defaults.size.subnodes,AUadj,distance,false);
				parent.nodeObj.add(obj.nodeObj);

				if ((obj.subnodes.length > 0) && (obj.subnodes !== false)) {

					obj = that.subnodes(obj,opt,distance);

				}
				

			});

			return parent;

		},

		noder: function(obj,opt,size,initialDistance,distance,istopNode) {

			var that = this;

			if ((obj.type === 'rock') || (obj.type === 'gas') || (obj.type === 'moon')) {

				obj = that.func.node(obj,opt,size,initialDistance,distance);

				// Main Object stats (start location)
	    		var radians = obj.orb_angle * (Math.PI / 180);
	   			obj.nodeObj.position.x = Math.cos(radians) * (obj.auXY);
	   			obj.nodeObj.position.z = Math.sin(radians) * (obj.auUV);
	   			obj.nodeObj.position.y = Math.cos(radians) * obj.orb_incl;

			}

			if ((obj.type === 'asteroid') || (obj.type === 'debris')) {

				obj = that.func.prepare(obj,opt,initialDistance,distance);

				var particleColor = '#555555';
				if (obj.type === 'asteroid') {
				
					particleColor = '#' + Math.random().toString(16).substring(2, 8);
				
				}

				if (istopNode === true) {

					size = size * 200;
				}

				particledata = that.func.create.particles(obj,opt,particleColor,size);
				obj.nodeObj = particledata.ps;
				obj.jitter = particledata.js;

			}

			// Ships and fleets go here
			if ((obj.type === 'ship') || (obj.type === 'fleet')) {

				// if ((obj.ships.length > 0) && (obj.ships !== false)) {

				// 	obj = that.func.ships.init(obj,'node',opt,obj.type_size_Ro_adjusted,distance,that);

				// }

			}

			return obj;

		},

		func: {

			create: {

				objlabel: {

					populate: function( message, parameters ) {
						if ( parameters === undefined ) parameters = {};
						
						var fontface = parameters.hasOwnProperty("fontface") ? 
							parameters["fontface"] : "Arial";
						
						var fontsize = parameters.hasOwnProperty("fontsize") ? 
							parameters["fontsize"] : 24;
						
						var borderThickness = parameters.hasOwnProperty("borderThickness") ? 
							parameters["borderThickness"] : 1;
						
						var borderColor = parameters.hasOwnProperty("borderColor") ?
							parameters["borderColor"] : { r:0, g:0, b:0, a:0 };
						
						var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
							parameters["backgroundColor"] : { r:0, g:0, b:0, a:0 };
							
						var canvas = document.createElement('canvas');
						var context = canvas.getContext('2d');
						// context.font = "Bold " + fontsize + "px " + fontface;
					    context.font = fontsize + "px " + fontface;
					    
						// get size data (height depends only on font size)
						var metrics = context.measureText( message );
						var textWidth = metrics.width;
						
						// background color
						context.fillStyle   = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
													  + backgroundColor.b + "," + backgroundColor.a + ")";
						// border color
						context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
													  + borderColor.b + "," + borderColor.a + ")";

						context.lineWidth = borderThickness;
						this.roundRect(context, borderThickness/2, borderThickness/2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
						// 1.4 is extra height factor for text below baseline: g,j,p,q.
						
						// text color
						context.fillStyle = "rgba(255, 255, 255, 1.0)";

						context.fillText( message, borderThickness, fontsize + borderThickness);
						
						// canvas contents will be used for a texture
						var texture = new THREE.Texture(canvas) 
						texture.needsUpdate = true;

						var spriteMaterial = new THREE.SpriteMaterial( 
							{ map: texture, useScreenCoordinates: false } );
						var sprite = new THREE.Sprite( spriteMaterial );
						sprite.scale.set(100,50,1.0);
						return sprite;

					},

					// function for drawing rounded rectangles
					roundRect: function(ctx, x, y, w, h, r) {

					    ctx.beginPath();
					    ctx.moveTo(x+r, y);
					    ctx.lineTo(x+w-r, y);
					    ctx.quadraticCurveTo(x+w, y, x+w, y+r);
					    ctx.lineTo(x+w, y+h-r);
					    ctx.quadraticCurveTo(x+w, y+h, x+w-r, y+h);
					    ctx.lineTo(x+r, y+h);
					    ctx.quadraticCurveTo(x, y+h, x, y+h-r);
					    ctx.lineTo(x, y+r);
					    ctx.quadraticCurveTo(x, y, x+r, y);
					    ctx.closePath();
					    ctx.fill();
						ctx.stroke();   
					
					}

				},

				planetoid: function(texture,radius,stub,offset) {

					node = true;
					// Set Scene
					var node = new THREE.Object3D();
					var planetObj;

					planetObj = new THREE.Mesh(new THREE.SphereGeometry(radius, 64, 64),  new THREE.MeshPhongMaterial({
									map: THREE.ImageUtils.loadTexture(texture)
								}));
		   			
		   			// planetObj.rotation.z = offset;

					planetObj.name = stub + '-obj';
					// node.up = new THREE.Vector3(1,0,0);
					node.name = stub;

					node.add(planetObj);
					
					return node;

				},

				atmospherics: function(texture,opt,count,startRadius) {

					// create destination canvas
					var canvasResult	= document.createElement('canvas');
					canvasResult.width	= 512;
					canvasResult.height	= 256;
					var contextResult	= canvasResult.getContext('2d');

					// load earthcloudmap
					var imageMap	= new Image();
					imageMap.crossOrigin="anonymous";
					imageMap.addEventListener("load", function() {
						
						// create dataMap ImageData for earthcloudmap
						var canvasMap	= document.createElement('canvas')
						canvasMap.width	= imageMap.width
						canvasMap.height= imageMap.height
						var contextMap	= canvasMap.getContext('2d')
						contextMap.drawImage(imageMap, 0, 0)
						var dataMap	= contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height)

						// load earthcloudmaptrans
						var imageTrans	= new Image();
						imageTrans.crossOrigin="anonymous";
						imageTrans.addEventListener("load", function(){
							// create dataTrans ImageData for earthcloudmaptrans
							var canvasTrans		= document.createElement('canvas')
							canvasTrans.width	= imageTrans.width
							canvasTrans.height	= imageTrans.height
							var contextTrans	= canvasTrans.getContext('2d')
							contextTrans.drawImage(imageTrans, 0, 0)
							var dataTrans		= contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height)
							// merge dataMap + dataTrans into dataResult
							var dataResult		= contextMap.createImageData(canvasMap.width, canvasMap.height)
							for(var y = 0, offset = 0; y < imageMap.height; y++){
								for(var x = 0; x < imageMap.width; x++, offset += 4){
									dataResult.data[offset+0]	= dataMap.data[offset+0]
									dataResult.data[offset+1]	= dataMap.data[offset+1]
									dataResult.data[offset+2]	= dataMap.data[offset+2]
									dataResult.data[offset+3]	= 255 - dataTrans.data[offset+0]
								}
							}
							// update texture with result
							contextResult.putImageData(dataResult,0,0)	
							material.map.needsUpdate = true;
						})
						imageTrans.src	= global.opt.loc.textures+'atmospheres/'+texture+'trans.jpg';
					}, false);

					imageMap.src = global.opt.loc.textures+'atmospheres/'+texture+'.jpg';

					var material  = new THREE.MeshPhongMaterial({

						map: new THREE.Texture(canvasResult),
						opacity: 0.5,
						transparent: true,
						depthWrite: false,
						depthTest:false,
						// blending: THREE.AdditiveBlending,

					});

					var rtn = new THREE.Object3D();
					rtn.add(new THREE.Mesh(new THREE.SphereGeometry((count + startRadius), 64, 64), material));

					return rtn;  

				},

				ellipse: function(xy,uv,pColor) {


					if (typeof pColor === 'undefined') {
						pColor = '#337ab7';
					}					

					var curve = new THREE.EllipseCurve(
						0,  0,
						xy, uv,
						0,  2 * Math.PI,
						false,
						0
					);
					var path = new THREE.Path( curve.getPoints( 1000 ) );
					var geometry = path.createPointsGeometry( 1000 );
					var material = new THREE.LineBasicMaterial( { color:pColor, linewidth:3 });

					var ellipse = new THREE.Line( geometry, material );

					return ellipse;


				},

				particles: function(obj,opt,pColor,pSize){

					if (typeof pColor === 'undefined') {
						pColor = '#FFFFFF';
					}

					var particles = new THREE.Geometry();
					var jitter = [];

					// Particle system time
				    var pMaterial = new THREE.PointsMaterial({
				      color: pColor,
				      size: pSize,
					  map: THREE.ImageUtils.loadTexture(global.opt.loc.textures+"nodes/asteroidsprite.png"),
					  blending: THREE.AdditiveBlending,
					  transparent: true,
					  depthTest: true,
					  depthWrite: false,
				    });

					// Main Object stats (start location)
				    for(var p=0;p<obj.opt_asteroids;p++) {

		    			var radians = (Math.random()*360) * (Math.PI / 180);					

		    			var au = {
		    				x: (obj.auXY * ((Math.random() * 200 + 900)/1000)),
		    				y: (obj.orb_incl * ((Math.random() * 400 + 800)/1000)),
		    				z: (obj.auUV * ((Math.random() * 200 + 900)/1000)),
		    				rad: radians,
		    			};

						var pX = Math.cos(radians) * au.x,
						pY = Math.cos(radians) * au.y,
						pZ = Math.sin(radians) * au.z,
						particle = new THREE.Vector3(pX, pY, pZ);
						
						// add it to the geometry
						particles.vertices.push(particle);
						jitter.push(au);

				    }

					// create the particle system
					var particleSystem = new THREE.Points(
					    particles,
					    pMaterial);

					return {ps:particleSystem,js:jitter};

				},

				lensflare : {

					make: function(x,y,z,textures,star_color) {

						var flareColor = new THREE.Color(star_color);
						var overallColor = new THREE.Color('#ffffff');

						var lensFlare = new THREE.LensFlare( textures[0], 700, 0.0, THREE.AdditiveBlending, overallColor );

						lensFlare.add( textures[1], 256, 0.0, THREE.AdditiveBlending);
						lensFlare.add( textures[1], 256, 0.0, THREE.AdditiveBlending);
						lensFlare.add( textures[1], 256, 0.0, THREE.AdditiveBlending);

						lensFlare.add( textures[2], 60, 0.6, THREE.AdditiveBlending,flareColor);
						lensFlare.add( textures[2], 70, 0.7, THREE.AdditiveBlending,flareColor);
						lensFlare.add( textures[2], 120, 0.9, THREE.AdditiveBlending,flareColor);
						lensFlare.add( textures[2], 70, 1.0, THREE.AdditiveBlending,flareColor);

						lensFlare.customUpdateCallback = this.update;
						lensFlare.position.set(x,y,z)

						scene.add( lensFlare );

					},

					update : function(object) {

						var f, fl = object.lensFlares.length;
						var flare;
						var vecX = -object.positionScreen.x * 2;
						var vecY = -object.positionScreen.y * 2;

						for( f = 0; f < fl; f++ ) {

							   flare = object.lensFlares[ f ];

							   flare.x = object.positionScreen.x + vecX * flare.distance;
							   flare.y = object.positionScreen.y + vecY * flare.distance;

							   flare.rotation = 0;

						}

						object.lensFlares[ 2 ].y += 0.025;
						object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );

					}
	
				},


			},

			ships: {

				init: function(obj,obj_type,opt,initialDistance,distance,parent) {

					var that = this;
	    			// 				

	    			// var au = {
	    			// 	x: (obj.auXY * ((Math.random() * 200 + 900)/1000)),
	    			// 	y: (obj._orb_incl * ((Math.random() * 400 + 800)/1000)),
	    			// 	z: (obj.auUV * ((Math.random() * 200 + 900)/1000)),
	    			// 	rad: radians,
	    			// };			

	    			var shipsObj = new THREE.Object3D();
	    			shipsObj.name = obj.stub+'-ships';
					obj.ships.forEach(function(ship){

						// Step 3 : Add Object
						// Parameter 1 : radiusTop
						// Parameter 2 : radiusBottom
						// Parameter 3 : segmentsRadius - Height of cylinder
						// Parameter 4 : segmentsHeight
						// Parameter 5 : openEnded cylinder
						//geometry = new THREE.CylinderGeometry(10, 10, 40, 50, 50, false) ; // Cylinder
						

						var singleShip = new THREE.Object3D();

						geometry = new THREE.CylinderGeometry(0, 1, 3, 50, 50, false) ; // Cone 
						material = new THREE.MeshBasicMaterial( { map: THREE.ImageUtils.loadTexture(global.opt.loc.textures+'nodes/rock-barren-small.jpg' ), overdraw: true } )
						mesh = new THREE.Mesh( geometry, material );
						singleShip.add(mesh);
						singleShip.name = ship.stub;

						var radians = (Math.random()*360) * (Math.PI / 180);
						var mlt = (ship.au* distance) + initialDistance;

						singleShip.position.x = Math.cos(radians) * mlt;
						singleShip.position.y = 0;
						singleShip.position.z = Math.sin(radians) * mlt;

						singleShip.rotation.z = 90 * Math.PI / 180;

						var shipEllipse = parent.func.create.ellipse(mlt,mlt,'#FF0000');
						shipEllipse.rotation.x = 90 * Math.PI / 180;

						shipsObj.add(shipEllipse);
						shipsObj.add(singleShip);

					});


					obj.shipsObj = shipsObj;
					obj.nodeObj.add(shipsObj);

					return obj;

				},

			},

			prepare: function(obj,opt,initialDistance,distance) {

				obj.orb_angle = parseFloat(obj.orb_angle);
				obj.rotation_adjusted = parseFloat(obj.rotation) * Math.PI / 180;

				// Set Planetary Orbital Ellipse Distances
				obj.au_adjusted = obj.au * distance

				obj.auXY = obj.auUV = obj.au_adjusted + initialDistance;

				switch(obj.orb_ellipse_dir) {
					case 0:
						obj.auXY = obj.auXY * obj.orb_ellipse;
						break;
					case 1:
						obj.auUV = obj.auUV * obj.orb_ellipse;
						break;
				}

				return obj;

			},

			node: function(obj,opt,size,initialDistance,distance){

				var that = this;

				obj = this.prepare(obj,opt,initialDistance,distance);

				var pT = obj.type;
				if (pT === 'moon') {
					pT = 'rock';
				}

				var pS = obj.type_surface;
				if (pT === 'gas') {
					pS = obj.type_atmosphere;
				}

				var tName = 'nodes/'+pT+'-'+pS+'-small.jpg';
				var PlanetTexture = global.opt.loc.textures+tName;
				if (typeof obj.texture !== 'undefined') {
					
					if (obj.texture != null) {

						obj.texture = 'users/'+obj.texture;
						PlanetTexture = global.opt.loc.textures+obj.texture;
					
					} else {

						obj.texture = tName;

					}
					
				} else {

					obj.texture = tName;

				}

				if (typeof (obj.type_size_Ro) === 'undefined') {
					obj.type_size_Ro = 1;
				}

				obj.type_size_Ro_adjusted = obj.type_size_Ro * size;

				obj.rotation_offset_adjusted = obj.rotation_offset * (1/Math.PI) 

				var node = that.create.planetoid(PlanetTexture,obj.type_size_Ro_adjusted,obj.stub,obj.rotation_offset_adjusted);
				obj.nodeObj = node;

				return obj;

			},

		},



// RING!
//
// if (obj._opt_hasring == 1) {
// var ring = that.create.ring((obj.type_size_Ro * s.scale.nodes),obj._opt_ring_count,obj._opt_ring_offset);
// node.add(ring);
// }

// LABELS
//
// Labels before subnodes plz
// ThreeSystem.generate.labels(planet,opt.defaults);

// OLD FUNCTION
//
// _star: function(star,textures,s,callback) {

// 	// Lighting
//     var light = new THREE.PointLight('#'+star.color, 1, 0,1);
//         light.position.set(0, 0, 0);			        
//         scene.add(light);


// 	var hemLight = new THREE.HemisphereLight(0xffe5bb, 0xFFBF00, 0.1);
// 	scene.add(hemLight);

// 	// texture load
//     var starTexture = THREE.ImageUtils.loadTexture('/assets/img/textures/stars/sun_yellow_medium.jpg');
//     var starSprite = new THREE.Sprite(

//     	new THREE.SpriteMaterial({
//       		map: starTexture,
//       		blending: THREE.AdditiveBlending,
//       		color: '#'+star.color
//     	})

//    	);

// 	starSprite.position.x = 0;
// 	starSprite.position.y = 0;
// 	starSprite.position.z = 0;

//     starSprite.scale.x = s.scale.star;
//     starSprite.scale.y = s.scale.star;
//     starSprite.scale.z = s.scale.star;
//     scene.add(starSprite);


// 	// texture load
//     var nt = THREE.ImageUtils.loadTexture('/assets/img/textures/stars/sun-1.png');
//     var n = new THREE.Sprite(

//     	new THREE.SpriteMaterial({
//       		map: nt,
//       		blending: THREE.AdditiveBlending,
//       		color: '#'+star.color
//     	})

//    	);

// 	n.position.x = 0;
// 	n.position.y = 0;
// 	n.position.z = 0;
//     n.scale.x = s.scale.star*100;
//     n.scale.y = s.scale.star*100;
//     n.scale.z = s.scale.star*100;
//     scene.add(n);

//     callback();

// },


	});
