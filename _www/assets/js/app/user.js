requirejs(['init'],function(i) {

	requirejs(['jquery','bootstrap','jscookie','global','conf.user','mustache','enscroll','micromarkdown'],function($,bootstrap,jscookie,gg,ud,Mustache,es,micromarkdown) {

		var gud = global.user.data = ud;

		var cookieData = jscookie.get('user');
		if (typeof(cookieData) !== 'undefined') {

			var cD = JSON.parse(cookieData);

			global.user.key = cD.key;
			global.user.name = cD.name;

		}
		var userEmit = global.user.call();

		global.user.cluster = false;
		var cookieClusterData = jscookie.get('user_cluster') 
		if (typeof cookieClusterData !== 'undefined') {

			var cE = JSON.parse(cookieClusterData);
			global.user.cluster = cE.cluster;

		}
		userEmit.cluster = global.user.cluster;
		global.socket.emit('user',userEmit);

		global.socket.on('user_data',function(aa){

			if (aa.ok === true) {

				// chat
				$(gud.chat.dom.box).remove();

				aa.channels.forEach(function(ta){
					ta.log.forEach(function(as){
						as.ddt = gud.chat.run.dateConvert(as.dt);
					});
				});

				var rendered = Mustache.render($(gud.chat.dom.template).html(),aa);
				$(global.dom.container).append(rendered);

				$(gud.chat.dom.box).hide();
				if (typeof (jscookie.get('chat')) !== 'undefined'){
					var chatCookie = JSON.parse(jscookie.get('chat'));
					if (chatCookie.toggle === true) {
						gud.chat.run.show(gud);
						$('#chat-box-nav > .nav-tabs a:first').tab('show');
					}
				}
				$('#chat-box-nav > .nav-tabs a:first').tab('show');

				// Needs to be made a function
				$(gud.nav.dom.stats).remove();
				var rendered = Mustache.render($(gud.nav.tmpl.box).html(),{id:gud.nav.dom.stats.substring(1)});
				$(global.dom.container).append(rendered);
				$(gud.nav.dom.stats_container).enscroll();
				$(gud.nav.dom.stats).hide();

				$(gud.nav.dom.nodes).remove();
				var rendered = Mustache.render($(gud.nav.tmpl.box).html(),{id:gud.nav.dom.nodes.substring(1)});
				$(global.dom.container).append(rendered);
				$(gud.nav.dom.nodes_container).enscroll();
				$(gud.nav.dom.nodes).hide();

				aa.nodes.forEach(function(az){
					
					var navNodeData = {
						stub:az.stub,
						texture:global.opt.loc.textures+'icons/'+az.type+'.jpg',
						name:az.star + ' - ' + az.name,
					};
					var rendered = Mustache.render($(gud.nav.tmpl.nodes_data).html(),navNodeData);
					$(gud.nav.dom.nodes_container).append(rendered);

				});

			} else {

				// error handling here

			}


		});

		global.socket.on('chat_data', function (a) {
			console.log(a);
			var passMsg = '';
			if (a.t == 1) {
				a.ddt = gud.chat.run.dateConvert(a.dt);
				passMsg = Mustache.render($(gud.chat.dom.templateline).html(),a);
				$('#chat-channel-'+a.ch).append(passMsg);
				$('#chat-channel-'+a.ch).animate({scrollTop: $('#chat-channel-'+a.ch).prop('scrollHeight')});
			}

		});

		global.socket.on('action_data',function(a) {

			var gud = global.user.data;

			if (a.ok != true) {

				console.log('Action_Data Error');
				console.log(a);
				// error handling here
			
			} else {

				switch(a.type) {
	
					case 'node_rename':
							location.reload(true);
						break;
					case 'node_buildings':

							gud.actions.data.buildings = a.res.buildings;

							a.res.tdir = global.opt.loc.textures+'buildings/';
							var rendered = Mustache.render($(gud.actions.dom.mustache.node_buildings).html(),a.res);
							$("#action-box-"+a.type+" > .action-box").html(rendered);

							console.log(a.res);

						break;
					case 'node_shipyard':

						var acttemplate = $(gud.actions.dom.mustache.node_shipyard).html();
			  			var actRend = Mustache.render(acttemplate,{});

			  			$("#action-box-"+a.type+" > .action-box").html(actRend);
			  			$("#action-box-"+a.type+" > .action-box").enscroll();	

						console.log(a.res);


						break;
					case 'node_buildings_put':
			  			
			  			var targetAction = {
			  				id:'node_buildings',
			  				type:'node_buildings',
			  				data:global.three.obj.meta.request,
			  				user:global.user.call(),
			  			};
			  			global.socket.emit('action',targetAction);

						break;
					case 'storyboard_add':
			    		global.socket.emit('storyboard',{id:global.three.obj.meta,user:global.user.call()});
						break;
					case 'storyboard_delete':
			    		global.socket.emit('storyboard',{id:global.three.obj.meta,user:global.user.call()});
			    		break;
					case 'storyboard_edit_get':

						if (a.res.clean !== true) {

							var storyMM = micromarkdown.parse(a.res.text);
							$(global.three.opt.inf.dom.story_data).find('.story-'+a.res.id).find('.story-text').html(storyMM);
						
						} else {

							var thisStory = $(global.three.opt.inf.dom.story_data).find('.story-'+a.res.id).find('.story-text');
							if ((typeof thisStory !== 'undefined') && (thisStory.length > 0)) {
								
								var rendered = Mustache.render($(gud.storyboard.tmpl.item_edit).html(),{id:a.res.id,text:a.res.text});
								$(thisStory).html(rendered);

							}

						}

						break;
					case 'node_settings':

						var actRend = "";
						var gto = global.three.obj;

						var acttemplate = $(gud.actions.dom.mustache.node_settings).html();

						var passToTMPL = {
							orig:{
								name:gto.origin.node.name_orig,
								stub:gto.origin.node.stub_orig,
							},
							current:{
								name:gto.origin.node.name,
								stub:gto.origin.node.stub,

							},
							img: {
								src: global.opt.loc.textures + gto.origin.node.texture,
							}
						};

			  			var actRend = Mustache.render(acttemplate,passToTMPL);

			  			$("#action-box-"+a.type+" > .action-box").html(actRend);
			  			$("#action-box-"+a.type+" > .action-box").enscroll();			  										
						break;

				}

			}
			
		});

		// newest notifications
		global.socket.on('notify_data',function(ab){

			console.log(ab);

		});

		global.socket.on('stats_data',function(ad){

			var rendered = Mustache.render($(gud.nav.tmpl.stats_credits).html(),{credits:ad.credits,xp:ad.xp});
			$(gud.nav.dom.nav_credits).html(rendered);

			$(gud.nav.dom.stats_container).html('');
			for (adkey in ad) {

				if ((adkey != 'level') && (adkey != 'xp') && (adkey != 'credits') ) { 

					var PassStatsToTmpl = {
						title:adkey,
						texture:global.opt.loc.textures + 'icons/'+adkey+'.jpg',
						value:ad[adkey],
					}

					var adR = Mustache.render($(gud.nav.tmpl.stats_data).html(),PassStatsToTmpl);
					$(gud.nav.dom.stats_container).append(adR);

				}

			}

		});

		// number of unread messages
		global.socket.on('message_data',function(ac){

			console.log(ac);

		});

		//
		// JQUERY STORYBOARD
		//

		// STORYBOARD SUBMIT
		$('body').on('submit',gud.storyboard.dom.form,function(event) {

			event.preventDefault();
			
			//
			// type: -> must be changed if it's a ship or fleet.
			//

			var data = {
				user:global.user.call(),
				text:$(gud.storyboard.dom.form).find('textarea').val(),
				type:global.three.obj.meta.type,
				origin:global.three.obj.meta.request,
				id:'storyboard_add'
			};
			global.socket.emit('action',data);

			$(gud.storyboard.dom.form).find('textarea').val('');

		});

		// STORYBOARD FORM TOGGLE
		$('body').on('click',gud.storyboard.dom.form_toggle,function() {

			if ($(gud.storyboard.dom.form).is(":visible")) {

				$(gud.storyboard.dom.form).hide();

			} else {

				$(gud.storyboard.dom.form).show();

			}

		});

		// STORYBOARD FORM EDIT
		$('body').on('click',gud.storyboard.dom.item_edit,function(e) {
			
			e.preventDefault();
			
			var data_id = $(this).data('id');
			if (typeof data_id !== 'undefined') {
				var cleanType = false;
				if ($(this).hasClass('editing-now')) {



					$(this).removeClass('editing-now');

				} else {

					$(this).addClass('editing-now');
					cleanType = true;

				}
					global.socket.emit('action',{id:'storyboard_edit_get',request:data_id,user:global.user.call(),clean:cleanType});

			}


		});

		$('body').on('submit',gud.storyboard.dom.item_edit_form,function(e) {
			
			e.preventDefault();
			
			var data_id = $(this).data('id');
			if (typeof data_id !== 'undefined') {

				var textVar = $(global.three.opt.inf.dom.story_data).find('.story-'+data_id).find('textarea').val();
				if ((typeof textVar !== 'undefined') && (textVar.length > 0)) {

					global.socket.emit('action',{id:'storyboard_edit',request:{id:data_id,text:textVar},user:global.user.call()});

				}
				

			}

		});

		$('body').on('click','.'+gud.storyboard.dom.item_delete_confirm,function(e) {

			e.preventDefault();
			
			var data_id = $(this).data('id');
			if (typeof data_id !== 'undefined') {

				global.socket.emit('action',{id:'storyboard_delete',request:data_id,user:global.user.call()});

			}

		});

		$('body').on('click',gud.storyboard.dom.item_delete,function(e) {

			e.preventDefault();
			
			var data_id = $(this).data('id');
			if (typeof data_id !== 'undefined') {

				if ($(this).hasClass('del-shown')) {

					$(this).popover('hide');
					$(this).removeClass('del-shown');


				} else {

					$(this).popover({
						trigger: 'manual',
						html:true,
						content:function(){
							return 'Are you sure? <a style="cursor:pointer;" data-id="'+data_id+'" class="'+gud.storyboard.dom.item_delete_confirm+'">Yes</a>.';
						}
					});
					$(this).popover('show');
					$(this).addClass('del-shown');


				}



			}


		});

		//
		// JQUERY NAV CALLS
		//

		$('body').on('click',gud.nav.dom.nodes_toggle,function() {

			if ($(gud.nav.dom.nodes).is(":visible")) {

				$(gud.nav.dom.nodes).hide();

			} else {

				$(gud.nav.dom.nodes).show();

			}

		});

		$('body').on('click',gud.chat.dom.toggle,function() {

			if (gud.chat.data.toggled === false) {
				gud.chat.run.show(gud);
				jscookie.set('chat',{toggle:true});
			} else {
				gud.chat.run.hide(gud,jscookie);
			}

		});


		// STATS TOGGLE
		$('body').on('click',gud.nav.dom.stats_toggle,function() {

			if ($(gud.nav.dom.stats).is(":visible")) {

				$(gud.nav.dom.stats).hide();

			} else {

				$(gud.nav.dom.stats).show();

			}

		});

		$(global.dom.container).on('submit',gud.chat.dom.form,function(event){

			event.preventDefault();

			var socketData = global.user.call();

			socketData.m = $(gud.chat.dom.input).val();
			socketData.ch = gud.chat.data.active;

			global.socket.emit('chat', socketData);
			$(gud.chat.dom.input).val('');

			return false;

		});


		//
		// ACTION WINDOWS
		//

		$('body').on('click','.action-collapser',function(e) {

			e.preventDefault();
			var tgt = $(this).data('tgt');
			if (typeof tgt !== 'undefined') {

				if ($('#'+tgt).is(":visible")) {
					$('#'+tgt).hide();
				} else {
					$('#'+tgt).show();
				}

			}

		});

		$('body').on('submit','#action-node-rename',function(e){
			e.preventDefault();

			var nameData = {
				id:'node_rename',
				user:global.user.call(),
				data:{},
			};

			var txt = $(this).find('input').val();
			if (txt.length > 0) {

				nameData.data.txt = txt;
				nameData.data.node = global.three.obj.origin.node.stub;

				global.socket.emit('action',nameData);

			}

		});

		$('body').on('submit','#action-node-map',function(e) {

			e.preventDefault();

			var map = $('#action-node-map')[0];
	        var fd = new FormData(map);

	        var iput = $("#action-node-map-file")[0];

	        fd.append("user", JSON.stringify(global.user.call()));
 			fd.append(iput.name,iput.files[0]);
 			fd.append('node',global.three.obj.origin.node.stub);

	        $.ajax({
	            url: global.opt.loc.api+'/user/texture',
	            type: 'POST',
	            data: fd,
	            success:function(data){
	                location.reload(true);
	            },
	            error:function(data,err){
	            	console.log(data);
	            	console.log(err);
	            },
	            cache: false,
	            contentType: false,
	            processData: false
	        });

		});

		$('body').on('click','.action-buildings-buy-expand',function(e){

			e.preventDefault();

			var id = $(this).data('id');
			if (typeof id !== 'undefined') {

				if ($(this).hasClass('bbe-pop')) {

					$(this).popover('hide');
					$(this).removeClass('bbe-pop');

				} else {

					$(this).popover({
						trigger: 'manual',
						html:true,
						content:function(){

							var rendered = Mustache.render($(gud.actions.dom.mustache.node_buildings_popover).html(),{id:id});
							return rendered;

						}
					});
					$(this).popover('show');
					$(this).addClass('bbe-pop');
				}

			}

		});

		$('body').on('click','.action-buildings-destroy',function(e){

			e.preventDefault();

			var id = $(this).data('id');
			if (typeof id !== 'undefined') {

				var bld = $.grep(gud.actions.data.buildings, function(e){ return e.id == id; });
				if (typeof bld !== 'undefined') {

					var max_min = -1 * parseInt(bld[0].amount);
					var numStr = $('.bbm-lister-'+id).text();
					var numInt = parseInt(numStr);
					
					var newNumInt = numInt-1;
					if (newNumInt < max_min) {
						newNumInt = max_min;
					}
					$('.bbm-lister-'+id).text(newNumInt);

					$('.action-buildings-btn-buy-costinger-'+id).text('');
					if (newNumInt > 0) {

						var bldcc = 0, passTxt = '';
						bld[0].costs.forEach(function(rtc){

							var forCost = 0;
							// for (a=0;a<numInt;a++) {
							// 	forCost += Math.round(rtc.cost * Math.pow(rtc.next,a));
							// }	

							var racost = 0;
							for (a=0;a<newNumInt;a++) {
 								racost += Math.round(rtc.cost * Math.pow(rtc.next,a));

							}
							racost = racost - forCost;

							var htmlRa = gud.actions.run.iconify(rtc.type) + ' ' + racost + ' ';
							if ((bldcc > 0) && (bldcc < bld[0].costs.length)) {
								htmlRa += '- ';
							}

							bldcc++;
							passTxt += htmlRa;

						});
						$('.action-buildings-btn-buy-costinger-'+id).html(passTxt);

						$('.action-buildings-btn-buy-costinger-'+id).css({'color':'orange'});
						$('.action-buildings-btn-buy-costinger-'+id).show();

					} else {

						if (newNumInt < 0) {

							var bldcc = 0, passTxt = '';
							bld[0].costs.forEach(function(rtc){

								var racost = 0;
								for (a=0;a<newNumInt;a++) {
	 								racost += Math.round(rtc.destroy * Math.pow(rtc.next,a));

								}

								var htmlRa = gud.actions.run.iconify(rtc.type) + ' ' + racost + ' ';
								if ((bldcc > 0) && (bldcc < bld[0].costs.length)) {
									htmlRa += '- ';
								}

								bldcc++;
								passTxt += htmlRa;

							});
							
							$('.action-buildings-btn-buy-costinger-'+id).html(passTxt);
							$('action-buildings-btn-buy-costinger-'+id).css('color','red');
							$('.action-buildings-btn-buy-costinger-'+id).show();

						} else {

							$('.action-buildings-btn-buy-costinger-'+id).hide();

						}


					}

				}

			}

		});

		$('body').on('click','.action-buildings-purchase',function(e){

			e.preventDefault();

			var id = $(this).data('id');
			if (typeof id !== 'undefined') {

				var bld = $.grep(gud.actions.data.buildings, function(e){ return e.id == id; });
				if ((typeof bld !== 'undefined') && (bld.length > 0)) {

					var numStr = $('.bbm-lister-'+id).text();
					var numInt = parseInt(numStr) +1;
					var newNumIntTot = numInt + bld[0].amount;
					$('.bbm-lister-'+id).text(numInt);

					$('.action-buildings-btn-buy-costinger-'+id).text('');
					if (numInt > 0) {

						var bldcc = 0, passTxt = '';
						bld[0].costs.forEach(function(rtc){

							var forCost = 0;
							for (a=0;a<bld[0].amount;a++) {
								forCost += Math.round(rtc.cost * Math.pow(rtc.next,a));
							}	

							var racost = 0;
							for (a=0;a<newNumIntTot;a++) {
 								racost += Math.round(rtc.cost * Math.pow(rtc.next,a));

							}
							racost = racost - forCost;

							var htmlRa = gud.actions.run.iconify(rtc.type) + ' ' + racost + ' ';
							if ((bldcc > 0) && (bldcc < bld[0].costs.length)) {
								htmlRa += '- ';
							}

							bldcc++;
							passTxt += htmlRa;

						});
						$('.action-buildings-btn-buy-costinger-'+id).html(passTxt);
						$('.action-buildings-btn-buy-costinger-'+id).css({'color':'orange'});
						$('.action-buildings-btn-buy-costinger-'+id).show();

					} else {

						if (numInt < 0) {

							var bldcc = 0, passTxt = '';
							bld[0].costs.forEach(function(rtc){

								var racost = 0;
								for (a=0;a<(numInt*-1);a++) {
	 								racost += Math.round(rtc.destroy * Math.pow(rtc.next,a));

								}

								var htmlRa = gud.actions.run.iconify(rtc.type) + ' ' + racost + ' ';
								if ((bldcc > 0) && (bldcc < bld[0].costs.length)) {
									htmlRa += '- ';
								}

								bldcc++;
								passTxt += htmlRa;

							});
							
							$('.action-buildings-btn-buy-costinger-'+id).html(passTxt);
							$('action-buildings-btn-buy-costinger-'+id).css('color','red');
							$('.action-buildings-btn-buy-costinger-'+id).show();

						} else {

							$('.action-buildings-btn-buy-costinger-'+id).hide();

						}


					}



					// v

				}

			}

		});

		$('body').on('submit','.action-buildings-btn-buy-form',function(e){

			e.preventDefault();

			var id = $(this).data('id');
			if (typeof id !== 'undefined') {

				var num = $(this).find('.bbm-lister').text();
				console.log(num);

				global.socket.emit('action',{
					user:global.user.call(),
					id:'node_buildings_put',
					data:{
						id:id,
						mutate:parseInt(num),
						node: global.three.obj.meta.request,
					}
				});

			}

			

		});

		$(global.dom.container).on('shown.bs.tab', 'a[data-toggle="tab"]', function (b) {

				var target = $(b.target).attr("href")
				gud.chat.data.active = target.split("-")[2];
		
		});


	}); // END SCRIPT


});
