require.config({

    baseUrl: '/assets/js/lib',

    paths: {
        app: '../app',
        vendor: '../../vendor',
        mustache: '/assets/vendor/node_modules/mustache/mustache.min',
        jscookie: '/assets/vendor/js.cookie/js.cookie',
        socket: '//cdn.socket.io/socket.io-1.2.0',
        bootstrap: '/assets/vendor/bootstrap-3.3.5-dist/js/bootstrap.min',
        jquery:'//code.jquery.com/jquery',
        require:'/assets/js/require',
        opt: '../opt',
        three: '/assets/vendor/three/three.min',
        OrbitalControls: '/assets/vendor/three/OrbitControls',
        threep:'/assets/js/app/threeP',
        enscroll:'/assets/vendor/enscroll-0.6.1/enscroll-0.6.1.min',
        micromarkdown:'/assets/vendor/micromarkdown/micromarkdown.min',

    },
    shim : {

        bootstrap : { "deps" :['jquery'] },
        socket : { exports: 'io' },
        three: { exports: 'THREE' },
        enscroll: {'deps':['jquery']},
    
    },    

});