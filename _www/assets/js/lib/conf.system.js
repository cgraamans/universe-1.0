define({

	valid: {

		ctrl:['cluster','star','node','ship','fleet'],

	},
	defaults: {

		orbitalMultiplyer: 10,
		ringSpace: 2,

		speed: {
			nodes: 0.1,
			subnodes: 0.1
		},
		distance: {
			star: 1000,
			nodes: 10,
			subnodes: 1,
			cpoints:2000,
		},
		size: {
			star: 100,
			nodes: 4,
			subnodes: 2,
			cpoints:5000,
		},

		container: 'render-box',
		skybox: '/assets/img/textures/skybox/c2.jpg',
		texture: {
			star: {
				colorshift:'/stars/star_colorshift.png',
				colorgraph:'/stars/star_color_modified.png',
				halo:'/stars/sun_halo.png',
				haloshift:'/stars/halo_colorshift.png',
				flare:'/stars/solarflare.png',
				corona:'/stars/corona.png'
			},
		},
	},
	inf:{
		dom: {
		
			buttons:'#system-menu-righttop',
			stats:'#system-menu-rightbottom',
			stats_nav:'#stats-nav',
			stats_box:'#stats-box',
			story_box:'#storyboard',
			story_data:'#storyboard-data',
			story_form:'#storyboard-form',
			story_icons:'#storyboard-icons',		
		},
		obj:{
		
			tracks: 'Ellipse-Container',
			labels: 'Label-Container',
		
		},
		tmpl:{
			
			stats_box_star: '#tmpl-stats-star',
			stats_box_node: '#tmpl-stats-node',
			stats_box_cluster: '#tmpl-stats-cluster',
			stats_box: '#tmpl-stats',
			stats_nav: '#tmpl-stats-nav',
			storyboard_box: '#tmpl-storyboard',
			storyboard_item: '#tmpl-storyboard-item',
			storyboard_user_icons: '#tmpl-storyboard-userIcons',

		}
	},

});