define({

	nav: {

		dom: {
			
			endeavour:'#nav-endeavour',
			notify:'#nav-notify',
			toggle:'.inline-topmenu',
			nav_credits:'#nav-credits',
			stats:'#nav-stats',
			stats_container:'#nav-stats-data',
			stats_toggle:'#nav-stats-toggle',
			nodes:'#nav-nodes',
			nodes_container:'#nav-nodes-data',
			nodes_toggle:'#node-menu-toggle',

		},
		tmpl: {

			stats_credits:'#tmpl-u-nav-stats-credits',
			box:'#tmpl-u-nav-box',
			nodes_data:'#tmpl-u-nav-nodes-data',
			stats_data:'#tmpl-u-nav-stats-data',

		},
		data:{
			stats_toggle:false,
		},

	},

	storyboard: {

		dom: {

			form:'#storyboard-form-add',
			form_toggle:'#storyboard-open-add',
			item_edit:'.storyboard-item-edit',
			item_edit_form:'.storyboard-form-edit',
			item_delete:'.storyboard-item-del',
			item_delete_confirm:'storyboard-item-deleteY',
			popover_dismiss:'storyboard-popover-byebye',

		},
		tmpl: {

			form:'#tmpl-u-storyboard-add',
			item_edit:'#tmpl-u-storyboard-edit',
		},

	},

	chat: {

		dom: {
			form:'#chat-form',
			input:'#chat-form > input',
			channels:'#chat-channel-list',
			box:'#chat-box',
			toggle:'#chat-menu-toggle',
			template:'#tmpl-chat-container',
			templatebtn:'#tmpl-chat-button',
			templateline: '#tmpl-chat-line',
		},

		data:{
			channels:[],
			toggled:false,
			active:'general'
		},

		run: {
			show: function(gud) {
				
				$(gud.chat.dom.box).show();
				$(gud.chat.dom.box).css('z-index', '100');
				gud.chat.data.toggled = true;
				
				return gud;
			
			},
			hide: function(gud,Cookies) {
				
				$(gud.chat.dom.box).css('z-index', '0');
				$(gud.chat.dom.box).hide();
				gud.chat.data.toggled = false;

				Cookies.set('chat',{toggle:false});

				return gud;

			},
			dateConvert: function(dt) {
				dd = new Date(dt * 1000);

				return {
				   h:(dd.getHours() < 10 ? '0'+dd.getHours() : dd.getHours()),
				   m:(dd.getMinutes() < 10 ? '0'+dd.getMinutes() : dd.getMinutes()),
				   s:(dd.getSeconds() < 10 ? '0'+dd.getSeconds() : dd.getSeconds())
		   		};	

			}

		}

	},

	actions: {

		run: {

			iconify: function(type) {

				var icon = false;
				switch(type) {
					case 'crd':
					icon = '<i class="fa fa-credit-card">';
					break;
				case 'ind':
					icon = '<i class="fa fa-industry">';
					break;
				case 'agr':
					icon = '<i class="fa fa-tree">';
					break;
				case 'sci':
					icon = '<i class="fa fa-flask">';
					break;
				case 'mil':
					icon = '<i class="fa fa-bolt">';
					break;																											
				}

				return icon;

			},

		},

		data: {

			buildings:[],
			hulls:[],

		},

		dom:{
			
			mustache: {

				global:"#tmpl-u-actions",
				buttons:"#tmpl-u-actions-box-btn",

				node_settings:'#tmpl-u-actions-box-node-settings',
				node_buildings:'#tmpl-u-actions-box-node-buildings',
				node_buildings_popover:'#tmpl-u-actions-box-node-buildings-buy-more',
				node_shipyard:'#tmpl-u-actions-box-node-shipyard',

			},

			elem: {

				node_buildings: '#actions-box-node-buildings-listing',
				node_shipyard_launchlist: '#actions-box-node-shipyard-launchlist',
				node_shipyard_hulllist: '#actions-box-node-shipyard-hulllist',

			},
			buttons: {
				
				size:'38', // px
				icons: {

					node_buildings:'fa-building',
					node_settings:'fa-wrench',
					node_shipyard:'fa-rocket',
				
				},
				
			},
		
		},

	},
		
});