var global = {
	dom: {
		container:'.default-page'
	}
		
};

define(['socket','opt'], function(io,opt){
	
	global.user = {

		call: function() {

			var rtn = {};

			if ((typeof (this.name) !== 'undefined') && (typeof (this.key) !== 'undefined')) {
				rtn = {name: this.name, key: this.key};
			}

			return rtn;

		}

	};

	global.opt = opt;
	global.socket = io(opt.loc.websocket,{'connect timeout': 1000});

});
