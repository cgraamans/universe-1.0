define({
	loc: {
		api:'http://api.universe',
		websocket:'http://api.universe:8080',
		textures:'http://api.universe/data/textures/',
	},
	iface:{
		auth: {
			login:'#login',
			register:'#register',
		},
		modals: {
			proc: {
				box: '#modal-process',
				text: '#main-box-process-content',
				span_err: 'global-modal-err',
				span_pop: 'global-modal-txt',				
			}
		}

	}
	
});