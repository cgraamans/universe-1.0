exports = module.exports = function(sockey,$state,route,callback) {

	return function(data) {

		var timings = {timers:[],intervals:[]},
			 rtn = {ok:false,data:false},
			 star = false;

		if (typeof data !== 'object') {

			rtn.err = 'Bad data passed to Cluster.';

		}

		if (typeof data.cluster === 'undefined') {

			rtn.err = 'No Cluster Name Defined.';

		}

		if (typeof rtn.err !== 'undefined') {
				
			$state.socket.emit(route+sockey.opt.socket.error,rtn);
			callback(timings);				

		} else {

			rtn.data = {};
			var model = require(sockey.opt.rootDir + '/models/cluster');
			
			sockey.modules.async.parallel([

				function(cb){

					// get all cluster data
					sockey.db.get($state,model._sql.getClusterData,data.cluster,function(r){

						if (r.ok !== true) {

							cb(r.err);

						} else {
						
							if (r.res.length == 1) {

								rtn.data.cluster = r.res[0];	
								cb();

							} else {

								cb('Cluster not found');

							}
							
						}

					});			

				},
				function(cb) {

					model.getFocus(sockey,$state,data,function(r){

						if (r !== false) {
							
							star = r;
							cb();

						} else {

							cb('Star not found');

						}

					});

				}

			],
			function(err){

				if (err) {

					rtn.err = err;
					
					$state.socket.emit(route+sockey.opt.socket.error,rtn);
					callback(timings);
		
				} else {

					if (rtn.data !== false) {

						model.starWalk(sockey,$state,star,data.cluster,function(r){

							if (r.ok !== true) {

								rtn.err = r.err;
						
								$state.socket.emit(route+sockey.opt.socket.error,rtn);
								callback(timings);

							} else {

								rtn.data.map = r.res,
								rtn.ok = true;

								$state.socket.emit(route+sockey.opt.socket.data,rtn);
								callback(timings);

							}

						});

					} else {

						$state.socket.emit(route+sockey.opt.socket.error,rtn);
						callback(timings);		
					
					}

				}

			});

		}

	}

};