exports = module.exports = function(sockey,$state,sock,callback) {


	return function(data) {
		// Note: Data is the request object passed by the socket.
		var timing = {timers:[],intervals:[]};

		var model = require('../models/node'),
			rtn = {
				ok:false,
				err:false
			},
			token = false;

		if (typeof data.star === 'undefined') {

			rtn.err = "No Star Passed";
			if (data.star.length < 1) {
				rtn.err = "No Star Passed";
			}

		}

		if (typeof data.token !== 'undefined') {
	
			if (data.token !== false) {
				
				token = data.token;

			}
	
		}

		if (rtn.err !== false) {
			
			$state.socket.emit(sock,rtn);
			callback(timing);

		} else {

			var res = false;

			model.getNodes($state,sockey,data.star,function(v){
				
				if (v.ok === true) {

					rtn.res = v.res,
					rtn.ok = true;

				} else {

					rtn.err = v.err;

				}
				$state.socket.emit(sock,rtn);

				callback(timing);

			});

		}
	}

};