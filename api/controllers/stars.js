exports = module.exports = function(sockey,$state,sock,callback) {


	return function(data) {

		console.log('data');
		console.log(data);

		var timing = {timers:[],intervals:[]};

		var model = require('../models/star'),
			rtn = {
				ok:false,
			},
			token = false;

		if (typeof data.star === 'undefined') {

			rtn.err = "No Star Passed";

		}

		if (typeof data.token !== 'undefined') {
	
			if (data.token !== false) {
				
				token = data.token;

			}
	
		}

		if (typeof rtn.err !== 'undefined') {
			
			$state.socket.emit(sock,rtn);
			callback(timing);

		} else {

			try {

				model.getStar($state,sockey,data.star,function(v){

						
					if (v.ok !== false) {

						rtn.ok = true,
						rtn.res = v.res;

					} else {

						rtn.err = v.err;
				
					}

					$state.socket.emit(sock,rtn);
					callback(timing);

				});

			} catch (err) {

				rtn.err = err;

				$state.socket.emit(sock,rtn);
				callback(timing);
	
			}

		}

	}

};