module.exports = {

	create: function(sockey,$state,callback) {
		
		var rtn = {ok:false,err:false};
		try {

			sockey.db.get($state,"SELECT id, name FROM data_clusters ORDER BY RAND() LIMIT 1",[],function(r) {
				
				rtn = r;
				callback(rtn);

			});

		} catch(err) {

			rtn.err = err;
			callback(rtn);

		}

	},

}