module.exports = {

	_validNodeTypes: [
		'moon',
		'rock',
		'gas'
	],

	_init: function() {

		try {

			if (!this.func) {
				this.func = require('./functions');
			}
			if (!this.dice) {
				this.dice = require('./dice');
			}
			rtn = this.func.rtnVar();

		} catch(e) {

			rtn.err = "INIT ERROR in NPC BUILDER";

		}
		return rtn;

	},

	nameLeader: function(data) {

		var rtn = this._init(),
			that = this;

		if (rtn.err === false) {

			try {

				rtn.res = {
					gender:that.dice.d100(50) ? "female" : "male",
					type:that.dice.roll({min:1,max:3})
				};
				
				rtn.res.name = data.firstNames[rtn.res.gender][Math.floor(Math.random() * data.firstNames[rtn.res.gender].length)] + " " + data.lastNames[Math.floor(Math.random() * data.lastNames.length)];
				rtn.res.gender === "female" ? rtn.res.gender = 0 : rtn.res.gender = 1;

				rtn.ok = true;

			} catch (e) {

				rtn.err = e;
			
			}

		}

		return rtn

	},

	nameColony: function(data,isNPC){

		var rtn = this._init(),
			that = this;

		if (rtn.err === false) {

			try {
			
				var prefix = data.name.prefix[Math.floor(Math.random() * data.name.prefix.length)],
					city = data.name.city[Math.floor(Math.random() * data.name.city.length)];

				rtn.res = {
				
					name: prefix + " " + city,
					stub: (prefix + "-" + city).toLowerCase(),
					loc_angle: that.dice.roll({min:0,max:3600,div:10}),
					loc_plane: that.dice.roll({min:0,max:3600,div:10}),
				};

				if (typeof isNPC !== 'undefined') {

					rtn.res.pop = that.dice.roll(data.npc.pop),
					rtn.res.mines = that.dice.roll(data.npc.mines),
					rtn.res.defence = that.dice.roll(data.npc.defence),
					rtn.res.credits = that.dice.roll(data.npc.credits);
				
				}

				rtn.ok = true;

			} catch (e) {

				rtn.err = e;
			
			}

		}

		return rtn;
				
	},

	npc: function(nodes,dataColony,dataNames,callback) {

		var rtn = this._init(),
			that = this,
			validIdList = [];

		nodes.forEach(function(node) {

			if(that._validNodeTypes.indexOf(node.aspects.type) > -1) {

				validIdList.push(node.aspects.node_id);

			}

			if (typeof node.subnodes !== 'undefined') {
				
				if (node.subnodes.length > 0) {

					node.subnodes.forEach(function(subNode){

						if(that._validNodeTypes.indexOf(subNode.aspects.type) > -1) {

							validIdList.push(subNode.aspects.node_id);

						}

					});

				}
			
			}

		});

		try {

			var colonyData = [];
			if (validIdList - 3 <= 0) {

				callback(null,colonyData);

			} else {

				var numColonies = this.dice.roll(dataColony.npc.num);
				if (numColonies > validIdList.length - 3) {

					numColonies = validIdList.length -3;
				
				}

				for(i=0;i<numColonies;i++){

					var nameColony = that.nameColony(dataColony,true);
					if (nameColony.ok === true) {

						var nameLeader = that.nameLeader(dataNames);
						if(nameLeader.ok === true) {

							colonyData.push({
								npc:{
									gender:nameLeader.res.gender,
									name:nameLeader.res.name,
									type:nameLeader.res.type,
									credits:nameColony.res.credits,
								},
								root:{
									node_id:validIdList[Math.floor(Math.random() * validIdList.length)],
									colony_name:nameColony.res.name,
									colony_stub:nameColony.res.stub,
									loc_angle:nameColony.res.loc_angle,
									loc_plane:nameColony.res.loc_plane,
								},
								aspects:{
									pop:nameColony.res.pop,
									jobs:nameColony.res.pop,
									defence:nameColony.res.defence
								},
								buildings:{
									mines:nameColony.res.mines
								}
							});

						}

					}

					var rmIndex = validIdList.indexOf(colonyData.name);
					if (rmIndex > -1) {

						validIdList.splice(rmIndex,1);

					}

				}

				callback(null,colonyData);
				
			}

		} catch(thErr) {

			callback(thErr);
		
		}

	}

};