module.exports = {

	_d100: function() {
		return Math.floor(Math.random() * 100) + 1;
	},

	d100: function(num) {
		var rtn = 0;
		if (this._d100() <= num) {
			rtn = 1;
		}
		return rtn;
	},

	d100Obj: function(arrayOfObj) {

		var diceResult = this._d100(),
			chanceCounter = 0,
			rtn = false;

		arrayOfObj.forEach(function(v){
			chanceCounter += parseInt(v.chance);
			if ((chanceCounter >= diceResult) && (rtn === false)) {
				rtn = v;
			}
		});
		
		return rtn;

	},

	roll: function(obj) {

		var div = 1,
			res,
			rtn = false;

		if (!("min" in obj)) {
			return 'object needs a minimum';
		}	
		if (!("max" in obj)) {
			return 'object needs a minimum';
		}

		if ("div" in obj) {
			div = parseInt(obj.div);
		}

		res = Math.floor(Math.random() * obj.max) + obj.min;
		res > 0 ? rtn = res/div : rtn = 0;
		
		return rtn;

	},

	jitter: function(val) {

		var jit = 20, // change to change jitter percentage. 20% seems reasonable.
			pct = (val/100);

		return this.roll({
			min:(pct*(100-jit)),
			max:(pct*(100+jit))
		});




	}

}