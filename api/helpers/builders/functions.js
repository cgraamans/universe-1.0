module.exports = {

	rtnVar: function() {

		return {
		
			ok:false,
			res:[],
			err:false
		
		};
	},

	getJSON: function(jsonLocation,libName) {

		var rtn = false,
			fs = require('fs');

		try {

			rtn = JSON.parse(fs.readFileSync(jsonLocation+'/data/'+libName+'.json', 'utf8'));

		} catch(e){

			console.error(e);
		
		}
		return rtn;

	}

};