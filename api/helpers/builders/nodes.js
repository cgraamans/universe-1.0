module.exports = {

	_validSubNodes: [
		'moon',
		'asteroids',
		'debris'
	],

	_sortByAU: function(arr) {

		return arr.sort(function(a, b) {
		    return a.aspects.au - b.aspects.au;
		});		

	},

	_makeNode: function(objType,isSubNode) {

		var factor = [1],
		dice = require('./dice'),
		that = this;

		// Passback Variable. This is pushed into the result.
		// NOTE:
		// 1 au = 389 lunar distances
		var passBack = {
			
			aspects: {

				au: 0,
				rotation:dice.roll(that.data.aspects.rotation.v),
				rotation_offset:0,
				factor:0,
				
				type:objType,
				type_surface:null,
				type_surface_quality:null,
				type_mass:0,
				type_atmosphere:null,
				type_atmosphere_density:0,
				type_size:null,
				type_size_r0:1,
				
				orb_incl:0,
				orb_angle:dice.roll(that.data.orbitals.angle),
				orb_ellipse:1,
				orb_ellipse_dir:0,

				opt_asteroids:0,
				opt_hasring:dice.d100(that.data.aspects.hasring),
				opt_ring_offset:0,
				opt_ring_count:0,
				opt_haswater:0,
				opt_gl:0

			},
			_factor:null,
			_factor_distance:1			


		};

		//
		// Conditionals
		//

		if (typeof isSubNode !== 'undefined') {

			// Subnode Specific

			passBack.aspects.au = dice.roll(that.data.distances.subnodes);

		} else {

			// Node Specific

			// Subnodes
			if (that.data.subnodes.hasOwnProperty(objType)) {

				passBack._subnodes = {
					moon:0,
					asteroids:0,
					debris:0
				},
				subnodeCounter = dice.roll(that.data.subnodes[objType]);

				if (subnodeCounter > 0) {
					for(f=0;f<subnodeCounter;f++) {

						var t = dice.d100Obj(that.data.subnodes.types);

						passBack._subnodes[t.type]++;

					}

				}
			}

			// Distances
			passBack.aspects.au = dice.roll(that.data.distances[objType]);

			// Has a debris ring?
			if (passBack.aspects.opt_hasring == 1) {

				passBack.aspects.opt_ring_count = dice.roll(that.data.aspects.ring.count),
				passBack.aspects.opt_ring_offset = dice.roll(that.data.aspects.ring.offset);

			}

		} // End Specificity

		// Rotation
		if (dice.d100(that.data.aspects.rotation.chanceOfOffset) == 1) {

			passBack.aspects.rotation_offset = dice.roll(that.data.aspects.rotation.offset);

		}

		// Orbital ellipse
		if (dice.d100(that.data.orbitals.chanceOfEllipse) == 1) {

			passBack.aspects.orb_ellipse = dice.roll(that.data.orbitals.ellipse.e);
			passBack.aspects.orb_ellipse_dir = dice.roll(that.data.orbitals.ellipse.dir);

		}

		// Orbit Inclination
		if (dice.d100(that.data.orbitals.chanceOfIncl) == 1) {

			passBack.aspects.orb_incl = dice.roll(that.data.orbitals.incl);

		}

		// Asteroid/Debris Object Counter
		if (that.data.aspects.objectCounter.hasOwnProperty(objType)) {

			passBack.aspects.opt_asteroids = dice.roll(that.data.aspects.objectCounter[objType]);
		
		}

		// HasWater?
		if (objType == 'asteroids' || objType == 'rock' || objType == 'moon') {

			passBack.aspects.opt_haswater = dice.d100(that.data.aspects.pctHasWater);
			// factor

		}

		// Atmosphere type
		if (that.data.atmospheres.conditionals.hasOwnProperty(objType)) {

			var asp = dice.d100Obj(that.data.atmospheres.conditionals[objType]);
			passBack.aspects.type_atmosphere = asp.type,
				factor.push(dice.roll(asp.conditional));

		}

		// Atmosphere density
		if (that.data.atmospheres.density.hasOwnProperty(objType)) {

			passBack.aspects.type_atmosphere_density = dice.roll(that.data.atmospheres.density[objType]);

		}		

		// Surface type
		if (that.data.surfaces.conditionals.hasOwnProperty(objType)) {
			var asp = dice.d100Obj(that.data.surfaces.conditionals[objType]);
			passBack.aspects.type_surface = asp.type,
				factor.push(dice.roll(asp.conditional));
		}

		// Surface quality
		if (that.data.surfaces.quality.hasOwnProperty(objType)) {

			var asp = dice.d100Obj(that.data.surfaces.quality[objType]);
			passBack.aspects.type_surface_quality = asp.type,
				factor.push(dice.roll(asp.conditional));

		}

		// Object Mass
		if (that.data.masses.hasOwnProperty(objType)) {

			passBack.aspects.type_mass = dice.roll(that.data.masses[objType]);
			// factor (gravity) (first bell curve)
		
		}

		// Size type
		if (that.data.sizes.hasOwnProperty(objType)) {

			// factor as per r0.
			var asp = dice.d100Obj(that.data.sizes[objType]);

			passBack.aspects.type_size = asp.type,
			passBack.aspects.type_size_r0 = dice.roll(asp.conditional);
		
		}

		passBack._factor = factor;

		return passBack;

	},

	_factorize: function(factors) {

		// Factors
		var totalFactor = 0;
		factors.forEach(function(v){
			totalFactor+=v;
		});

		return Math.round((totalFactor/factors.length)*10000)/10000;

	},

	_seed: function(objType,factor) {

		var that = this,
		rtn = {};

		// User Property Seeds
		if (that.data.seeds.hasOwnProperty(objType)) {

			rtn.pop = Math.round(that.data.seeds[objType].pop * factor);
		
		}

		return rtn;

	},

	_init: function(jsonLocation) {

		try {

			this.func = require('./functions');
			this.dice = require('./dice');

			rtn = this.func.rtnVar(),
			this.data = this.func.getJSON(jsonLocation,'node');
			
			if (this.data === false) {

				rtn.err = "INIT ERROR in NODE BUILDER"

			}

		} catch(e) {

			rtn.err = "INIT ERROR in NODE BUILDER";

		}
		return rtn;

	},

	createFromStar: function(star,jsonLocation,callback) {

		var rtn = this._init(jsonLocation),
			that = this;

		try {

			for (var p in star.nodes) {

			    if (star.nodes.hasOwnProperty(p)) {

			    	if (star.nodes[p] > 0) {

			    		for(i=0;i<star.nodes[p];i++) {

		    				var node = that._makeNode(p);

							// Distance factoring
							if (node.aspects.au < star.gl_max && node.aspects.au > star.gl_min) {

								node.aspects.opt_gl = 1;

							} else {

				    			var distFactor = 1;

								if (node.aspects.au > star.gl_max) {
									
									if (that.data.distances.hasOwnProperty(p)) {

										distFactor = 1-(node.aspects.au - star.gl_max) * (1/that.data.distances[p].max*10);
									
									}

				    			}

								if (node.aspects.au < star.gl_min) {
									
									if (that.data.distances.hasOwnProperty(p)) {
										
										distFactor = (star.gl_min - node.aspects.au) * (1/that.data.distances[p].min*10);
									
									}

								}
								node._factor_distance = Math.round(distFactor*100)/100;
								node._factor.push(node._factor_distance);

							}

							// Factorize
							node.aspects.factor = that._factorize(node._factor);
							
							// Seed
							node.seed = that._seed(p,node.aspects.factor);

			    			rtn.res.push(node);

			    		}

			    	}

			    }
			}
			rtn.res = that._sortByAU(rtn.res);
			
			rtn.ok = true;

		} catch(e) {
			
			if (e) rtn.err = e;
			
		}

		callback(rtn);

	},

	createFromNodes: function(nodes,jsonLocation,callback) {

		var rtn = this._init(jsonLocation),
			that = this;

		try {

			nodes.forEach(function(node){

				 if (typeof node._subnodes !== 'undefined') {

					node.subnodes = [];
					var snCT = 1;

				 	that._validSubNodes.forEach(function(v) {


				 		if (node._subnodes.hasOwnProperty(v)) {

				 			if (node._subnodes[v] > 0) {

				 				for(a=0;a<node._subnodes[v];a++) {

				    				var subnode = that._makeNode(v,true);

				    				var name = node.root.name + "/" + snCT + " [" + subnode.aspects.type[0].toUpperCase() + subnode.aspects.type.substr(1) + "]",
				    					stub = node.root.stub + "-" + snCT + "-" + subnode.aspects.type;

			    					snCT++;
				    				
				    				if (node.aspects.opt_gl == 1) {

				    					subnode.aspects.opt_gl = 1;
				    				
				    				} else {

				    					subnode._factor.push(node._factor_distance);

				    				}

				    				subnode.root = {

				    					star_id: node.root.star_id,
				    					name: name,
				    					moniker: name,
				    					stub: stub,
				    					parent_id: node.aspects.node_id

				    				};

									// Factorize
									subnode.aspects.factor = that._factorize(subnode._factor);
									// Seed
									subnode.seed = that._seed(v,subnode.aspects.factor);
		
				 					node.subnodes.push(subnode);

				 				}
				 				
				 			}

				 		}

				 	});

				 }

			});

			rtn.res = nodes;

		} catch(e) {
			
			if (e) rtn.err = e;
			
		}

		rtn.ok = true;
		callback(rtn);

	}

};