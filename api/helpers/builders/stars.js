module.exports = {

	create: function(sockey,$state,callback,isNeighbour) {

		var rtn = {

			ok:false,
			res:[],
			err:[]

		};


		var fs = require('fs');
		var dice = require('./dice');

		try {

			var data = JSON.parse(fs.readFileSync(sockey.DIR+'/data/star.json', 'utf8'));
		
		} catch(e){

			if (e) throw e;
		
		}

		var neighbours = (dice.roll(data.neighbours.count)+1),
			a = 0;

		if (typeof isNeighbour != 'undefined') {
			neighbours--;
		}

		
		sockey.modules.async.whilst(

		    function() { return a < neighbours; },
		    function(cb) {

		    	try{

					sockey.db.get($state,"SELECT id, avg_R0, gl_min, gl_max, node_max_gas, node_max_rock, avg_lum, avg_mass, avg_spec_trans FROM data_stars ORDER BY RAND() LIMIT 1",[],function(r){

						a++;
						if (r.ok === true) {


							var passBack = {

								aspect: {
									star_id:r.res[0].id,
									r0: dice.jitter(r.res[0].avg_R0),
									mass: dice.jitter(r.res[0].avg_mass),
									lum: dice.jitter(r.res[0].avg_lum),
									spec_trans: dice.jitter(r.res[0].avg_spec_trans),
								},
								gl_min:r.res[0].gl_min,
								gl_max:r.res[0].gl_max,
								nodes:{
									asteroids:dice.roll(data.nodes.asteroids),
									debris:dice.roll(data.nodes.debris)
								},
								distance:dice.roll(data.neighbours.distance),
								inCluster:dice.d100(data.neighbours.is.inCluster),
								isLinked:dice.d100(data.neighbours.is.knownSystem)
							};

							var nodes_gas = dice.roll(data.nodes.gas),
								nodes_rock = dice.roll(data.nodes.rock);

							if (nodes_gas <= r.res[0].node_max_gas) {passBack.nodes.gas = nodes_gas} else {passBack.nodes.gas =  r.res[0].node_max_gas}
							if (nodes_rock <= r.res[0].node_max_rock) {passBack.nodes.rock = nodes_rock} else {passBack.nodes.rock = r.res[0].node_max_rock}

							rtn.res.push(passBack);

							cb();

						}

				    });

		    	} catch(e) {

					cb(e);
		    	
		    	}

	    	},
	    	function(err){

	    		if (err){

	    			rtn.err.push(err);

	    		} else {

	    			rtn.ok = true;

	    		}
				callback(rtn);
	    	
	    	}
    	
    	);

	}







};