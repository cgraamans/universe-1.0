exports = module.exports = function(sockey,$state,sock,callback) {

	var local = {

		login: function(sockey,$state,user,callback) {

			var rtn = {
				ok:false,
				err:false,
				res:false,
			};

			sockey.db.get($state,"SELECT id, password FROM users WHERE name = ?",user.name,function(a){

				if (a.ok === false) {

					rtn.err = 'Password query failed.';
					callback(rtn);

				} else {

					if (a.res.length == 1) {

							var bcrypt = require('bcryptjs');
							if (bcrypt.compareSync(user.password,a.res[0].password) === true) {

								user.id = a.res[0].id;
								sockey.token.update(sockey,$state,user,function(arr){

									if (arr.ok === true) {

										rtn.ok = true;
										rtn.res = arr.res;

									} else {

										rtn.err = 'User not found.';

									}
									callback(rtn);

								});


							} else {

								rtn.err = 'User not found.';
								callback(rtn);

							}
							

					} else {

						rtn.err = 'User not found.';
						callback(rtn);
					
					}		

				}

			});

		}

	};

	return function(data) {

		// Note: Data is the request object passed by the socket.
		var timing = {timers:[],intervals:[]};

		// CONTROLLER CODE
		
			var emit = {
				ok:false,
				err:false,
			},
			that = this;

			if (typeof data !== 'object') {

				emit.err = 'Bad data passed to auth.';

			}

			if ((typeof data.user !== 'object') || (typeof (data.user.name) !== 'string') || (typeof (data.user.password) !== 'string')) {
				
				emit.err = 'User data invalid.';

			} else {

				if (data.user.name.length < 1) {
					
					emit.err = 'You need to fill in a name.';
				
				}

				if (data.user.password.length < 1) {

					emit.err = 'You need to fill in a password.';

				}

			}

			if (emit.err === false) {

				local.login(sockey,$state,data.user,function(li){

					emit.res = li;
					emit.ok = true;
					$state.socket.emit(sock,emit);
					callback(timing);

				});

			} else {

				$state.socket.emit(sock,emit);
				callback(timing);

			}

		// CONTROLLER CODE ENDS HERE

	}

};