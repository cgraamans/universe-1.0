exports = module.exports = function(sockey,$state,sock,callback) {

	var local = {

		registration: function(sockey,$state,user,callback) {

			var rtn = {
				ok:false,
				err:false,
				res:false,
			},
			that = this;

			sockey.db.get($state,"SELECT id FROM users WHERE name = ?",user.name,function(a){

				if (a.ok === true) {
					
					if (a.res.length > 0) {

						rtn.err = 'User name already in use.';
						callback(rtn);

					} else {

						var ins = {
							name: user.name,
							insdate:Math.floor(Date.now() / 1000)
						},
						bcrypt = require('bcryptjs');

						ins.password = bcrypt.hashSync(user.password,bcrypt.genSaltSync(10));

						if (typeof user.email !== 'undefined') {

							ins.email = user.email;

						}

						$state.db.query("INSERT INTO users SET ?",ins,function(c,b) {

							if (c == null) {

								that.userId = b.insertId;

								var token = sockey.token.generate();

								$state.db.query('INSERT INTO user_keys SET ?', {

									user_id:b.insertId,
									token: token,
									insdate:(Math.floor(Date.now() / 1000)), 

								},function(err, result) {

									if (err) {
										
										rtn.err = 'Insert Tokey Error.';
									
									} else {

										rtn.ok = true;
										rtn.res = {name:user.name,token:token};

									}
									callback(rtn);
								});

							} else {

								rtn.err = 'Error inserting user.';
								callback(rtn);

							}

						});

					}
					
				} else {

					rtn.err = 'Username query failed.';
					callback(rtn);

				}
				
			});

		},

		creation: function(sockey,$state,callback) {

			var rtn = {
				ok:false,
				err:false
			};

			if (this.userId) {

				var registerModel = require(sockey.DIR+'/models/ssr'),
					that = this;

				registerModel.create(sockey,$state,that.userId,function(sysRegRes){

					console.log('sysRegRes');

					if (sysRegRes.ok === true) {

						$state.db.query("SELECT n.stub FROM nodes as n INNER JOIN users u ON u.home = n.id WHERE u.id = ?",[that.userId],function(c,b) {

							if (c) {

								rtn.err = "No home found for User";
								callback(rtn);
							
							} else {
								
								rtn.res = b[0].stub;
								rtn.ok = true;
								callback(rtn);

							}

						});


					} else {

						rtn.err = "Solar System Creation Problem"
						callback(rtn);

					}					

				
				});

			} else {

				rtn.err = "No UserID";
				callback(rtn);

			}

		}

	};

	return function(data) {

		// Note: Data is the request object passed by the socket.
		var timing = {timers:[],intervals:[]};

		// CONTROLLER CODE
		
			var emit = {
				ok:false,
				err:false,
			},
			that = this;

			if (typeof data !== 'object') {

				emit.err = 'Bad data passed.';

			}

			if ((typeof data.user !== 'object') || (typeof (data.user.name) !== 'string') || (typeof (data.user.password) !== 'string')) {
				
				emit.err = 'User data invalid.';

			} else {

				if (data.user.name.length < 1) {
					
					emit.err = 'You need to fill in a name.';
				
				}

				if (data.user.password.length < 1) {

					emit.err = 'You need to fill in a password.';

				}

			}

			if (emit.err === false) {

				var validator = require('validator');
				if (validator.isLength(data.user.name,sockey.opt.auth.lengths.username) === true) {

					if (validator.matches(data.user.name,'^[a-zA-Z0-9!@#$%^&*]*$','g') === true) {

						if (validator.isLength(data.user.password,sockey.opt.auth.lengths.password) === true) {

							if (validator.matches(data.user.password,'^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]*$','g') === true) {

								if (sockey.opt.auth.register_email === true) {

									if (typeof data.user.email !== 'undefined') {

										if (validator.isEmail(data.user.email) === true) {

											local.registration(sockey,$state,data.user,function(reg) {

												if (reg.ok === true) {

													local.creation(sockey,$state,function(ss){

														if (ss.ok === true) {

															reg.res.home = ss.res;
															$state.socket.emit(sock,reg);
															
														} else {

															$state.socket.emit(sock,reg);

														}
														callback(timing);

													});
												
												} else {

													$state.socket.emit(sock,reg);
													callback(timing);

												}

											});

										} else {

											emit.err = 'Email address is invalid.';
											$state.socket.emit(sock,emit);
											callback(timing);

										}

									} else {

										emit.err = 'Email address needed.';
										$state.socket.emit(sock,emit);
										callback(timing);

									}
									
								} else {

									data.user.email = null;
									local.registration(sockey,$state,data.user,function(reg) {

										console.log('local.registration');

										if (reg.ok === true) {

											local.creation(sockey,$state,function(ss){
												
												console.log('ss');

												if (ss.ok === true) {

													reg.res.home = ss.res;
													$state.socket.emit(sock,reg);
													callback(timing);

												} else {

													$state.socket.emit(sock,reg);
													callback(timing);

												}

											});
										
										} else {
											
											console.log('one emit err low');
											console.log(reg);

											$state.socket.emit(sock,reg);
											callback(timing);

										}
	
									});
								}

							} else {

								emit.err = 'Password must contain letters and at least one special character and one number.';
								$state.socket.emit(sock,emit);
								callback(timing);

							}

						} else {

							emit.err = 'Password must be between 6 and 60 characters long.';
							$state.socket.emit(sock,emit);
							callback(timing);

						}							

					} else {

						emit.err = 'Usernames may contain letters, special characters and numbers.';
						$state.socket.emit(sock,emit);
						callback(timing);

					}

				} else {

					emit.err = 'User name must be between 3 and 32 Characters..';
					$state.socket.emit(sock,emit);
					callback(timing);

				}


			} else {

				$state.socket.emit(sock,emit);
				callback(timing);

			}

		// CONTROLLER CODE ENDS HERE

	}

};