module.exports = {

	route: [

		{
			sock:'cluster',
			controller:'./controllers/cluster'
		},
		{
			sock:'stars',
			controller:'./controllers/stars'
		},
		{
			sock:'nodes',
			controller:'./controllers/nodes'
		},

		// Example Routes
		{
			sock:'example',
			controller:'./controllers/example'
		},
		{
			sock:'keytest',
			controller:'./controllers/example-keytest',
		},

	]
}