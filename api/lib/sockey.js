module.exports = {
	
	// Database
	db: {

		connect: function(options) {

			var mysql = require('mysql');
			return mysql.createConnection(options);			

		},

		get: function($state,sql_str,val,callback){

			var rtn = {
				ok:false,
				res:false,
				err:false,
			};

			var simple = {sql:sql_str};
			simple.timeout = 40000;
			simple.values = val;

			if (typeof $state.db !== 'undefined') {

				if ($state.db !== false) {

					$state.db.query(simple, function(error,rslt) {

						if (error) {
							rtn.err = error;
						} else {
							rtn.res = rslt;
							rtn.ok = true;
						}
						callback(rtn);

					});

				} else {

					rtn.err = 'Bad connection';
					callback(rtn);

				}

			} else {

				rtn.err = 'No connection';
				callback(rtn);

			}

		},

	},

};

