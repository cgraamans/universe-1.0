exports = module.exports = {

	_sql: {

		getSingleStar:"SELECT cs.id as star_id, cs.name as star_name, cs.stub as star_stub, dc.stub as cluster_stub, csl.distance, csl.plane, csl.incl FROM cluster_star_links AS csl INNER JOIN cluster_stars cs ON cs.id = csl.star_from_id INNER JOIN clusters c ON c.id = cs.cluster_id INNER JOIN data_clusters dc ON dc.id = c.cluster_id WHERE cs.stub = ?",
		getClusterData:"SELECT dc.name, dc.stub, c.image_starscape, c.year, c.created_on, u.name as created_by FROM data_clusters as dc INNER JOIN clusters c ON c.cluster_id = dc.id INNER JOIN users u ON c.created_by = u.id WHERE dc.stub = ?",
		getStarLinksByStarId:"SELECT csl.star_to_id as star_id, cs.name as star_name, cs.stub as star_stub, dc.stub as cluster_stub, csl.distance, csl.plane, csl.incl FROM cluster_star_links AS csl INNER JOIN cluster_stars cs ON cs.id = csl.star_to_id INNER JOIN clusters c ON c.id = cs.cluster_id INNER JOIN data_clusters dc ON dc.id = c.cluster_id WHERE csl.star_from_id = ?",
		getRandomStarByCluster:"SELECT cs.stub FROM cluster_stars As cs INNER JOIN clusters c ON c.id = cs.cluster_id INNER JOIN data_clusters dc ON dc.id = c.cluster_id WHERE dc.stub = ? ORDER BY RAND() LIMIT 1"
	},

	arrayContains: function(a, obj) {
	
		for (var i = 0; i < a.length; i++) {
    
    		if (a[i] == obj) {
    
        		return true;
    
    		}
	
		}
		return false;
	
	},


	getClusterData: function(sockey,$state,cluster,callback) {

		var  rtn = {ok:false,res:{},err:[]},
			that = this;

		// get all cluster data and star data
		sockey.db.get($state,that._sql.getClusterData,cluster,function(r){

			if (r.ok !== true) {

				rtn.err.push(r);

			} else {

				rtn.res.stars = r.res,
				rtn.ok = true;
				if (r.res.length > 0) {
					rtn.res.starmap = r.res[0];
				}

			}
			callback(rtn);

		});

	},

	getStarLinkData: function(sockey,$state,star_id,callback) {

		sockey.db.get($state,this._sql.getStarLinksByStarId,[star_id],function(s){

			if (s.ok !== true) {

				console.log(s.err);
				callback('- Walk: SQL ERROR GETTING FIRST STAR LINKS');

			} else {

				callback(null,s.res);

			}

		});

	},

	starWalk: function(sockey,$state,star,cluster,callback,maxDepth,depth,obj) {

		var rtn = {ok:false},
			that = this;

		if (typeof maxDepth === 'undefined') {

			maxDepth = 5;

		}
		if (typeof depth === 'undefined') {

			depth = 0;

		}

		if (typeof obj === 'undefined') {

			that._walkedStars = [];
			sockey.db.get($state,that._sql.getSingleStar,star,function(r) {

				if (r.ok !== true) {

					rtn.err = '- Walk: SQL ERROR';
					callback(rtn);

				} else {

					if (r.res.length > 0) {

						var newObj = r.res[0];
						that._walkedStars.push(newObj.star_id);
						that.getStarLinkData(sockey,$state,newObj.star_id,function(err,res){

							if (err) {

								rtn.err = err;
								callback(rtn);
							
							} else {

								if (res.length > 0) {

									newObj.childNodes = res;
									that.starWalk(sockey,$state,star,cluster,function(sWalk){

										if (sWalk.ok === true) {

											rtn.res = sWalk.res,
											rtn.ok = true;

										} else {

											rtn.err = sWalk.err;

										}
										callback(rtn);

									},maxDepth,depth,newObj);

								} else {

									rtn.ok = true,
									rtn.res = newObj;

									callback(rtn);

								}

							}

						});

					} else {

						rtn.err = '- Walk: Star Starting Point Not Found.';
						callback(rtn);

					}
					
				}

			});

		} else {

			var unsetList = [];
			depth++;
			sockey.modules.async.forEachOf(obj.childNodes,function(val,key,cb) {

				if (that.arrayContains(that._walkedStars,val.star_id) === false) {
					
					that._walkedStars.push(val.star_id),
					obj.childNodes[key].foreignCluster = false;

					if (val.cluster_stub != cluster) {

						obj.childNodes[key].foreignCluster = true;

					}

					that.getStarLinkData(sockey,$state,val.star_id,function(err,res){

						if (err) {

							cb(err);
						
						} else {

							obj.childNodes[key].childNodes = res;
							that.starWalk(sockey,$state,star,cluster,function(subRes){

								if (subRes.ok === true) {

									obj.childNodes[key] = subRes.res;
									cb();

								}

							},maxDepth,depth,obj.childNodes[key]);
							
						}

					});

				} else {

					unsetList.push(key);
					cb();
				
				}

			},function(err){

				if (err) {

					rtn.err = err;

				} else {
					unsetList.forEach(function(us){

						obj.childNodes.splice(us,1);

					});
					rtn.ok = true;
					rtn.res = obj;

				}

				callback(rtn);

			});

		}

	},

	getFocus: function(sockey,$state,data,callback) {

		var that = this;

		if (typeof data.star === 'undefined') {

			sockey.db.get($state,that._sql.getRandomStarByCluster,data.cluster,function(r){

				if (r.ok === true) {
  
					if (r.res.length > 0) {
					
						callback(r.res[0].stub);
					
					} else {

						callback(false);
					
					}

				} else {

					callback(false);

				}

			}); 

		} else {
			
			console.log(data.star);

			sockey.db.get($state,"SELECT cs.id FROM cluster_stars AS cs WHERE cs.stub = ?",data.star,function(r){

				console.log(r);

				if (r.ok === true) {

					if (r.res.length == 1) {

						callback(data.star);

					} else {

						callback(false);

					}

				} else {

					callback(false);

				}

			});

		}

	}

};

