module.exports = {
	_sql: {

		getNodesByStarStub:"SELECT n.name, n.moniker, n.stub, na.au, na.rotation, na.rotation_offset, na.factor, na.orb_incl, na.orb_angle, na.orb_ellipse, na.orb_ellipse_dir, na.opt_asteroids, na.opt_hasring, na.opt_ring_offset, na.opt_ring_count, na.opt_haswater, na.opt_gl, na.type, na.type_surface, na.type_surface_quality, na.type_mass, na.type_atmosphere, na.type_atmosphere_density, na.type_size, na.type_size_r0 FROM  `cluster_stars` AS cs INNER JOIN nodes n ON n.star_id = cs.id INNER JOIN node_aspects na ON na.node_id = n.id WHERE cs.stub = ? AND n.parent_id IS NULL ORDER BY na.au ASC",
		getSubNodesByNodeStub:"SELECT nn.*, na.*, n.stub as parent_id FROM nodes AS nn INNER JOIN nodes n ON n.id = nn.parent_id INNER JOIN node_aspects na ON na.node_id = nn.id WHERE n.stub = ? ORDER BY na.au ASC",
		getColoniesByNodeStub:"SELECT nu.colony_name, nu.colony_stub, nu.year, nu.loc_angle, nu.loc_plane, nun.name as npc_name, nun.gender as npc_gender, u.name as user_name FROM `node_users` AS nu INNER JOIN nodes n ON n.id = nu.node_id LEFT JOIN node_user_npcs nun ON nun.nu_id = nu.id LEFT JOIN users u ON u.id = nu.user_id WHERE n.stub = ?",

	},
	getNodes: function($state,sockey,starStub,callback) {

		rtn = {ok:false},
		that = this;
		sockey.db.get($state,that._sql.getNodesByStarStub,starStub,function(r){

			if (r.ok !== true) {

				rtn.err = r.err;
				callback(rtn);

			} else {

				if (r.res.length < 1) {
				
					rtn.err = "Nodes For '"+starStub+"' Not Found";
					callback(rtn);

				} else {

					sockey.modules.async.forEachOf(r.res,function(val,key,fEOCallback) {

						r.res[key].subnodes = [];
						r.res[key].colonies = [];

						sockey.modules.async.parallel(

							[

								function(cb) {

									sockey.db.get($state,that._sql.getSubNodesByNodeStub,r.res[key].stub,function(sNodes){

										if (sNodes.ok === true) {

											if(sNodes.res.length > 0) {

												r.res[key].subnodes = sNodes.res;	

											}
											cb();


										} else {
										
											cb(err);	
										
										}			
										
									});

								},

								function(cb) {


									sockey.db.get($state,that._sql.getColoniesByNodeStub,r.res[key].stub,function(sColonies){

										if (sColonies.ok === true) {

											if(sColonies.res.length > 0) {

												r.res[key].colonies = sColonies.res;	

											}
											cb();


										} else {
										
											cb(err);	
										
										}			
										
									});

								}

							],
							function(errAlpha){

								if (errAlpha) {

									fEOCallback(errAlpha);

								} else {

									fEOCallback();

								}

							}

						);

					},
					function(err){

						if (err) {
							
							rtn.err = err;
						
						} else {

							rtn.res = r.res,
							rtn.ok = true;
						
						}

						callback(rtn);

					});

				}

			}

		});

	}
};

/*

SELECT n.name, n.name_orig, n.stub, n.stub_orig, na.au, na.rotation, na.rotation_offset, na.factor, na.orb_incl, na.orb_angle, na.orb_ellipse, na.orb_ellipse_dir, na.opt_asteroids, na.opt_hasring, na.opt_ring_offset, na.opt_ring_count, na.opt_haswater, na.opt_gl, na.type, na.type_surface, na.type_surface_quality, na.type_mass, na.type_atmosphere, na.type_atmosphere_density, na.type_size, na.type_size_r0 FROM  `cluster_stars` AS cs INNER JOIN nodes n ON n.star_id = cs.id INNER JOIN node_aspects na ON na.node_id = n.id WHERE cs.stub = ? AND n.parent_id IS NULL

*/