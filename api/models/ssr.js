
var solarsystemRegistration = {

	_sql: {

		getClusterById:"SELECT id FROM clusters WHERE cluster_id = ?",
		setCluster:"INSERT INTO clusters SET ?",
		getStarByName:"SELECT id FROM cluster_stars WHERE name = ?",
		setStar:"INSERT INTO cluster_stars SET ?",
		setStarLink:"INSERT INTO cluster_star_links SET ?",
		setNodeRoot:"INSERT INTO nodes SET ?",
		setNodeAspects:"INSERT INTO node_aspects SET ?",
		setNodeSeeds:"INSERT INTO node_seeds SET ?",
		setNodeUser:"INSERT INTO node_users SET ?",
		setNodeUserNPC:"INSERT INTO node_user_npcs SET ?",
		setNodeUserAspects:"INSERT INTO node_user_aspects SET ?",
		setNodeUserModifiers:"INSERT INTO node_user_modifiers SET ?",
		putUserColonyHome:"UPDATE users SET home = ? WHERE id = ?",
		getDataNPCType:"SELECT productivity,welfare,freedom,stability FROM `data_npc_types` WHERE id = ?",

	},

	_func: {

		stubby: function(name){

			return name.replace(' ','-').toLowerCase();
		
		},

		unixTimeStamp: function() {

			 return Math.round(new Date().getTime()/1000);
		
		},

		romanize: function(num) {

			var romanMatrix = [
			  [1000, 'M'],
			  [900, 'CM'],
			  [500, 'D'],
			  [400, 'CD'],
			  [100, 'C'],
			  [90, 'XC'],
			  [50, 'L'],
			  [40, 'XL'],
			  [10, 'X'],
			  [9, 'IX'],
			  [5, 'V'],
			  [4, 'IV'],
			  [1, 'I']
			],
			that = this;

			if (num === 0) {
				
				return '';
			
			}

			for (var i = 0; i < romanMatrix.length; i++) {
				
				if (num >= romanMatrix[i][0]) {
					
					return romanMatrix[i][1] + that.romanize(num - romanMatrix[i][0]);

				}
		
			}
		
		},

		reverseDegrees: function(num) {

			if (num >= 180) {
				return num-180;
			} else {
				return num+180;
			}

		}

	},

	_initNewSystem: function(sockey,$state,callback) {

		var rtn = {
			res: {
				stars:{},
				nodes:{}
			},
		},helperStar = require(sockey.DIR+'/helpers/builders/stars');

		helperStar.create(sockey,$state,function(r){

			if (r.ok === true) {

				rtn.res.stars = r.res;

				var helperNode = require(sockey.DIR+'/helpers/builders/nodes');
				helperNode.createFromStar(r.res[0],sockey.DIR,function(s) {

					if (s.ok === true) {

						rtn.ok = true,
						rtn.res.nodes = s.res;

					} else {

						rtn.err = s;

					}
					callback(rtn);

				});

			} else {

				rtn.err = r;
				callback(rtn);

			}

		});

	},

	_initCluster: function(sockey,$state,sysObj,userId,callback) {

		rtn = {ok:false},
		that = this;
		sockey.db.get($state,that._sql.getClusterById,sysObj.cluster.id,function(r){

			if (r.ok !== true) {

				rtn.err = r;
				callback(rtn);

			} else {

				if (r.res.length > 0) {

					for(i=0,c=sysObj.stars.length;i<c;i++){

						sysObj.stars[i].aspect.cluster_id = r.res[0].id;

					}
					rtn.ok = true,
					rtn.res = sysObj;
					callback(rtn);

				} else {

					var fs = require('fs');
					fs.readdir(sockey.DIR + sockey.opt.custom.skyboxes, function(errt, items) {

						if (errt) {

							rtn.err = errt;
							callback(rtn);

						} else {

							var ins = {
								cluster_id:sysObj.cluster.id,
								image_starscape:items[Math.floor(Math.random() * items.length)],
								created_on:that._func.unixTimeStamp(),
								created_by:userId
							}

							$state.db.query(that._sql.setCluster,ins, function(erru, rslt) {

								if (erru) {

									rtn.err = erru;
									callback(rtn);
								
								} else {

									for(i=0,c=sysObj.stars.length;i<c;i++) {

										sysObj.stars[i].aspect.cluster_id = rslt.insertId;

									}

									rtn.ok = true,
									rtn.res = sysObj;
									callback(rtn);

								}

							});

						}


					});

				}

			}

		});

	},

	_nameCluster: function(sockey,$state,callback) {

		rtn = {ok:false};
		var clustersModel = require(sockey.DIR+'/helpers/builders/clusters');
			clustersModel.create(sockey,$state,function(s){
				
			if (s.ok === true) {

				rtn.ok = true,
				rtn.res = s.res[0];

						
			} else {

				rtn.err = s;

			}
			callback(rtn);


		});

	},

	_nameStar: function(sockey,$state,clusterName,callback,itt) {

		var rtn = {ok:false},
			that = this,
			multiplyer = 1000;

		if (typeof itt === 'undefined') {
			
			itt = 0;
		
		}
		var ittC = itt%10;
		if (ittC > 0) {

			multiplyer * (10*Math.round(ittC));

		}

		itt++;
		var starName = clusterName + ' ' + (Math.round(Math.random() * multiplyer) + 1);
		sockey.db.get($state,that._sql.getStarByName,[starName],function(r){

			if (r.ok === true) {

				if (r.res.length > 0) {

					that._nameStar(sockey,$state,clusterName,function(s) {

						if (s.ok === true) {
							
							rtn.ok = true;
							rtn.res = starName;							

						} else {

							rtn.err = s;
						}
						callback(rtn);

					},itt);

				} else {

					rtn.ok = true,
					rtn.res = starName;
					callback(rtn);
				}

			} else {

				rtn.err = r;	
				callback(rtn);

			}

		});

	},

	_colonize: function(sockey,$state,processedNodes,userId,callback) {

		var rtn = {
				ok:false,
				res:{
					nu_id:false,
					npcColonies:[]
				}
			},
			helper = require(sockey.DIR+'/helpers/builders/colonies'),
			func = require(sockey.DIR+'/helpers/builders/functions');

		var colonyData = func.getJSON(sockey.DIR,'colony'),
			nameData = func.getJSON(sockey.DIR,'names');
		
		if ((colonyData !== false) && (nameData !== false)) {

			sockey.modules.async.parallel(

				[
					// User
					function(cb) {

						var homeId = false,
							isInGlList = [],
							isARockList = [],
							userColonyObj = helper.nameColony(colonyData);

						if (userColonyObj.ok !== true) {

							cb(userColonyObj.err);

						} else {

							processedNodes.nodes.forEach(function(node){

								if (node.aspects.opt_gl == 1) {

									isInGlList.push(node.aspects.node_id);

								}

							});
							if (isInGlList.length > 0) {

								homeId = isInGlList[Math.floor(Math.random() * isInGlList.length)];
							
							} else {

								processedNodes.nodes.forEach(function(node){

									if(node.aspects.type == 'rock') {

										isARockList.push(node.aspects.node_id);
									
									}
							
								});

								if (isARockList.length > 0) {

									homeId = isARockList[Math.floor(Math.random() * isARockList.length)];

								} else {

									homeId = processedNodes.nodes[Math.floor(Math.random() * processedNodes.nodes.length)];

								}

							}

							if (homeId !== false) {

								var newHome = {

									node_id:homeId,
									user_id:userId,
									colony_name:userColonyObj.res.name,
									colony_stub:userColonyObj.res.stub,
									loc_angle:userColonyObj.res.loc_angle,
									loc_plane:userColonyObj.res.loc_plane

								};

								$state.db.query(that._sql.setNodeUser,newHome,function(csfErr,csfRes) {
										
									if (csfErr) {

										cb(csfErr);
									
									} else {
										var val = {};
										val.nu_id = rtn.res.nu_id = csfRes.insertId;
									
										sockey.modules.async.parallel([

											function(tcb){

												$state.db.query(that._sql.putUserColonyHome,[homeId,userId],function(csfnErr,csfnRes) {

													if (csfnErr) {

														tcb(csfnErr);

													} else {

														tcb();

													}

												});

											},
											function(tcb){

												$state.db.query(that._sql.setNodeUserAspects,val,function(csfnErr,csfnRes) {

													if (csfnErr) {

														tcb(csfnErr);

													} else {

														tcb();

													}

												});

											},
											function(tcb){

												$state.db.query(that._sql.setNodeUserModifiers,val,function(csfsErr,csfsRes) {

													if (csfsErr) {

														tcb(csfsErr);

													} else {

														tcb();

													}

												});

											},

										],function(errF){

											if (errF) {

												cb(errF);
											
											} else {

												cb();

											}

										});
										}

									});

							} else {

								cb('NO HOME FOUND');

							}

						}
					},

					// NPC Colonies
					function(cb) {

						var colonies = helper.npc(processedNodes.nodes,colonyData,nameData,function(colErr,colRes){

							if (colErr) {
								
								cb('FAILED COLONY CREATOR');
									
							} else {

								rtn.res.npcColonies = colRes;
								cb();

							}
							

						});
					}

				],

				function(err){

					if (err) {
						
						rtn.err = err;

					} else {

						sockey.modules.async.forEachOf(rtn.res.npcColonies,function(val,key,cbk){

							var nu_id = false;
								
							$state.db.query(that._sql.setNodeUser,val.root,function(csfErr,csfRes) {

								if (csfErr) {

									cbk(csfErr);
								
								} else {

									val.npc.nu_id = val.aspects.nu_id = csfRes.insertId;
									sockey.modules.async.parallel([

										function(tcb){

											$state.db.query(that._sql.setNodeUserNPC,val.npc,function(csfnErr,csfnRes) {

												if (csfnErr) {
													tcb(csfnErr);
												} else {
													tcb();
												}

											});

										},
										function(tcb){

											$state.db.query(that._sql.setNodeUserAspects,val.aspects,function(csfsErr,csfsRes) {

												if (csfsErr) {

													tcb(csfsErr);

												} else {

													tcb();

												}

											});

										},
										function(tcb){

											$state.db.query(that._sql.getDataNPCType,val.npc.type,function(csfpErr,csfpRes) {

												if (csfpErr) {

													tcb(csfpErr);

												} else {

													csfpRes[0].nu_id = val.npc.nu_id;
													$state.db.query(that._sql.setNodeUserModifiers,csfpRes[0],function(csftErr,csftRes) {

														if (csftErr) {

															tcb(csftErr);

														} else {
	
															tcb();

														}

													});

												}

											});

										},										

									],function(errF){

										if (errF) {
											
											cbk(errF);
										
										} else {

											cbk();

										}

									});

								}

							});

						},function(errN){

							if (errN) {

								rtn.err = errN;
								callback(rtn);

							} else {

								rtn.ok = true;
								callback(rtn);

							}

						});

					}
			
				}

			);

		} else {

			rtn.err = "JSON FILE LOOKUP ERROR";
			callback(rtn);

		}

	},

	create: function(sockey,$state,userId,callback) {

		var rtn = {ok:false},
			that = this,
			startTime = Date.now();
		sockey.modules.async.waterfall(

			[

				// Create the systems and nodes
				function(cb){
					
					that._initNewSystem(sockey,$state,function(r) {

						if (r.ok !== true) {

							cb(r);

						} else {

							cb(null,r.res);

						}

					});

				},

				// Name the cluster
				function(sys,cb){

					that._nameCluster(sockey,$state,function(cl) {

						if (cl.ok !== true) {

							cb(cl);

						} else {

							sys.cluster = cl.res;
							cb(null,sys);

						}

					});

				},

				// Add the cluster and assign this cluster to all stars
				function(sys,cb) {

					that._initCluster(sockey,$state,sys,userId,function(r){

						if (r.ok !== true) {
						
							cb(r);
						
						} else {

							sys = r.res;
							cb(null,sys);
						}

					});

				},

				// Name Stars
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.stars,function(val,key,fEOCallback) {
						
						that._nameStar(sockey,$state,sys.cluster.name,function(r) {

							if (r.ok !== true) {

								fEOCallback(r);

							} else {

								sys.stars[key].aspect.name = r.res,
								sys.stars[key].aspect.stub = that._func.stubby(r.res);

								if (key == 0) {

									sys.stars[key].aspect.discovered_by = userId,
									sys.stars[key].aspect.discovered_on = that._func.unixTimeStamp()

								}
								fEOCallback();

							}

						});

					},function(errt){
						if (errt) {
							cb(errt);
						} else {
							cb(null,sys);
						}

					});

				}, 

				// Insert star aspects
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.stars,function(val,key,cbk){

						// NOTE: SEPARATE FUNCTION FOR DISCOVER
						$state.db.query(that._sql.setStar,sys.stars[key].aspect, function(errt, rslt) {

							if (errt) {

								cbk(errt);
							
							} else {

								sys.stars[key].id = rslt.insertId;

								if (key == 0) {

									sys.stars[key].homeOfNodes = true;

								}

								cbk();

							}

						});

					},function(err){
						if (err) {

							cb(err);

						} else {

							cb(null,sys);

						}
						
					});	

				},

				// insert distances
				function(sys,cb) {
					
					var centralNode = false,
						starList = [];

					sockey.modules.async.forEachOf(sys.stars,function(val,key,cbk) {

						if (typeof val.homeOfNodes !== 'undefined') {

							if (val.homeOfNodes === true) {

								centralNode = val.id;

							}
							cbk();

						} else {

							cbk();

						}

					},function(errf){

						if (errf) {

							cb(errf);

						} else {

							var dice = require(sockey.DIR + '/helpers/builders/dice');
							sockey.modules.async.forEachOf(sys.stars,function(val,key,cbk){

								if (typeof val.homeOfNodes === 'undefined') {

									var insBlock = {star_from_id:centralNode};

										insBlock.star_to_id = val.id,
										insBlock.distance = val.distance,
										insBlock.incl = dice.roll({min:0,max:3600,div:10}),
										insBlock.plane = dice.roll({min:0,max:3600,div:10});

									$state.db.query(that._sql.setStarLink,insBlock,function(csErr,csRes){

										if (csErr) {

											cbk(csErr);

										} else {

											starList.push(insBlock);
											cbk();

										}

									});

								} else {

									cbk();

								}

							}, function(erfn){

								if (erfn) {

									cb(erfn);

								} else {

									sockey.modules.async.forEachOf(starList,function(val,key,cbk){

										val.star_from_id = val.star_to_id,
										val.star_to_id = centralNode;

										val.incl = that._func.reverseDegrees(val.incl);
										val.plane = that._func.reverseDegrees(val.plane);

										$state.db.query(that._sql.setStarLink,val,function(csErr,csRes){

											if (csErr) {

												cbk(csErr);

											} else {

												cbk();

											}

										});


									},
									function(errfe){

										if (errfe) {

											cb(errfe);

										} else {

											cb(null,sys);

										}

									});

								}

							});

						}

					});

				},

				// Name Nodes
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						var num = key + 1;
						sys.nodes[key].root = {
							star_id: sys.stars[0].id,
							name: sys.stars[0].aspect.name + ' '  + that._func.romanize(num),
							moniker: sys.stars[0].aspect.name + ' '  + that._func.romanize(num),
							stub: that._func.stubby(sys.stars[0].aspect.name + '-'  + that._func.romanize(num))
						};

						fEOCallback();

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},

				// INSERT NODE ROOTS
				function(sys,cb) {
					
					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						$state.db.query(that._sql.setNodeRoot,val.root,function(csErr,csRes){

							if (csErr) {

								fEOCallback(csErr);

							} else {

								sys.nodes[key].aspects.node_id = csRes.insertId;

								if (typeof val.seed.pop !== 'undefined') {

									sys.nodes[key].seed.node_id = csRes.insertId;

								}

								fEOCallback();
							}

						});

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},

				// INSERT NODE ASPECTS
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						$state.db.query(that._sql.setNodeAspects,val.aspects,function(csErr,csRes){

							if (csErr) {

								fEOCallback(csErr);

							} else {

								fEOCallback();
							}

						});

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},

				// SET NODE SEEDS
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						if (typeof val.seed.pop !== 'undefined') {

							$state.db.query(that._sql.setNodeSeeds,val.seed,function(csErr,csRes){

								if (csErr) {

									fEOCallback(csErr);

								} else {

									fEOCallback();
								}

							});
						} else {

							fEOCallback();

						}


					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},				

				// CREATE SUBNODES
				function(sys,cb) {

					var nodesModel = require(sockey.DIR+'/helpers/builders/nodes');

					var nodes = nodesModel.createFromNodes(sys.nodes,sockey.DIR,function(rtn) {

						if (rtn.ok === true) {

							sys.nodes = rtn.res;
							cb(null,sys);

						} else {

							cb(rtn.err);

						}

					});

				},

				// INSERT SUBNODES ROOT
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						if (typeof val.subnodes !== 'undefined') {

							if (val.subnodes.length > 0 ) {
								
								sockey.modules.async.forEachOf(val.subnodes,function(subVal,subKey,subfEOCallback) {

									$state.db.query(that._sql.setNodeRoot,subVal.root,function(csfErr,csfRes){

										if (csfErr) {

											subfEOCallback(csfErr);

										} else {
											c
											sys.nodes[key].subnodes[subKey].aspects.node_id = csfRes.insertId;
											if (typeof val.seed.pop !== 'undefined') {

												sys.nodes[key].subnodes[subKey].seed.node_id = csfRes.insertId;

											}
											subfEOCallback();
										}

									});

								},
								function(subErr){

									if (subErr) {
										
										fEOCallback(subErr);
									
									} else {
										
										fEOCallback();
									
									}

								});

							} else {

								fEOCallback();

							}

						} else {

							fEOCallback();

						}

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},


				// INSERT SUBNODES ASPECT
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						if (typeof val.subnodes !== 'undefined') {

							if (val.subnodes.length > 0 ) {
								
								sockey.modules.async.forEachOf(val.subnodes,function(subVal,subKey,subfEOCallback) {

									if (typeof subVal.aspects !== 'undefined') {

										$state.db.query(that._sql.setNodeAspects,subVal.aspects,function(csfErr,csfRes){

											if (csfErr) {

												subfEOCallback(csfErr);

											} else {

												subfEOCallback();
											}

										});

									} else {

										subfEOCallback();

									}

								},
								function(subErr){

									if (subErr) {
										
										fEOCallback(subErr);
									
									} else {
										
										fEOCallback();
									
									}

								});

							} else {

								fEOCallback();

							}

						} else {

							fEOCallback();

						}

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},

				// INSERT SUBNODES SEEDS
				function(sys,cb) {

					sockey.modules.async.forEachOf(sys.nodes,function(val,key,fEOCallback) {

						if (typeof val.subnodes !== 'undefined') {

							if (val.subnodes.length > 0 ) {
								
								sockey.modules.async.forEachOf(val.subnodes,function(subVal,subKey,subfEOCallback) {

									if (typeof subVal.seed.pop !== 'undefined') {

										$state.db.query(that._sql.setNodeSeeds,subVal.seed,function(csfErr,csfRes){

											if (csfErr) {

												subfEOCallback(csfErr);

											} else {

												subfEOCallback();
											}

										});

									} else {

										subfEOCallback();

									}

								},
								function(subErr){

									if (subErr) {
										
										fEOCallback(subErr);
									
									} else {
										
										fEOCallback();
									
									}

								});

							} else {

								fEOCallback();

							}

						} else {

							fEOCallback();

						}

					},function(errt){

						if (errt) {

							cb(errt);

						} else {

							cb(null,sys);

						}

					});

				},

			],

			function(err,result){
				
				if (err) {

					rtn.err = err;
					callback(rtn);

				} else {

					that._colonize(sockey,$state,result,userId,function(resOfColonize){

						if (resOfColonize.ok === true) {

							result.colonies = resOfColonize;
							rtn.res = result,
							rtn.ok = true;

							var endTime = Date.now() - startTime;

							console.log('Total Creation Proc Time: ' + endTime + ' ms');

						} else {

							rtn.err = resOfColonize.err
							
						}
						callback(rtn);

					});

				}

			}

		);

	},

	discover:function(sockey,$state,userId,starId,callback) {

		var rtn = {ok:false},
			that = this;

		sockey.modules.async.waterfall(

			[

				// Create the systems and nodes
				function(cb){
					
					that._initNewSystem(sockey,$state,function(r) {

						if (r.ok !== true) {
							cb(r);
						} else {
							cb(null,r.res);
						}

					},true);

				},

				function(sys,cb){

					that._nameCluster(sockey,$state,function(cl) {

						if (cl.ok !== true) {

							cb(cl);

						} else {

							sys.cluster = cl.res;
							cb(null,sys);

						}

					});

				},

			],
			function(err,sys) {

				if (err) {
					
					rtn.err = err;
					callback(rtn);
				
				} else {

					rtn.res = sys;
					rtn.ok = true
					callback(rtn);

				}

			}

		);

	}


};

module.exports = solarsystemRegistration;