module.exports = {
	_sql: {
		getStarAspectsByStub:"SELECT cs.name, cs.discovered_on,u.name as discovered_by,cs.r0,cs.mass,cs.lum,cs.spec_trans, ds.K, ds.spectral, ds.colorhex, ds.gl_min, ds.gl_max, ds.type, ds.name as star_type FROM `cluster_stars` AS cs LEFT JOIN users u ON u.id = cs.discovered_by INNER JOIN data_stars ds ON ds.id = cs.star_id WHERE cs.stub = ? ",
		getClusterByStarStub:"SELECT dc.name, dc.stub, c.image_starscape, c.created_on, u.name as created_by, c.year FROM `cluster_stars` AS cs INNER JOIN clusters c ON c.id = cs.cluster_id INNER JOIN data_clusters dc ON dc.id = c.cluster_id INNER JOIN users u ON u.id = c.created_by WHERE cs.stub = ?",
		getStarLinksByStarStub:"SELECT cs.stub, csl.distance, csl.plane, csl.incl FROM cluster_star_links AS csl INNER JOIN cluster_stars cs ON cs.id = csl.star_to_id INNER JOIN cluster_stars csP ON csP.id = csl.star_from_id INNER JOIN clusters c ON c.id = cs.cluster_id WHERE csP.stub = ?",	
	},

	mapStars: function(sockey,$state,listOfStars,callback,maxDepth,depth,obj) {

		var rtn = {ok:false},
			that = this,
			uSet = [];

		if (typeof maxDepth === 'undefined') {

			maxDepth = 5;

		}
		if (typeof depth === 'undefined') {

			depth = 0;

		}

		// if (typeof obj === 'undefined') {

		// 	obj = listOfStars;

		// }

		if (depth > maxDepth) {

			callback(listOfStars);
		
		}
	
		if (listOfStars.length < 1) {

			callback(listOfStars);

		}

		depth++;
		sockey.modules.async.forEachOf(listOfStars,function(val,key,cb) {

			if (that.arrayContains(that._walkedStars,val.stub) === false) {
					
				that._walkedStars.push(val.stub);

				sockey.db.get($state,that._sql.getStarAspectsByStub,val.stub,function(aspects){

					if (aspects.ok !== true) {

						cb(aspects);

					} else {

						listOfStars[key].aspects = aspects.res[0];
						sockey.db.get($state,that._sql.getStarLinksByStarStub,val.stub,function(r){

							if (r.ok !== true) {

								cb(r);
							
							} else {

								if (r.res.length > 0) {

									that.mapStars(sockey,$state,r.res,function(subRes){

										if (subRes.ok === true) {

											listOfStars[key].map = subRes.res;
											cb();

										} else {

											cb(subRes);

										}

									},maxDepth,depth);


								} else {

									listOfStars[key].map = [];
									cb();

								}
								
							}

						});

					}

				});

			} else {

				uSet.push(key);
				cb();
				
			}

		},
		function(err) {

			if (err) {

				rtn.err = err;

			} else {

				rtn.ok = true;

				uSet.forEach(function(u){
					listOfStars.splice(u,1);
				});

				rtn.res = listOfStars;

			}

			callback(rtn);

		});

	},

	getStar: function($state,sockey,stub,callback) {

		rtn = {ok:false},
		that = this;

		sockey.db.get($state,that._sql.getStarAspectsByStub,stub,function(r){

			if (r.ok !== true) {

				rtn.err = r;

			} else {

				if (r.res.length < 1) {
				
					rtn.err = "Star Not Found";
					callback(rtn);

				} else {

					var res = {
						
						stub:stub,
						aspects:r.res[0],

					};

					sockey.modules.async.parallel(
					
						[
							
							function(cb) {

								sockey.db.get($state,that._sql.getClusterByStarStub,stub,function(r){

									if(r.ok === true) {

										res.cluster = r.res[0];
										cb();

									} else {
										
										cb(r);
									
									}
								
								});

							},

							function(cb) {
								
								sockey.db.get($state,that._sql.getStarLinksByStarStub,stub,function(r){

									if(r.ok === true) {

										that._walkedStars = [stub],		
										res.map = r.res;

										that.mapStars(sockey,$state,res.map,function(mapResult){

											if (mapResult.ok === true) {

												res.map = mapResult.res;
												cb();

											} else {
												
												cb(mapResult);
											
											}

										},5);

									} else {
										
										cb(r);
									
									}

								});

							}
						],
						function(err){

							if (err) {

								rtn.err = err;
							
							} else {

								rtn.ok = true;
								rtn.res = res;
							
							}
							callback(rtn);
						}

					);

				}

			}

		});

	},

	arrayContains: function(a, obj) {
	
		for (var i = 0; i < a.length; i++) {
    
    		if (a[i] == obj) {
    
        		return true;
    
    		}
	
		}
		return false;
	
	},

};