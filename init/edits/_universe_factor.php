<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

/* SYSTEM CREATION */

/*
  Core confs
    - number of nodes
    - number of subnodes for gas, rock
    - distances
*/

$config['_uf_conf'] = array(

  'node_num'=> array(
    'min'=>5,
    'max'=>9
  ),
  'subnode_num'=> array(
    'gas'=>array(
      'min'=>3,
      'max'=>5
    ),
    'rock'=>array(
      'min'=>0,
      'max'=>2
    )
  ),
  'au_node'=>array(
    'max'=>400,
    'min'=>3,
    'div'=>10
  ),
  'au_subnode'=>array(
    'max'=>100,
    'min'=>20,
    'div'=>10
  )
);


/*
  Main Generation
    - _type
    - _opt
    - _orb
    - rotation
*/
$config['_uf_main'] = array(
  'type'=>array(

    'node'=>array(

      array(
        'type'=>'gas',
        'chance'=>'25'
      ),
      array(
        'type'=>'rock',
        'chance'=>'45'
      ),
      array(
        'type'=>'debris',
        'chance'=>'10'
      ),
      array(
        'type'=>'asteroid',
        'chance'=>'30'
      )

    ),
    'subnode'=>array(

      array(
        'type'=>'moon',
        'chance'=>'60'
      ),
      array(
        'type'=>'debris',
        'chance'=>'15'
      ),
      array(
        'type'=>'asteroid',
        'chance'=>'25'
      )

    )

  ),

  'ellipse' => array(
    'min'=>750,
    'max'=>1000,
    'div'=>1000,
    'chance'=>30
  ),

  'incl'=>array(
    'min'=>1,
    'max'=>20000,
    'div'=>1000
  ),

  'num_obj'=>array(
    'min'=>30,
    'max'=>50
  ),
  
  'haswater'=>70,

  'hasring'=>30,
  'ring_offset'=>array(
    'min'=>1,
    'max'=>18000,
    'div'=>100
  ),
  'ring_count'=>array(
    'min'=>1,
    'max'=>4
  ),

  'rotation'=>array(
    'max'=>100,
    'min'=>1,
    'div'=>100
  )

);

/*
  Types
    - atmosphere
    - surface
    - size
    - mass
*/
$config['_uf_main_types'] = array(

  'atmosphere'=>array(

    'gas'=>array(
      array(
        'chance'=>'80',
        'factor'=>array(
          'min'=>'0.2',
          'max'=>'0.4'
        ),
        'type'=>'toxic'
      ),
      array(
        'chance'=>'19',
        'factor'=>array(
          'min'=>'0.6',
          'max'=>'0.8',
        ),
        'type'=>'neutral'
      ),
      array(
        'chance'=>'1',
        'factor'=>array(
          'min'=>'0.9',
          'max'=>'1.1'
        ),
        'type'=>'clean'
      )
    ),
    'rock'=>array(
      array(
        'chance'=>'33',
        'factor'=>array(
          'min'=>'0.2',
          'max'=>'0.4'
        ),
        'type'=>'toxic'
      ),
      array(
        'chance'=>'45',
        'factor'=>array(
          'min'=>'0.6',
          'max'=>'0.8'
        ),
        'type'=>'neutral'
      ),
      array(
        'chance'=>'15',
        'factor'=>array(
          'min'=>'0.9',
          'max'=>'1.1'
        ),
        'type'=>'clean'
      ),
      array(
        'chance'=>'7',
        'factor'=>array(
          'min'=>'0.1',
          'max'=>'0.3'
        ),
        'type'=>'none'
      )
    ),
    'moon'=>array(
      array(
        'chance'=>'25',
        'factor'=>array(
          'min'=>'0.2',
          'max'=>'0.4'
        ),
        'type'=>'toxic'
      ),
      array(
        'chance'=>'12',
        'factor'=>array(
          'min'=>'0.6',
          'max'=>'0.8'
        ),
        'type'=>'neutral'
      ),
      array(
        'chance'=>'8',
        'factor'=>array(
          'min'=>'0.9',
          'max'=>'1.1'
        ),
        'type'=>'clean'
      ),
      array(
        'chance'=>'55',
        'factor'=>array(
          'min'=>'0.9',
          'max'=>'1.1'
        ),
        'type'=>'none'
      )
    )

  ),
  
  'surface'=>array(

    'rock'=>array(
      array(
        'chance'=>'20',
        'factor'=>'0.3',
        'type'=>'volcanic'
      ),
      array(
        'chance'=>'20',
        'factor'=>'0.7',
        'type'=>'barren'
      ),    
      array(
        'chance'=>'20',
        'factor'=>'0.8',
        'type'=>'desert'
      ),    
      array(
        'chance'=>'25',
        'factor'=>'1',
        'type'=>'water'
      ),
      array(
        'chance'=>'15',
        'factor'=>'0.4',
        'type'=>'broken'
      )
    ),
    'moon'=>array(
      array(
        'chance'=>'20',
        'factor'=>'0.3',
        'type'=>'volcanic'
      ),
      array(
        'chance'=>'20',
        'factor'=>'0.7',
        'type'=>'barren'
      ),    
      array(
        'chance'=>'20',
        'factor'=>'0.8',
        'type'=>'desert'
      ),    
      array(
        'chance'=>'25',
        'factor'=>'1',
        'type'=>'water'
      ),
      array(
        'chance'=>'15',
        'factor'=>'0.4',
        'type'=>'broken'
      )  
    ),
    'asteroid'=>array(
      array(
        'chance'=>'40',
        'factor'=>'0.4',
        'type'=>'broken'
      ),
      array(
        'chance'=>'60',
        'factor'=>'0.6',
        'type'=>'barren'
      )
    )

  ),

  'size'=>array(

    'rock'=>array(
      array(
        'chance'=>'40',
        'factor'=>array(
          'min'=>'1',
          'max'=>'2'
        ),
        'type'=>'small'
      ),
      array(
        'chance'=>'30',
        'factor'=>array(
          'min'=>'2',
          'max'=>'3'
        ),
        'type'=>'medium'

      ),    
      array(
        'chance'=>'20',
        'factor'=>array(
          'min'=>'3',
          'max'=>'4'
        ),
        'type'=>'large'
      ),    
      array(
        'chance'=>'10',
        'factor'=>array(
          'min'=>'4',
          'max'=>'5'
        ),
        'type'=>'super'
      )
    ),
    'gas'=>array(
      array(
        'chance'=>'40',
        'factor'=>array(
          'min'=>'3',
          'max'=>'6'
        ),
        'type'=>'small'
      ),
      array(
        'chance'=>'30',
        'factor'=>array(
          'min'=>'6',
          'max'=>'10'
        ),
        'type'=>'medium'
      ),    
      array(
        'chance'=>'20',
        'factor'=>array(
          'min'=>'10',
          'max'=>'15'
        ),
        'type'=>'large'
      ),    
      array(
        'chance'=>'10',
        'factor'=>array(
          'min'=>'15',
          'max'=>'20'
        ),
        'type'=>'super'
      )
    ),
    'moon'=>array(
      array(
        'chance'=>'50',
        'factor'=>array(
          'min'=>'0.1',
          'max'=>'0.2'
        ),
        'type'=>'small'
      ),
      array(
        'chance'=>'30',
        'factor'=>array(
          'min'=>'0.2',
          'max'=>'0.4'
        ),
        'type'=>'medium'
      ),
      array(
        'chance'=>'20',
        'factor'=>array(
          'min'=>'0.4',
          'max'=>'0.6'
        ),
        'type'=>'large'
      ),
    ),

  ),

  'mass'=>array(

    'rock'=>array(
      'min'=>100,
      'max'=>600,
      'div'=>100
    ),
    'gas'=>array(
      'min'=>100,
      'max'=>1000,
      'div'=>100
    )

  )

);

/*
  Stats for maximum allowed... for node
    - _pop
    - _mil
    - _agr
    - _sci
    - _ind
*/
$config['_uf_userstats'] = array(

  'rock'=>array(
    'pop'=>9000000000,
    'sci'=>24000,
    'agr'=>48000,
    'ind'=>48000,
    'mil'=>24000,
  ),
  'gas'=>array(
    'pop'=>900000000,
    'sci'=>60000,
    'agr'=>10000,
    'ind'=>200000,
    'mil'=>60000,    
  ),
  'moon'=>array(
    'pop'=>500000000,
    'sci'=>4000,
    'agr'=>4000,
    'ind'=>10000,
    'mil'=>10000,
  ),
  'asteroid'=>array(
    'pop'=>2500000,
    'sci'=>3000,
    'agr'=>1000,
    'ind'=>2000,
    'mil'=>3000,
  ),
  'debris'=>array(
    'pop'=>4000,
    'sci'=>100,
    'agr'=>100,
    'ind'=>250,
    'mil'=>300,
  )

);


/*
  Maximum resource extraction per planet for user 

*/
$config['_uf_resources'] = array(

    'iron'=>array(
      'rock'=>500,
      'gas'=>100,
      'moon'=>150,
      'asteroid'=>10,
      'debris'=>5
    ),
    'gold'=>array(
      'rock'=>300,
      'gas'=>50,
      'moon'=>75,
      'asteroid'=>50,
      'debris'=>10
    ),
    'titanium'=>array(
      'rock'=>75,
      'gas'=>50,
      'moon'=>50,
      'asteroid'=>25,
      'debris'=>5
    ),

    'phosphorus'=>array(
      'rock'=>500,
      'gas'=>100,
      'moon'=>100,
      'asteroid'=>25,
      'debris'=>5
    ),
    'feldspar'=>array(
      'rock'=>300,
      'gas'=>150,
      'moon'=>150,
      'asteroid'=>10,
      'debris'=>5
    ),
    'halite'=>array(
      'rock'=>150,
      'gas'=>75,
      'moon'=>75,
      'asteroid'=>50,
      'debris'=>10
    ),

    'terbium'=>array(
      'rock'=>300,
      'gas'=>150,
      'moon'=>75,
      'asteroid'=>30,
      'debris'=>10
    ),
    'uranium'=>array(
      'rock'=>300,
      'gas'=>100,
      'moon'=>100,
      'asteroid'=>75,
      'debris'=>25
    ),
    'argon'=>array(
      'rock'=>150,
      'gas'=>500,
      'moon'=>75,
      'asteroid'=>30,
      'debris'=>5
    ),

    'neodymium'=>array(
      'rock'=>100,
      'gas'=>75,
      'moon'=>75,
      'asteroid'=>30,
      'debris'=>10
    ),
    'tantalum'=>array(
      'rock'=>15,
      'gas'=>5,
      'moon'=>5,
      'asteroid'=>30,
      'debris'=>50
    ),
    'h3'=>array(
      'rock'=>25,
      'gas'=>500,
      'moon'=>100,
      'asteroid'=>10,
      'debris'=>5
    )

);

/*
  Resources

    'gold'=>0,          ind   rock    
    'titanium'=>0,      ind   rock
    platinum            ind   rock
    rhodium             ind   rock
     h3

    zeolites            agr   rock
    'phosphorus'=>      agr,  gas
    'feldspar'=>0,      agr   rock
    'halite'=>0,        agr   
    nox

    'terbium'=>0,       mil
    indium              mil,ast
    'molybdenum'=>0     mil
    uranium             mil
    argon

    'neodymium'=>0,     sci
    'beryllium'=>0,     sci
    'gallium'=>0,       sci
    Tantalum            sci,debr
    helium              sci
*/

?>