-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2016 at 02:14 AM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `__universe`
--

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `insdate` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clusters`
--

CREATE TABLE IF NOT EXISTS `clusters` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(255) NOT NULL COMMENT '// data_cluster_id',
  `image_starscape` varchar(254) NOT NULL,
  `tick` int(10) NOT NULL DEFAULT '0',
  `year` decimal(11,1) NOT NULL DEFAULT '1.0',
  `created_on` int(11) NOT NULL,
  `created_by` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cluster_stars`
--

CREATE TABLE IF NOT EXISTS `cluster_stars` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_id` int(255) NOT NULL,
  `cluster_id` int(254) NOT NULL,
  `name` varchar(128) NOT NULL,
  `stub` varchar(256) NOT NULL,
  `discovered_on` int(14) DEFAULT NULL,
  `discovered_by` int(255) DEFAULT NULL,
  `r0` decimal(10,3) NOT NULL,
  `mass` decimal(10,3) NOT NULL,
  `lum` decimal(10,3) NOT NULL,
  `spec_trans` decimal(5,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cluster_star_links`
--

CREATE TABLE IF NOT EXISTS `cluster_star_links` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_from_id` int(255) NOT NULL,
  `star_to_id` int(255) DEFAULT NULL,
  `plane` decimal(4,1) NOT NULL,
  `incl` decimal(4,1) NOT NULL,
  `distance` decimal(10,3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_buildings`
--

CREATE TABLE IF NOT EXISTS `data_buildings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` int(1) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `data_buildings`
--

INSERT INTO `data_buildings` (`id`, `name`, `type`, `icon`, `description`) VALUES
(20, 'Post office', 0, 'post_office.jpg', 'A post office generates money through its global postal network.'),
(21, 'Weather Station', 0, 'weather_station.jpg', 'A simple weather station for atmospheric data collection'),
(22, 'Launchpad', 1, 'launchpad.jpg', 'Your first starbase on a world is nothing more than a platform and a gantry for your rockets.\r\n\r\nNote: You only need one of these.'),
(23, 'Small Factory', 0, 'factory-sm.jpg', 'Small factory, producing industrial goods.'),
(24, 'mine', 0, '', 'A mine for extracting resources');

-- --------------------------------------------------------

--
-- Table structure for table `data_building_stats`
--

CREATE TABLE IF NOT EXISTS `data_building_stats` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `building_id` int(255) NOT NULL,
  `jobs` int(11) NOT NULL DEFAULT '0',
  `welfare` decimal(4,2) DEFAULT NULL,
  `stability` decimal(4,2) DEFAULT NULL,
  `productivity` decimal(4,2) DEFAULT NULL,
  `freedom` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `data_building_stats`
--

INSERT INTO `data_building_stats` (`id`, `building_id`, `jobs`, `welfare`, `stability`, `productivity`, `freedom`) VALUES
(25, 20, 3, 0.01, NULL, NULL, NULL),
(26, 21, 5, NULL, 0.01, NULL, NULL),
(27, 23, 10, NULL, NULL, 0.01, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_clusters`
--

CREATE TABLE IF NOT EXISTS `data_clusters` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `stub` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=767 ;

--
-- Dumping data for table `data_clusters`
--

INSERT INTO `data_clusters` (`id`, `name`, `stub`) VALUES
(1, 'Briareus', 'briareus'),
(2, 'Aigaion', 'aigaion'),
(3, 'Cottus', 'cottus'),
(4, 'Gyges', 'gyges'),
(5, 'Agrius', 'agrius'),
(6, 'Alcyoneus', 'alcyoneus'),
(7, 'Aloadae', 'aloadae'),
(8, 'Otos', 'otos'),
(9, 'Ephialtes', 'ephialtes'),
(10, 'Antaeus', 'antaeus'),
(11, 'Argus Panoptes', 'argus-panoptes'),
(13, 'Arges', 'arges'),
(14, 'Brontes', 'brontes'),
(15, 'Steropes', 'steropes'),
(16, 'Cyclopes', 'cyclopes'),
(17, 'Polyphemus', 'polyphemus'),
(18, 'Enceladus', 'enceladus'),
(19, 'Geryon', 'geryon'),
(20, 'Orion', 'orion'),
(21, 'Porphyrion', 'porphyrion'),
(22, 'Talos', 'talos'),
(23, 'Tityos', 'tityos'),
(24, 'Typhon', 'typhon'),
(25, 'Achlys', 'achlys'),
(26, 'Adephagia', 'adephagia'),
(27, 'Adikia', 'adikia'),
(28, 'Aergia', 'aergia'),
(29, 'Agon', 'agon'),
(30, 'Aidos', 'aidos'),
(31, 'Aisa', 'aisa'),
(32, 'Alala', 'alala'),
(33, 'Alastor', 'alastor'),
(34, 'Aletheia', 'aletheia'),
(35, 'Achos', 'achos'),
(36, 'Ania', 'ania'),
(37, 'Lupe', 'lupe'),
(38, 'Alke', 'alke'),
(39, 'Amechania', 'amechania'),
(40, 'Anaideia', 'anaideia'),
(41, 'Angelia', 'angelia'),
(42, 'Apate', 'apate'),
(43, 'Apheleia', 'apheleia'),
(44, 'Aporia', 'aporia'),
(45, 'Arete', 'arete'),
(46, 'Atë', 'atë'),
(47, 'Bia', 'bia'),
(48, 'Caerus', 'caerus'),
(49, 'Corus', 'corus'),
(50, 'Deimos', 'deimos'),
(51, 'Dikaiosyne', 'dikaiosyne'),
(53, 'Dolos', 'dolos'),
(54, 'Dysnomia', 'dysnomia'),
(55, 'Dyssebeia', 'dyssebeia'),
(57, 'Ekecheiria', 'ekecheiria'),
(58, 'Eleos', 'eleos'),
(59, 'Elpis', 'elpis'),
(60, 'Epiphron', 'epiphron'),
(61, 'Eris', 'eris'),
(63, 'Anteros', 'anteros'),
(64, 'Eros', 'eros'),
(65, 'Hedylogos', 'hedylogos'),
(66, 'Himeros', 'himeros'),
(67, 'Pothos', 'pothos'),
(68, 'Eucleia', 'eucleia'),
(69, 'Eulabeia', 'eulabeia'),
(71, 'Eupheme', 'eupheme'),
(72, 'Eupraxia', 'eupraxia'),
(73, 'Eusebeia', 'eusebeia'),
(74, 'Euthenia', 'euthenia'),
(75, 'Gelos', 'gelos'),
(76, 'Geras', 'geras'),
(77, 'Harmonia', 'harmonia'),
(78, 'Hebe', 'hebe'),
(79, 'Hedone', 'hedone'),
(80, 'Heimarmene', 'heimarmene'),
(81, 'Homados', 'homados'),
(82, 'Homonoia', 'homonoia'),
(83, 'Horkos', 'horkos'),
(84, 'Horme', 'horme'),
(85, 'Hybris', 'hybris'),
(86, 'Hypnos', 'hypnos'),
(87, 'Ioke', 'ioke'),
(88, 'Kakia', 'kakia'),
(89, 'Kalokagathia', 'kalokagathia'),
(90, 'Koalemos', 'koalemos'),
(91, 'Kratos', 'kratos'),
(92, 'Kydoimos', 'kydoimos'),
(94, 'Limos', 'limos'),
(95, 'Lyssa', 'lyssa'),
(96, 'Mania', 'mania'),
(97, 'Clotho', 'clotho'),
(98, 'Lachesis', 'lachesis'),
(99, 'Atropos', 'atropos'),
(100, 'Momus', 'momus'),
(101, 'Moros', 'moros'),
(102, 'Nemesis', 'nemesis'),
(103, 'Nike', 'nike'),
(104, 'Nomos', 'nomos'),
(105, 'Oizys', 'oizys'),
(106, 'Epiales', 'epiales'),
(107, 'Morpheus', 'morpheus'),
(108, 'Phantasos', 'phantasos'),
(109, 'Phobetor', 'phobetor'),
(110, 'Palioxis', 'palioxis'),
(111, 'Peitharchia', 'peitharchia'),
(112, 'Peitho', 'peitho'),
(113, 'Penia', 'penia'),
(114, 'Penthus', 'penthus'),
(115, 'Pepromene', 'pepromene'),
(116, 'Pheme', 'pheme'),
(117, 'Philophrosyne', 'philophrosyne'),
(118, 'Philotes', 'philotes'),
(119, 'Phobos', 'phobos'),
(120, 'Phrike', 'phrike'),
(121, 'Phthonus', 'phthonus'),
(122, 'Pistis', 'pistis'),
(123, 'Poine', 'poine'),
(124, 'Polemos', 'polemos'),
(125, 'Ponos', 'ponos'),
(126, 'Poros', 'poros'),
(127, 'Praxidike', 'praxidike'),
(128, 'Proioxis', 'proioxis'),
(129, 'Prophasis', 'prophasis'),
(130, 'Ptocheia', 'ptocheia'),
(131, 'Soter', 'soter'),
(132, 'Sophrosyne', 'sophrosyne'),
(133, 'Techne', 'techne'),
(135, 'Thrasos', 'thrasos'),
(136, 'Tyche', 'tyche'),
(137, 'Zelos', 'zelos'),
(139, 'Angelos', 'angelos'),
(140, 'Askalaphos', 'askalaphos'),
(141, 'Cerberus', 'cerberus'),
(142, 'Charon', 'charon'),
(143, 'Empusa', 'empusa'),
(144, 'Erebos', 'erebos'),
(145, 'Alecto', 'alecto'),
(146, 'Tisiphone', 'tisiphone'),
(147, 'Megaera', 'megaera'),
(148, 'Hecate', 'hecate'),
(152, 'Keuthonymos', 'keuthonymos'),
(153, 'Cronus', 'cronus'),
(154, 'Lamia', 'lamia'),
(155, 'Lampades', 'lampades'),
(156, 'Gorgyra', 'gorgyra'),
(157, 'Orphne', 'orphne'),
(158, 'Macaria', 'macaria'),
(159, 'Melinoe', 'melinoe'),
(160, 'Menoetes', 'menoetes'),
(161, 'Mormo', 'mormo'),
(162, 'Nyx', 'nyx'),
(165, 'Kokytos', 'kokytos'),
(169, 'Tartarus', 'tartarus'),
(170, 'Thanatos', 'thanatos'),
(171, 'Aegaeon', 'aegaeon'),
(172, 'Amphitrite', 'amphitrite'),
(173, 'Benthesikyme', 'benthesikyme'),
(174, 'Brizo', 'brizo'),
(175, 'Ceto', 'ceto'),
(176, 'Charybdis', 'charybdis'),
(177, 'Cymopoleia', 'cymopoleia'),
(178, 'Delphin', 'delphin'),
(179, 'Eidothea', 'eidothea'),
(181, 'Gorgons', 'gorgons'),
(182, 'Stheno', 'stheno'),
(183, 'Euryale', 'euryale'),
(185, 'Deino', 'deino'),
(187, 'Pemphredo', 'pemphredo'),
(188, 'Aello', 'aello'),
(189, 'Ocypete', 'ocypete'),
(190, 'Podarge', 'podarge'),
(192, 'Nicothoe', 'nicothoe'),
(193, 'Hippocampi', 'hippocampi'),
(194, 'Hydros', 'hydros'),
(195, 'Bythos', 'bythos'),
(196, 'Aphros', 'aphros'),
(197, 'Karkinos', 'karkinos'),
(198, 'Ladon', 'ladon'),
(199, 'Leucothea', 'leucothea'),
(200, 'Nereides', 'nereides'),
(201, 'Arethusa', 'arethusa'),
(202, 'Galene', 'galene'),
(203, 'Psamathe', 'psamathe'),
(204, 'Nereus', 'nereus'),
(205, 'Nerites', 'nerites'),
(206, 'Oceanus', 'oceanus'),
(208, 'Phorcys', 'phorcys'),
(209, 'Pontos', 'pontos'),
(210, 'Poseidon', 'poseidon'),
(211, 'Proteus', 'proteus'),
(212, 'Scylla', 'scylla'),
(213, 'Aglaope', 'aglaope'),
(214, 'Himerope', 'himerope'),
(215, 'Leucosia', 'leucosia'),
(216, 'Ligeia', 'ligeia'),
(217, 'Molpe', 'molpe'),
(218, 'Parthenope', 'parthenope'),
(219, 'Peisinoe', 'peisinoe'),
(220, 'Raidne', 'raidne'),
(221, 'Teles', 'teles'),
(223, 'Argyron', 'argyron'),
(224, 'Atabyrius', 'atabyrius'),
(225, 'Chalcon', 'chalcon'),
(226, 'Chryson', 'chryson'),
(227, 'Damon', 'damon'),
(229, 'Dexithea', 'dexithea'),
(230, 'Lycos', 'lycos'),
(231, 'Lysagora', 'lysagora'),
(232, 'Makelo', 'makelo'),
(233, 'Megalesius', 'megalesius'),
(234, 'Mylas', 'mylas'),
(235, 'Nikon', 'nikon'),
(236, 'Ormenos', 'ormenos'),
(237, 'Simon', 'simon'),
(238, 'Skelmis', 'skelmis'),
(239, 'Tethys', 'tethys'),
(240, 'Thalassa', 'thalassa'),
(241, 'Thaumas', 'thaumas'),
(242, 'Thoosa', 'thoosa'),
(243, 'Triteia', 'triteia'),
(244, 'Triton', 'triton'),
(245, 'Achelois', 'achelois'),
(247, 'Aether', 'aether'),
(248, 'Alectrona', 'alectrona'),
(249, 'Anemoi', 'anemoi'),
(250, 'Boreas', 'boreas'),
(251, 'Eurus', 'eurus'),
(252, 'Notus', 'notus'),
(253, 'Zephyrus', 'zephyrus'),
(254, 'Aparctias', 'aparctias'),
(255, 'Apheliotes', 'apheliotes'),
(256, 'Argestes', 'argestes'),
(257, 'Caicias', 'caicias'),
(258, 'Circios', 'circios'),
(259, 'Euronotus', 'euronotus'),
(260, 'Lips', 'lips'),
(261, 'Skeiron', 'skeiron'),
(263, 'Arke', 'arke'),
(264, 'Astraios', 'astraios'),
(265, 'Stilbon', 'stilbon'),
(266, 'Eosphorus', 'eosphorus'),
(267, 'Hesperus', 'hesperus'),
(268, 'Pyroeis', 'pyroeis'),
(269, 'Phaethon', 'phaethon'),
(270, 'Phaenon', 'phaenon'),
(271, 'Aurai', 'aurai'),
(272, 'Aura', 'aura'),
(273, 'Chaos', 'chaos'),
(274, 'Chione', 'chione'),
(275, 'Helios', 'helios'),
(276, 'Selene', 'selene'),
(277, 'Eos', 'eos'),
(278, 'Hemera', 'hemera'),
(279, 'Hera', 'hera'),
(280, 'Herse', 'herse'),
(281, 'Iris', 'iris'),
(282, 'Nephelai', 'nephelai'),
(283, 'Ouranos', 'ouranos'),
(284, 'Pandia', 'pandia'),
(285, 'Alcyone', 'alcyone'),
(286, 'Sterope', 'sterope'),
(289, 'Maia', 'maia'),
(290, 'Merope', 'merope'),
(291, 'Taygete', 'taygete'),
(292, 'Zeus', 'zeus'),
(293, 'Aetna', 'aetna'),
(294, 'Amphictyonis', 'amphictyonis'),
(295, 'Anthousai', 'anthousai'),
(298, 'Britomartis', 'britomartis'),
(299, 'Cabeiri', 'cabeiri'),
(300, 'Aitnaios', 'aitnaios'),
(301, 'Alkon', 'alkon'),
(302, 'Eurymedon', 'eurymedon'),
(303, 'Onnes', 'onnes'),
(304, 'Tonnes', 'tonnes'),
(305, 'Centaurs', 'centaurs'),
(306, 'Asbolus', 'asbolus'),
(307, 'Chariclo', 'chariclo'),
(308, 'Chiron', 'chiron'),
(309, 'Eurytion', 'eurytion'),
(310, 'Nessus', 'nessus'),
(311, 'Pholus', 'pholus'),
(312, 'Akmon', 'akmon'),
(313, 'Passalos', 'passalos'),
(314, 'Chloris', 'chloris'),
(315, 'Comus', 'comus'),
(316, 'Corymbus', 'corymbus'),
(317, 'Cybele', 'cybele'),
(318, 'Acmon', 'acmon'),
(319, 'Damnameneus', 'damnameneus'),
(320, 'Delas', 'delas'),
(321, 'Epimedes', 'epimedes'),
(323, 'Iasios', 'iasios'),
(324, 'Kelmis', 'kelmis'),
(325, 'Skythes', 'skythes'),
(326, 'Titias', 'titias'),
(327, 'Cyllenus', 'cyllenus'),
(329, 'Dryades', 'dryades'),
(330, 'Epimeliades', 'epimeliades'),
(331, 'Gaia', 'gaia'),
(332, 'Hamadryades', 'hamadryades'),
(333, 'Hecaterus', 'hecaterus'),
(334, 'Hephaestus', 'hephaestus'),
(335, 'Hermes', 'hermes'),
(336, 'Eunomia', 'eunomia'),
(337, 'Dike', 'dike'),
(338, 'Eirene', 'eirene'),
(339, 'Thallo', 'thallo'),
(340, 'Auxo', 'auxo'),
(341, 'Karpo', 'karpo'),
(342, 'Pherousa', 'pherousa'),
(343, 'Euporie', 'euporie'),
(344, 'Orthosie', 'orthosie'),
(345, 'Auge', 'auge'),
(346, 'Anatole', 'anatole'),
(347, 'Mousika', 'mousika'),
(348, 'Musica', 'musica'),
(349, 'Gymnastika', 'gymnastika'),
(350, 'Gymnastica', 'gymnastica'),
(351, 'Nymphe', 'nymphe'),
(352, 'Mesembria', 'mesembria'),
(353, 'Sponde', 'sponde'),
(354, 'Elete', 'elete'),
(355, 'Akte', 'akte'),
(356, 'Acte', 'acte'),
(357, 'Hesperis', 'hesperis'),
(358, 'Dysis', 'dysis'),
(359, 'Arktos', 'arktos'),
(360, 'Eiar', 'eiar'),
(361, 'Pthinoporon', 'pthinoporon'),
(362, 'Cheimon', 'cheimon'),
(363, 'Korybantes', 'korybantes'),
(364, 'Damneus', 'damneus'),
(365, 'Idaios', 'idaios'),
(366, 'Kyrbas', 'kyrbas'),
(367, 'Okythoos', 'okythoos'),
(368, 'Prymneus', 'prymneus'),
(369, 'Pyrrhichos', 'pyrrhichos'),
(370, 'Maenades', 'maenades'),
(371, 'Methe', 'methe'),
(372, 'Meliae', 'meliae'),
(373, 'Naiades', 'naiades'),
(374, 'Daphne', 'daphne'),
(375, 'Metope', 'metope'),
(376, 'Minthe', 'minthe'),
(377, 'Hekaerge', 'hekaerge'),
(378, 'Loxo', 'loxo'),
(379, 'Oupis', 'oupis'),
(380, 'Oreades', 'oreades'),
(381, 'Adrasteia', 'adrasteia'),
(382, 'Echo', 'echo'),
(383, 'Oceanides', 'oceanides'),
(384, 'Beroe', 'beroe'),
(385, 'Calypso', 'calypso'),
(386, 'Clytie', 'clytie'),
(387, 'Eidyia', 'eidyia'),
(388, 'Pan', 'pan'),
(389, 'Achelous', 'achelous'),
(390, 'Acis', 'acis'),
(391, 'Acheron', 'acheron'),
(392, 'Alpheus', 'alpheus'),
(393, 'Asopus', 'asopus'),
(394, 'Cladeus', 'cladeus'),
(396, 'Cocytus', 'cocytus'),
(397, 'Lethe', 'lethe'),
(398, 'Peneus', 'peneus'),
(399, 'Phlegethon', 'phlegethon'),
(400, 'Styx', 'styx'),
(401, 'Scamander', 'scamander'),
(402, 'Priapus', 'priapus'),
(403, 'Rhea', 'rhea'),
(404, 'Satyrs', 'satyrs'),
(405, 'Krotos', 'krotos'),
(406, 'Silenus', 'silenus'),
(407, 'Telete', 'telete'),
(408, 'Zagreus', 'zagreus'),
(409, 'Adonis', 'adonis'),
(410, 'Aphaea', 'aphaea'),
(411, 'Carme', 'carme'),
(412, 'Carmanor', 'carmanor'),
(413, 'Chrysothemis', 'chrysothemis'),
(414, 'Cyamites', 'cyamites'),
(415, 'Demeter', 'demeter'),
(416, 'Despoina', 'despoina'),
(417, 'Dionysus', 'dionysus'),
(419, 'Hestia', 'hestia'),
(420, 'Persephone', 'persephone'),
(421, 'Philomelus', 'philomelus'),
(422, 'Plutus', 'plutus'),
(424, 'Aiakos', 'aiakos'),
(425, 'Aeolus', 'aeolus'),
(428, 'Aristaeus', 'aristaeus'),
(430, 'Attis', 'attis'),
(431, 'Bolina', 'bolina'),
(433, 'Pollux', 'pollux'),
(434, 'Endymion', 'endymion'),
(436, 'Glaucus', 'glaucus'),
(437, 'Hemithea', 'hemithea'),
(438, 'Heracles', 'heracles'),
(439, 'Lampsace', 'lampsace'),
(441, 'Ino', 'ino'),
(442, 'Phoebe', 'phoebe'),
(443, 'Hilaeira', 'hilaeira'),
(445, 'Palaemon', 'palaemon'),
(446, 'Phylonoe', 'phylonoe'),
(447, 'Psyche', 'psyche'),
(448, 'Apollo', 'apollo'),
(449, 'Asclepius', 'asclepius'),
(450, 'Aceso', 'aceso'),
(451, 'Aegle', 'aegle'),
(452, 'Epione', 'epione'),
(453, 'Hygieia', 'hygieia'),
(454, 'Iaso', 'iaso'),
(455, 'Panacea', 'panacea'),
(456, 'Telesphorus', 'telesphorus'),
(457, 'Acratopotes', 'acratopotes'),
(458, 'Agdistis', 'agdistis'),
(459, 'Alexiares', 'alexiares'),
(460, 'Anicetus', 'anicetus'),
(461, 'Aphroditus', 'aphroditus'),
(462, 'Astraea', 'astraea'),
(463, 'Auxesia', 'auxesia'),
(464, 'Charites', 'charites'),
(465, 'Aglaea', 'aglaea'),
(466, 'Euphrosyne', 'euphrosyne'),
(468, 'Hegemone', 'hegemone'),
(469, 'Antheia', 'antheia'),
(470, 'Pasithea', 'pasithea'),
(471, 'Cleta', 'cleta'),
(472, 'Phaenna', 'phaenna'),
(473, 'Eudaimonia', 'eudaimonia'),
(474, 'Euthymia', 'euthymia'),
(475, 'Calleis', 'calleis'),
(476, 'Paidia', 'paidia'),
(477, 'Pandaisia', 'pandaisia'),
(478, 'Pannychis', 'pannychis'),
(479, 'Ceraon', 'ceraon'),
(480, 'Chrysus', 'chrysus'),
(481, 'Circe', 'circe'),
(482, 'Daemones Ceramici', 'daemones-ceramici'),
(483, 'Syntribos', 'syntribos'),
(484, 'Smaragos', 'smaragos'),
(485, 'Asbetos', 'asbetos'),
(486, 'Sabaktes', 'sabaktes'),
(487, 'Omodamos', 'omodamos'),
(488, 'Deipneus', 'deipneus'),
(489, 'Eiresione', 'eiresione'),
(490, 'Eileithyia', 'eileithyia'),
(491, 'Enyalius', 'enyalius'),
(492, 'Enyo', 'enyo'),
(493, 'Harpocrates', 'harpocrates'),
(494, 'Hermaphroditus', 'hermaphroditus'),
(495, 'Hymenaios', 'hymenaios'),
(496, 'Ichnaea', 'ichnaea'),
(497, 'Iynx', 'iynx'),
(498, 'Matton', 'matton'),
(499, 'Muses', 'muses'),
(500, 'Aoide', 'aoide'),
(501, 'Arche', 'arche'),
(502, 'Melete', 'melete'),
(503, 'Mneme', 'mneme'),
(504, 'Calliope', 'calliope'),
(505, 'Clio', 'clio'),
(506, 'Erato', 'erato'),
(507, 'Euterpe', 'euterpe'),
(508, 'Melpomene', 'melpomene'),
(509, 'Polyhymnia', 'polyhymnia'),
(510, 'Terpsichore', 'terpsichore'),
(511, 'Thalia', 'thalia'),
(512, 'Urania', 'urania'),
(513, 'Cephisso', 'cephisso'),
(514, 'Apollonis', 'apollonis'),
(515, 'Borysthenis', 'borysthenis'),
(516, 'Hypate', 'hypate'),
(517, 'Mese', 'mese'),
(518, 'Nete', 'nete'),
(519, 'Polymatheia', 'polymatheia'),
(520, 'Palaestra', 'palaestra'),
(521, 'Rhapso', 'rhapso'),
(522, 'Abderus', 'abderus'),
(523, 'Achilles', 'achilles'),
(524, 'Aeneas', 'aeneas'),
(525, 'Ajax', 'ajax'),
(526, 'Amphitryon', 'amphitryon'),
(527, 'Bellerophon', 'bellerophon'),
(528, 'Castor', 'castor'),
(529, 'Chrysippus', 'chrysippus'),
(530, 'Daedalus', 'daedalus'),
(533, 'Eunostus', 'eunostus'),
(534, 'Ganymede', 'ganymede'),
(535, 'Hector', 'hector'),
(536, 'Iolaus', 'iolaus'),
(537, 'Jason', 'jason'),
(538, 'Meleager', 'meleager'),
(540, 'Orpheus', 'orpheus'),
(543, 'Alcestis', 'alcestis'),
(544, 'Amymone', 'amymone'),
(545, 'Andromache', 'andromache'),
(546, 'Andromeda', 'andromeda'),
(547, 'Antigone', 'antigone'),
(548, 'Arachne', 'arachne'),
(549, 'Ariadne', 'ariadne'),
(550, 'Atalanta', 'atalanta'),
(551, 'Briseis', 'briseis'),
(552, 'Caeneus', 'caeneus'),
(554, 'Clytemnestra', 'clytemnestra'),
(555, 'Danaë', 'danaë'),
(556, 'Deianeira', 'deianeira'),
(557, 'Electra', 'electra'),
(558, 'Europa', 'europa'),
(559, 'Hecuba', 'hecuba'),
(560, 'Helen', 'helen'),
(561, 'Hermione', 'hermione'),
(562, 'Iphigenia', 'iphigenia'),
(563, 'Ismene', 'ismene'),
(564, 'Jocasta', 'jocasta'),
(565, 'Medea', 'medea'),
(566, 'Medusa', 'medusa'),
(567, 'Niobe', 'niobe'),
(568, 'Pandora', 'pandora'),
(569, 'Penelope', 'penelope'),
(570, 'Phaedra', 'phaedra'),
(571, 'Polyxena', 'polyxena'),
(572, 'Semele', 'semele'),
(573, 'Abas', 'abas'),
(574, 'Acastus', 'acastus'),
(575, 'Acrisius', 'acrisius'),
(576, 'Actaeus', 'actaeus'),
(577, 'Admetus', 'admetus'),
(578, 'Adrastus', 'adrastus'),
(579, 'Aeacus', 'aeacus'),
(580, 'Aeëtes', 'aeëtes'),
(581, 'Aegeus', 'aegeus'),
(582, 'Aegimius', 'aegimius'),
(583, 'Aegisthus', 'aegisthus'),
(584, 'Aegyptus', 'aegyptus'),
(585, 'Aeson', 'aeson'),
(586, 'Aëthlius', 'aëthlius'),
(587, 'Aetolus', 'aetolus'),
(588, 'Agamemnon', 'agamemnon'),
(589, 'Agasthenes', 'agasthenes'),
(590, 'Agenor', 'agenor'),
(591, 'Alcinous', 'alcinous'),
(592, 'Alcmaeon', 'alcmaeon'),
(593, 'Aleus', 'aleus'),
(594, 'Amphiaraus', 'amphiaraus'),
(595, 'Amphictyon', 'amphictyon'),
(596, 'Amphion', 'amphion'),
(597, 'Amycus', 'amycus'),
(598, 'Anaxagoras', 'anaxagoras'),
(599, 'Anchises', 'anchises'),
(600, 'Arcesius', 'arcesius'),
(601, 'Argeus', 'argeus'),
(602, 'Argus', 'argus'),
(603, 'Assaracus', 'assaracus'),
(604, 'Asterion', 'asterion'),
(605, 'Athamas', 'athamas'),
(606, 'Atreus', 'atreus'),
(607, 'Augeas', 'augeas'),
(608, 'Autesion', 'autesion'),
(609, 'Bias', 'bias'),
(610, 'Busiris', 'busiris'),
(611, 'Cadmus', 'cadmus'),
(612, 'Car', 'car'),
(613, 'Catreus', 'catreus'),
(614, 'Cecrops', 'cecrops'),
(615, 'Ceisus', 'ceisus'),
(616, 'Celeus', 'celeus'),
(617, 'Cephalus', 'cephalus'),
(619, 'Cepheus', 'cepheus'),
(620, 'Charnabon', 'charnabon'),
(621, 'Cinyras', 'cinyras'),
(622, 'Codrus', 'codrus'),
(623, 'Corinthus', 'corinthus'),
(624, 'Cranaus', 'cranaus'),
(626, 'Creon', 'creon'),
(627, 'Cres', 'cres'),
(628, 'Cresphontes', 'cresphontes'),
(629, 'Cretheus', 'cretheus'),
(630, 'Criasus', 'criasus'),
(631, 'Cylarabes', 'cylarabes'),
(632, 'Cynortas', 'cynortas'),
(633, 'Cyzicus', 'cyzicus'),
(634, 'Danaus', 'danaus'),
(635, 'Dardanus', 'dardanus'),
(636, 'Deiphontes', 'deiphontes'),
(637, 'Demophon', 'demophon'),
(638, 'Diomedes', 'diomedes'),
(639, 'Echemus', 'echemus'),
(640, 'Echetus', 'echetus'),
(641, 'Eetion', 'eetion'),
(642, 'Electryon', 'electryon'),
(643, 'Elephenor', 'elephenor'),
(644, 'Eleusis', 'eleusis'),
(645, 'Epaphus', 'epaphus'),
(646, 'Epopeus', 'epopeus'),
(647, 'Erechtheus', 'erechtheus'),
(648, 'Erginus', 'erginus'),
(649, 'Erichthonius', 'erichthonius'),
(651, 'Eteocles', 'eteocles'),
(652, 'Eurotas', 'eurotas'),
(653, 'Eurystheus', 'eurystheus'),
(654, 'Euxantius', 'euxantius'),
(655, 'Gelanor', 'gelanor'),
(656, 'Haemus', 'haemus'),
(658, 'Hippothoön', 'hippothoön'),
(659, 'Hyrieus', 'hyrieus'),
(660, 'Ilus', 'ilus'),
(662, 'Laërtes', 'laërtes'),
(663, 'Laomedon', 'laomedon'),
(664, 'Lycaon', 'lycaon'),
(665, 'Lycurgus', 'lycurgus'),
(666, 'Makedon', 'makedon'),
(667, 'Megareus', 'megareus'),
(669, 'Melanthus', 'melanthus'),
(670, 'Memnon', 'memnon'),
(671, 'Menelaus', 'menelaus'),
(672, 'Menestheus', 'menestheus'),
(673, 'Midas', 'midas'),
(674, 'Minos', 'minos'),
(675, 'Myles', 'myles'),
(676, 'Nestor', 'nestor'),
(677, 'Nycteus', 'nycteus'),
(678, 'Odysseus', 'odysseus'),
(679, 'Oebalus', 'oebalus'),
(680, 'Oedipus', 'oedipus'),
(681, 'Oeneus', 'oeneus'),
(682, 'Oenomaus', 'oenomaus'),
(683, 'Oenopion', 'oenopion'),
(684, 'Ogygus', 'ogygus'),
(685, 'Oicles', 'oicles'),
(686, 'Oileus', 'oileus'),
(687, 'Orestes', 'orestes'),
(688, 'Oxyntes', 'oxyntes'),
(689, 'Pandion', 'pandion'),
(690, 'Peleus', 'peleus'),
(691, 'Pelias', 'pelias'),
(692, 'Pelops', 'pelops'),
(693, 'Pentheus', 'pentheus'),
(694, 'Perseus', 'perseus'),
(695, 'Phineus', 'phineus'),
(696, 'Phlegyas', 'phlegyas'),
(697, 'Phoenix', 'phoenix'),
(698, 'Phoroneus', 'phoroneus'),
(699, 'Phyleus', 'phyleus'),
(700, 'Pirithoös', 'pirithoös'),
(701, 'Pittheus', 'pittheus'),
(702, 'Polybus', 'polybus'),
(703, 'Polynices', 'polynices'),
(704, 'Priam', 'priam'),
(705, 'Proetus', 'proetus'),
(706, 'Pylades', 'pylades'),
(707, 'Rhadamanthys', 'rhadamanthys'),
(708, 'Rhesus', 'rhesus'),
(709, 'Sarpedon', 'sarpedon'),
(711, 'Sithon', 'sithon'),
(712, 'Talaus', 'talaus'),
(713, 'Tegyrios', 'tegyrios'),
(714, 'Telamon', 'telamon'),
(715, 'Telephus', 'telephus'),
(716, 'Temenus', 'temenus'),
(717, 'Teucer', 'teucer'),
(718, 'Teutamides', 'teutamides'),
(719, 'Teuthras', 'teuthras'),
(720, 'Thyestes', 'thyestes'),
(721, 'Tisamenus', 'tisamenus'),
(722, 'Tyndareus', 'tyndareus'),
(723, 'Amphilochus', 'amphilochus'),
(724, 'Anius', 'anius'),
(725, 'Branchus', 'branchus'),
(726, 'Calchas', 'calchas'),
(727, 'Carnus', 'carnus'),
(728, 'Carya', 'carya'),
(729, 'Cassandra', 'cassandra'),
(730, 'Ennomus', 'ennomus'),
(731, 'Halitherses', 'halitherses'),
(732, 'Helenus', 'helenus'),
(733, 'Iamus', 'iamus'),
(734, 'Idmon', 'idmon'),
(735, 'Manto', 'manto'),
(736, 'Melampus', 'melampus'),
(737, 'Mopsus', 'mopsus'),
(738, 'Polyeidos', 'polyeidos'),
(739, 'Telemus', 'telemus'),
(740, 'Tiresias', 'tiresias'),
(741, 'Aegea', 'aegea'),
(742, 'Aella', 'aella'),
(743, 'Alcibie', 'alcibie'),
(744, 'Antandre', 'antandre'),
(745, 'Antiope', 'antiope'),
(746, 'Areto', 'areto'),
(747, 'Asteria', 'asteria'),
(748, 'Bremusa', 'bremusa'),
(749, 'Celaeno', 'celaeno'),
(750, 'Eurypyle', 'eurypyle'),
(751, 'Hippolyta', 'hippolyta'),
(752, 'Hippothoe', 'hippothoe'),
(753, 'Iphito', 'iphito'),
(754, 'Lampedo', 'lampedo'),
(755, 'Marpesia', 'marpesia'),
(756, 'Melanippe', 'melanippe'),
(757, 'Molpadia', 'molpadia'),
(758, 'Myrina', 'myrina'),
(759, 'Orithyia', 'orithyia'),
(760, 'Otrera', 'otrera'),
(761, 'Pantariste', 'pantariste'),
(762, 'Penthesilea', 'penthesilea'),
(763, 'Ixion', 'ixion'),
(764, 'Sisyphus', 'sisyphus'),
(765, 'Tantalus', 'tantalus'),
(766, 'Zethus', 'zethus');

-- --------------------------------------------------------

--
-- Table structure for table `data_events`
--

CREATE TABLE IF NOT EXISTS `data_events` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `data_events`
--

INSERT INTO `data_events` (`id`, `text`, `type`) VALUES
(1, '__PLANET__ is experiencing volcanic upheaval. Citizens are alarmed and many have been killed. The government is asking for urgent assistance.', 0),
(2, 'The __ORBITAL__ has registered an outbreak of an unknown STD. It causes people to vomit uncontrollably.', 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_event_action`
--

CREATE TABLE IF NOT EXISTS `data_event_action` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `event_id` text NOT NULL,
  `text` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_hulls`
--

CREATE TABLE IF NOT EXISTS `data_hulls` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `engines` int(10) NOT NULL,
  `hullpoints` int(10) NOT NULL,
  `base_attack` int(10) NOT NULL DEFAULT '0',
  `base_defense` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `data_hulls`
--

INSERT INTO `data_hulls` (`id`, `name`, `engines`, `hullpoints`, `base_attack`, `base_defense`) VALUES
(1, 'Gnat', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_hull_costs`
--

CREATE TABLE IF NOT EXISTS `data_hull_costs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hull_id` int(255) NOT NULL,
  `type` char(3) NOT NULL,
  `val` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `data_hull_costs`
--

INSERT INTO `data_hull_costs` (`id`, `hull_id`, `type`, `val`) VALUES
(1, 1, 'crd', 10000),
(2, 1, 'ind', 100),
(3, 1, 'sci', 100);

-- --------------------------------------------------------

--
-- Table structure for table `data_hull_models`
--

CREATE TABLE IF NOT EXISTS `data_hull_models` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hull_id` int(255) NOT NULL,
  `model` text NOT NULL,
  `created_on` int(10) DEFAULT NULL,
  `created_by` int(255) DEFAULT NULL,
  `modified_on` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `data_hull_models`
--

INSERT INTO `data_hull_models` (`id`, `hull_id`, `model`, `created_on`, `created_by`, `modified_on`) VALUES
(1, 1, '{\r\n    "faces":[33,2,0,1,3,0,1,2,3,33,3,7,6,2,3,4,5,0,33,7,5,4,6,4,6,7,5,33,0,4,5,1,1,7,6,2,33,0,2,6,4,1,0,5,7,33,5,7,3,1,6,4,3,2,33,50,51,10,11,8,9,10,11,33,52,15,14,53,12,13,14,15,33,12,13,19,18,16,17,18,18,33,55,53,14,38,19,15,14,20,33,38,14,34,48,20,14,21,22,33,54,50,11,39,23,8,11,24,33,18,19,23,22,18,18,25,26,33,37,9,17,41,27,28,18,18,33,36,12,18,40,29,16,18,18,33,9,8,16,17,28,9,18,18,33,42,22,26,44,30,26,31,32,33,40,18,22,42,18,18,26,30,33,41,17,21,43,18,18,33,34,33,17,16,20,21,18,18,35,33,33,45,25,29,47,36,37,38,39,33,43,21,25,45,34,33,37,36,33,21,20,24,25,33,35,40,37,33,22,23,27,26,26,25,41,31,33,46,30,31,47,42,43,44,39,33,25,24,28,29,37,40,45,38,33,26,27,31,30,31,41,44,43,33,44,26,30,46,32,31,43,42,33,48,34,35,49,22,21,46,47,33,14,15,35,34,14,13,46,21,33,39,11,33,49,24,11,48,47,33,11,10,32,33,11,10,49,48,33,15,39,49,35,13,24,47,46,33,32,48,49,33,49,22,47,48,33,24,44,46,28,40,32,42,45,33,28,46,47,29,45,42,39,38,33,23,43,45,27,25,34,36,41,33,27,45,47,31,41,36,39,44,33,19,41,43,23,18,18,34,25,33,16,40,42,20,18,18,30,35,33,20,42,44,24,35,30,32,40,33,8,36,40,16,9,29,18,18,33,13,37,41,19,17,27,18,18,33,52,54,39,15,12,23,24,13,33,10,38,48,32,10,20,22,49,33,51,55,38,10,9,19,20,10,33,9,37,55,51,28,27,19,9,33,12,36,54,52,16,29,23,12,33,36,8,50,54,29,9,8,23,33,37,13,53,55,27,17,15,19,33,12,52,53,13,16,12,15,17,33,8,9,51,50,9,28,9,8],\r\n    "uvs":[],\r\n    "vertices":[-1.00744,0.959034,1.0087,-1.00744,1.12068,4.92654,-1.00744,0.959033,-0.991295,-1.00744,1.12068,-4.90913,0.992555,0.959034,1.0087,0.992555,1.12068,1.0087,0.992555,0.959033,-0.991295,0.992555,1.12068,-0.991295,1,-1,-1,1,-1,1,-1,-1,1,-1,-1,-1,1,1,-1,0.999999,1,1,-1,1,1,-1,1,-1,1,-1,-1,1,-1,1,1,1,-1,0.999999,1,1,1,-1,-1,1,-1,1,1,1,-1,0.999999,1,1,1.67799,-0.5,-0.5,1.67799,-0.5,0.5,1.67799,0.5,-0.499999,1.67799,0.5,0.500001,2.96206,-0.124458,-0.503442,2.96206,-0.124458,0.496558,2.08343,0.5,-0.499999,2.08343,0.5,0.500001,-4.0271,0.025024,0.967456,-4.06082,0.014565,-0.952658,-3.823,0.673918,0.673918,-3.823,0.673918,-0.673919,1,-0,-1,1,0,1,-1,0,1,-1,-0,-1,1,-0,-1,1,0,1,1,-0,-1,1,0,1,1.67799,0,-0.5,1.67799,0,0.5,3.11505,0,-0.499999,3.11505,0,0.500001,-4.14908,-0,0.999999,-4.14908,-1e-06,-1,0.049308,-1,-1,0.049308,-1,1,0.049308,1,-1.45009,0.049307,1,1.45009,0.049308,-0,-1,0.049308,0,1],\r\n    "name":"CubeGeometry",\r\n    "normals":[-0.688375,-0.724723,-0.029786,-0.688375,-0.724723,0.029786,-0.082247,0.037599,0.99588,-0.082186,0.037599,-0.99588,0.835353,0.546007,-0.063387,0.47615,-0.878597,-0.036103,0.835353,0.546007,0.063387,0.47615,-0.878597,0.036103,0,-0.707083,-0.707083,0,-0.707083,0.707083,-0.120762,-0.733329,0.669027,-0.121708,-0.736045,-0.665853,0.007996,0.535508,-0.844447,-0.129276,0.750084,-0.648549,-0.129276,0.750084,0.648549,0.007996,0.535508,0.844447,0.148625,0.705191,0.693228,0.071505,0.339335,0.937925,0,0,1,0.005127,-0.109134,0.993988,-0.071078,-0.011841,0.997375,-0.510178,0.713706,0.479904,-0.490005,-0.313852,0.813227,0.005127,-0.109134,-0.993988,-0.072268,-0.015564,-0.997253,0.28959,0.196326,0.936766,0.447676,0.303537,0.84106,0.056764,-0.053957,0.996918,0,-0.316202,0.948668,0.71572,-0.680441,0.15717,0.94995,0,0.312387,0.476058,0.621815,-0.621815,0.311899,0.001556,-0.950102,0.28959,-0.196326,0.936766,0.312387,0,0.94995,0.447676,-0.303537,0.84106,0.312876,-0.001556,0.949767,0.557756,-0.607959,0.564989,0.349193,-0.66451,0.660634,0.822474,0.100864,0.559771,0.557939,-0.606952,-0.565935,0.476058,0.621815,0.621815,0.826228,0.090945,-0.555925,0.168065,0.73217,-0.660024,0.168065,0.73217,0.660024,0.345347,-0.657582,-0.669546,-0.510178,0.713706,-0.479904,-0.482894,-0.182409,-0.856441,-0.034883,-0.830378,-0.556078,-0.047243,-0.79577,0.603717],\r\n    "metadata":{\r\n        "normals":50,\r\n        "uvs":0,\r\n        "vertices":56,\r\n        "generator":"io_three",\r\n        "faces":52,\r\n        "type":"Geometry",\r\n        "version":3\r\n    }\r\n}', 1458596814, 1, 1458596814);

-- --------------------------------------------------------

--
-- Table structure for table `data_npc_types`
--

CREATE TABLE IF NOT EXISTS `data_npc_types` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `productivity` decimal(4,1) NOT NULL DEFAULT '50.0',
  `welfare` decimal(4,1) NOT NULL DEFAULT '50.0',
  `freedom` decimal(4,1) NOT NULL DEFAULT '50.0',
  `stability` decimal(4,1) NOT NULL DEFAULT '50.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `data_npc_types`
--

INSERT INTO `data_npc_types` (`id`, `name`, `productivity`, `welfare`, `freedom`, `stability`) VALUES
(1, 'agressive', 65.0, 40.0, 30.0, 65.0),
(2, 'passive', 50.0, 50.0, 50.0, 50.0),
(3, 'amenable', 40.0, 60.0, 60.0, 40.0);

-- --------------------------------------------------------

--
-- Table structure for table `data_resources`
--

CREATE TABLE IF NOT EXISTS `data_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `data_resources`
--

INSERT INTO `data_resources` (`id`, `name`) VALUES
(1, 'gold'),
(2, 'titanium'),
(3, 'iron'),
(5, 'h3'),
(7, 'phosphorus'),
(8, 'feldspar'),
(9, 'halite'),
(11, 'terbium'),
(14, 'uranium'),
(15, 'argon'),
(16, 'neodymium'),
(19, 'tantalum');

-- --------------------------------------------------------

--
-- Table structure for table `data_stars`
--

CREATE TABLE IF NOT EXISTS `data_stars` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` char(1) NOT NULL,
  `K` int(5) NOT NULL,
  `spectral` varchar(3) NOT NULL,
  `colorhex` char(6) NOT NULL DEFAULT 'FFFFFF',
  `node_max_gas` int(2) NOT NULL,
  `node_max_rock` int(2) NOT NULL,
  `gl_min` decimal(64,4) NOT NULL,
  `gl_max` decimal(64,4) NOT NULL,
  `avg_lum` decimal(8,2) NOT NULL,
  `avg_mass` decimal(5,2) NOT NULL,
  `avg_R0` decimal(3,1) NOT NULL,
  `avg_spec_trans` decimal(5,4) NOT NULL DEFAULT '1.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `data_stars`
--

INSERT INTO `data_stars` (`id`, `name`, `type`, `K`, `spectral`, `colorhex`, `node_max_gas`, `node_max_rock`, `gl_min`, `gl_max`, `avg_lum`, `avg_mass`, `avg_R0`, `avg_spec_trans`) VALUES
(1, 'White Dwarf', 'F', 6500, 'VII', 'ffffff', 1, 3, 0.2000, 0.5000, 1.20, 1.10, 0.4, 0.5000),
(2, 'Red Dwarf', 'M', 3000, 'VI', 'ff4d4d', 1, 3, 0.2000, 0.4000, 0.04, 0.40, 0.6, 1.0000),
(3, 'Yellow Dwarf', 'G', 5000, 'V', 'ffff4d', 2, 4, 0.3000, 0.5000, 0.60, 0.80, 0.5, 0.8000),
(4, 'Red Main Sequence', 'K', 4500, 'V', 'ff4d4d', 1, 4, 0.5000, 0.8000, 0.60, 0.60, 1.7, 1.0000),
(5, 'Yellow Main Sequence', 'G', 6500, 'V', 'ffff4d', 3, 5, 0.7000, 1.2000, 1.00, 1.00, 1.0, 0.8000),
(6, 'Red Subgiant', 'M', 5000, 'IV', 'ff4d4d', 2, 4, 1.5000, 3.0000, 3.00, 5.00, 4.8, 1.0000),
(7, 'Yellow Subgiant', 'G', 6000, 'IV', 'ffff4d', 3, 5, 1.8000, 3.5000, 10.00, 8.00, 3.0, 0.8000),
(8, 'Blue Subgiant', 'B', 8000, 'IV', '4d94ff', 4, 5, 2.3000, 5.0000, 15.00, 10.00, 3.0, 0.2500),
(9, 'Red Giant', 'M', 6000, 'III', 'ff4d4d', 3, 4, 1.5000, 3.0000, 15.00, 8.00, 6.0, 1.0000),
(10, 'Blue Giant', 'O', 10000, 'III', '4d94ff', 5, 7, 3.5000, 5.5000, 30.00, 25.00, 5.0, 0.2500),
(11, 'Red Supergiant', 'M', 8000, 'II', 'ff4d4d', 4, 5, 6.0000, 8.0000, 80.00, 20.00, 8.0, 1.0000),
(12, 'Blue Supergiant', 'O', 17000, 'II', '4d94ff', 6, 8, 10.5000, 15.8000, 150.00, 50.00, 7.0, 0.2500),
(13, 'Blue Hypergiant', 'O', 30000, 'I', '4d94ff', 7, 8, 25.0000, 37.3000, 200.00, 60.00, 10.0, 0.2500);

-- --------------------------------------------------------

--
-- Table structure for table `data_workers`
--

CREATE TABLE IF NOT EXISTS `data_workers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `data_workers`
--

INSERT INTO `data_workers` (`id`, `key`, `value`) VALUES
(1, 'year', '360'),
(2, 'year_incr', '0.1'),
(3, 'usernode_min_pop', '1600'),
(4, 'usernode_rate_birth', '225'),
(5, 'usernode_rate_death', '40'),
(6, 'usernode_rate_factor', '10000'),
(7, 'usernode_rate_variation', '100'),
(8, 'level_xp_base', '2'),
(9, 'level_xp_multiply', '1000');

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `moniker` varchar(100) NOT NULL,
  `stub` varchar(100) NOT NULL,
  `parent_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_aspects`
--

CREATE TABLE IF NOT EXISTS `node_aspects` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `au` decimal(60,4) NOT NULL,
  `rotation` decimal(4,3) NOT NULL,
  `rotation_offset` decimal(5,3) NOT NULL DEFAULT '0.000' COMMENT '0-pi()',
  `factor` decimal(30,10) NOT NULL,
  `orb_incl` decimal(6,3) NOT NULL,
  `orb_angle` decimal(6,3) NOT NULL,
  `orb_ellipse` decimal(5,4) NOT NULL DEFAULT '0.0000',
  `orb_ellipse_dir` decimal(6,3) DEFAULT '0.000',
  `opt_asteroids` int(5) NOT NULL DEFAULT '0',
  `opt_hasring` tinyint(4) DEFAULT NULL,
  `opt_ring_offset` decimal(32,4) NOT NULL,
  `opt_ring_count` int(2) NOT NULL,
  `opt_haswater` tinyint(4) NOT NULL DEFAULT '0',
  `opt_gl` tinyint(4) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `type_surface` varchar(32) DEFAULT NULL,
  `type_surface_quality` varchar(32) DEFAULT NULL,
  `type_mass` decimal(30,6) NOT NULL,
  `type_atmosphere` varchar(32) DEFAULT NULL,
  `type_atmosphere_density` int(1) NOT NULL,
  `type_size` varchar(32) DEFAULT NULL,
  `type_size_r0` decimal(64,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_resources`
--

CREATE TABLE IF NOT EXISTS `node_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `resource_id` int(255) NOT NULL,
  `val` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_seeds`
--

CREATE TABLE IF NOT EXISTS `node_seeds` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `pop` bigint(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_textures`
--

CREATE TABLE IF NOT EXISTS `node_textures` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `modified_by` int(255) NOT NULL,
  `modified_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_users`
--

CREATE TABLE IF NOT EXISTS `node_users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `node_id` int(255) NOT NULL,
  `colony_name` varchar(255) NOT NULL,
  `colony_stub` varchar(255) NOT NULL,
  `tick` int(10) NOT NULL DEFAULT '0',
  `year` decimal(61,1) NOT NULL DEFAULT '1.0',
  `loc_angle` decimal(4,1) NOT NULL,
  `loc_plane` decimal(4,1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_aspects`
--

CREATE TABLE IF NOT EXISTS `node_user_aspects` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `pop` bigint(255) NOT NULL DEFAULT '1600',
  `wages` int(10) NOT NULL DEFAULT '100000',
  `taxrate` decimal(4,1) NOT NULL DEFAULT '20.0',
  `jobs` bigint(255) NOT NULL DEFAULT '1600',
  `defence` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_buildings`
--

CREATE TABLE IF NOT EXISTS `node_user_buildings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `building_id` int(255) NOT NULL COMMENT 'data_buildings -> id',
  `amount` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_data`
--

CREATE TABLE IF NOT EXISTS `node_user_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stub` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_modifiers`
--

CREATE TABLE IF NOT EXISTS `node_user_modifiers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `productivity` decimal(4,1) NOT NULL DEFAULT '50.0',
  `welfare` decimal(4,1) NOT NULL DEFAULT '50.0',
  `stability` decimal(4,1) NOT NULL DEFAULT '50.0',
  `freedom` decimal(4,1) NOT NULL DEFAULT '50.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_npcs`
--

CREATE TABLE IF NOT EXISTS `node_user_npcs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `credits` bigint(255) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_resources`
--

CREATE TABLE IF NOT EXISTS `node_user_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `resource_id` int(3) NOT NULL,
  `value` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `node_id` int(255) DEFAULT NULL,
  `star_id` int(255) NOT NULL,
  `cluster_id` int(255) NOT NULL,
  `ship_id` int(255) DEFAULT NULL,
  `fleet_id` int(255) DEFAULT NULL,
  `created_on` int(10) NOT NULL,
  `modified_on` int(10) DEFAULT NULL,
  `deleted_on` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `story_data`
--

CREATE TABLE IF NOT EXISTS `story_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `story_id` int(255) NOT NULL,
  `cluster_year` decimal(11,1) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `insdate` int(10) NOT NULL,
  `auth` int(1) NOT NULL DEFAULT '0' COMMENT '9 = global admin',
  `credits` bigint(255) NOT NULL DEFAULT '0',
  `level` int(5) NOT NULL DEFAULT '1',
  `xp` int(255) NOT NULL DEFAULT '0',
  `node_home` int(255) DEFAULT NULL,
  `cluster_active` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_alliances`
--

CREATE TABLE IF NOT EXISTS `user_alliances` (
  `id` int(255) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_alliance_members`
--

CREATE TABLE IF NOT EXISTS `user_alliance_members` (
  `id` int(255) NOT NULL,
  `alliance_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat`
--

CREATE TABLE IF NOT EXISTS `user_chat` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `channel_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat_channels`
--

CREATE TABLE IF NOT EXISTS `user_chat_channels` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `admin_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat_logs`
--

CREATE TABLE IF NOT EXISTS `user_chat_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_events`
--

CREATE TABLE IF NOT EXISTS `user_events` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `event_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `event_action` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_fleets`
--

CREATE TABLE IF NOT EXISTS `user_fleets` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `user_id` int(255) NOT NULL,
  `location_node` int(255) DEFAULT NULL,
  `location_star` int(255) DEFAULT NULL,
  `destination_node` int(255) DEFAULT NULL,
  `destination_star` int(255) DEFAULT NULL,
  `altitude` decimal(10,3) NOT NULL,
  `home` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_keys`
--

CREATE TABLE IF NOT EXISTS `user_keys` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `insdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

CREATE TABLE IF NOT EXISTS `user_messages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id_from` int(255) DEFAULT NULL,
  `user_id_to` int(255) NOT NULL,
  `user_transfer_id` int(255) DEFAULT NULL,
  `subject` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `date` int(11) NOT NULL,
  `read` int(1) NOT NULL DEFAULT '0',
  `saved` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `message` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `node_id` int(255) DEFAULT NULL,
  `system_id` int(255) DEFAULT NULL,
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_ships`
--

CREATE TABLE IF NOT EXISTS `user_ships` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `stub` varchar(32) NOT NULL,
  `au` int(5) DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `hull_id` int(255) NOT NULL,
  `star_id` int(255) DEFAULT NULL,
  `node_id` int(255) DEFAULT NULL,
  `fleet_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
