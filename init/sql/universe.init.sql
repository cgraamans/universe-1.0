-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2016 at 02:15 AM
-- Server version: 5.5.52-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `__universe`
--

-- --------------------------------------------------------

--
-- Table structure for table `bans`
--

CREATE TABLE IF NOT EXISTS `bans` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `ip` varchar(64) NOT NULL,
  `hostname` varchar(255) NOT NULL,
  `reason` text NOT NULL,
  `insdate` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `clusters`
--

CREATE TABLE IF NOT EXISTS `clusters` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `cluster_id` int(255) NOT NULL COMMENT '// data_cluster_id',
  `image_starscape` varchar(254) NOT NULL,
  `tick` int(10) NOT NULL DEFAULT '0',
  `year` decimal(11,1) NOT NULL DEFAULT '1.0',
  `created_on` int(11) NOT NULL,
  `created_by` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cluster_stars`
--

CREATE TABLE IF NOT EXISTS `cluster_stars` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_id` int(255) NOT NULL,
  `cluster_id` int(254) NOT NULL,
  `name` varchar(128) NOT NULL,
  `stub` varchar(256) NOT NULL,
  `discovered_on` int(14) DEFAULT NULL,
  `discovered_by` int(255) DEFAULT NULL,
  `r0` decimal(10,3) NOT NULL,
  `mass` decimal(10,3) NOT NULL,
  `lum` decimal(10,3) NOT NULL,
  `spec_trans` decimal(5,4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cluster_star_links`
--

CREATE TABLE IF NOT EXISTS `cluster_star_links` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_from_id` int(255) NOT NULL,
  `star_to_id` int(255) DEFAULT NULL,
  `plane` decimal(4,1) NOT NULL,
  `incl` decimal(4,1) NOT NULL,
  `distance` decimal(10,3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_buildings`
--

CREATE TABLE IF NOT EXISTS `data_buildings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `type` int(1) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_building_stats`
--

CREATE TABLE IF NOT EXISTS `data_building_stats` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `building_id` int(255) NOT NULL,
  `jobs` int(11) NOT NULL DEFAULT '0',
  `welfare` decimal(4,2) DEFAULT NULL,
  `stability` decimal(4,2) DEFAULT NULL,
  `productivity` decimal(4,2) DEFAULT NULL,
  `freedom` decimal(4,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_clusters`
--

CREATE TABLE IF NOT EXISTS `data_clusters` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `stub` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=767 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_events`
--

CREATE TABLE IF NOT EXISTS `data_events` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `text` longtext NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_event_action`
--

CREATE TABLE IF NOT EXISTS `data_event_action` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `event_id` text NOT NULL,
  `text` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_hulls`
--

CREATE TABLE IF NOT EXISTS `data_hulls` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `engines` int(10) NOT NULL,
  `hullpoints` int(10) NOT NULL,
  `base_attack` int(10) NOT NULL DEFAULT '0',
  `base_defense` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_hull_costs`
--

CREATE TABLE IF NOT EXISTS `data_hull_costs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hull_id` int(255) NOT NULL,
  `type` char(3) NOT NULL,
  `val` int(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_hull_models`
--

CREATE TABLE IF NOT EXISTS `data_hull_models` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `hull_id` int(255) NOT NULL,
  `model` text NOT NULL,
  `created_on` int(10) DEFAULT NULL,
  `created_by` int(255) DEFAULT NULL,
  `modified_on` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_npc_types`
--

CREATE TABLE IF NOT EXISTS `data_npc_types` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `productivity` decimal(4,1) NOT NULL DEFAULT '50.0',
  `welfare` decimal(4,1) NOT NULL DEFAULT '50.0',
  `freedom` decimal(4,1) NOT NULL DEFAULT '50.0',
  `stability` decimal(4,1) NOT NULL DEFAULT '50.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_resources`
--

CREATE TABLE IF NOT EXISTS `data_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_stars`
--

CREATE TABLE IF NOT EXISTS `data_stars` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type` char(1) NOT NULL,
  `K` int(5) NOT NULL,
  `spectral` varchar(3) NOT NULL,
  `colorhex` char(6) NOT NULL DEFAULT 'FFFFFF',
  `node_max_gas` int(2) NOT NULL,
  `node_max_rock` int(2) NOT NULL,
  `gl_min` decimal(64,4) NOT NULL,
  `gl_max` decimal(64,4) NOT NULL,
  `avg_lum` decimal(8,2) NOT NULL,
  `avg_mass` decimal(5,2) NOT NULL,
  `avg_R0` decimal(3,1) NOT NULL,
  `avg_spec_trans` decimal(5,4) NOT NULL DEFAULT '1.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Table structure for table `data_workers`
--

CREATE TABLE IF NOT EXISTS `data_workers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `key` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `nodes`
--

CREATE TABLE IF NOT EXISTS `nodes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `star_id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `moniker` varchar(100) NOT NULL,
  `stub` varchar(100) NOT NULL,
  `parent_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_aspects`
--

CREATE TABLE IF NOT EXISTS `node_aspects` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `au` decimal(60,4) NOT NULL,
  `rotation` decimal(4,3) NOT NULL,
  `rotation_offset` decimal(5,3) NOT NULL DEFAULT '0.000' COMMENT '0-pi()',
  `factor` decimal(30,10) NOT NULL,
  `orb_incl` decimal(6,3) NOT NULL,
  `orb_angle` decimal(6,3) NOT NULL,
  `orb_ellipse` decimal(5,4) NOT NULL DEFAULT '0.0000',
  `orb_ellipse_dir` decimal(6,3) DEFAULT '0.000',
  `opt_asteroids` int(5) NOT NULL DEFAULT '0',
  `opt_hasring` tinyint(4) DEFAULT NULL,
  `opt_ring_offset` decimal(32,4) NOT NULL,
  `opt_ring_count` int(2) NOT NULL,
  `opt_haswater` tinyint(4) NOT NULL DEFAULT '0',
  `opt_gl` tinyint(4) DEFAULT NULL,
  `type` varchar(32) NOT NULL,
  `type_surface` varchar(32) DEFAULT NULL,
  `type_surface_quality` varchar(32) DEFAULT NULL,
  `type_mass` decimal(30,6) NOT NULL,
  `type_atmosphere` varchar(32) DEFAULT NULL,
  `type_atmosphere_density` int(1) NOT NULL,
  `type_size` varchar(32) DEFAULT NULL,
  `type_size_r0` decimal(64,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_resources`
--

CREATE TABLE IF NOT EXISTS `node_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `resource_id` int(255) NOT NULL,
  `val` int(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_seeds`
--

CREATE TABLE IF NOT EXISTS `node_seeds` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `pop` bigint(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_textures`
--

CREATE TABLE IF NOT EXISTS `node_textures` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `node_id` int(255) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `modified_by` int(255) NOT NULL,
  `modified_on` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_users`
--

CREATE TABLE IF NOT EXISTS `node_users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) DEFAULT NULL,
  `node_id` int(255) NOT NULL,
  `colony_name` varchar(255) NOT NULL,
  `colony_stub` varchar(255) NOT NULL,
  `tick` int(10) NOT NULL DEFAULT '0',
  `year` decimal(61,1) NOT NULL DEFAULT '1.0',
  `loc_angle` decimal(4,1) NOT NULL,
  `loc_plane` decimal(4,1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_aspects`
--

CREATE TABLE IF NOT EXISTS `node_user_aspects` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `pop` bigint(255) NOT NULL DEFAULT '1600',
  `wages` int(10) NOT NULL DEFAULT '100000',
  `taxrate` decimal(4,1) NOT NULL DEFAULT '20.0',
  `jobs` bigint(255) NOT NULL DEFAULT '1600',
  `defence` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_buildings`
--

CREATE TABLE IF NOT EXISTS `node_user_buildings` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `building_id` int(255) NOT NULL COMMENT 'data_buildings -> id',
  `amount` int(255) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_data`
--

CREATE TABLE IF NOT EXISTS `node_user_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `stub` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_modifiers`
--

CREATE TABLE IF NOT EXISTS `node_user_modifiers` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `productivity` decimal(4,1) NOT NULL DEFAULT '50.0',
  `welfare` decimal(4,1) NOT NULL DEFAULT '50.0',
  `stability` decimal(4,1) NOT NULL DEFAULT '50.0',
  `freedom` decimal(4,1) NOT NULL DEFAULT '50.0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_npcs`
--

CREATE TABLE IF NOT EXISTS `node_user_npcs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '0',
  `credits` bigint(255) NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `node_user_resources`
--

CREATE TABLE IF NOT EXISTS `node_user_resources` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nu_id` int(255) NOT NULL,
  `resource_id` int(3) NOT NULL,
  `value` int(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stories`
--

CREATE TABLE IF NOT EXISTS `stories` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `node_id` int(255) DEFAULT NULL,
  `star_id` int(255) NOT NULL,
  `cluster_id` int(255) NOT NULL,
  `ship_id` int(255) DEFAULT NULL,
  `fleet_id` int(255) DEFAULT NULL,
  `created_on` int(10) NOT NULL,
  `modified_on` int(10) DEFAULT NULL,
  `deleted_on` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `story_data`
--

CREATE TABLE IF NOT EXISTS `story_data` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `story_id` int(255) NOT NULL,
  `cluster_year` decimal(11,1) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `insdate` int(10) NOT NULL,
  `auth` int(1) NOT NULL DEFAULT '0' COMMENT '9 = global admin',
  `credits` bigint(255) NOT NULL DEFAULT '0',
  `level` int(5) NOT NULL DEFAULT '1',
  `xp` int(255) NOT NULL DEFAULT '0',
  `node_home` int(255) DEFAULT NULL,
  `cluster_active` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_alliances`
--

CREATE TABLE IF NOT EXISTS `user_alliances` (
  `id` int(255) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_alliance_members`
--

CREATE TABLE IF NOT EXISTS `user_alliance_members` (
  `id` int(255) NOT NULL,
  `alliance_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `level` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat`
--

CREATE TABLE IF NOT EXISTS `user_chat` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `channel_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat_channels`
--

CREATE TABLE IF NOT EXISTS `user_chat_channels` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `admin_id` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_chat_logs`
--

CREATE TABLE IF NOT EXISTS `user_chat_logs` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_events`
--

CREATE TABLE IF NOT EXISTS `user_events` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `event_id` int(255) NOT NULL,
  `user_id` int(255) NOT NULL,
  `event_action` int(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_fleets`
--

CREATE TABLE IF NOT EXISTS `user_fleets` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `user_id` int(255) NOT NULL,
  `location_node` int(255) DEFAULT NULL,
  `location_star` int(255) DEFAULT NULL,
  `destination_node` int(255) DEFAULT NULL,
  `destination_star` int(255) DEFAULT NULL,
  `altitude` decimal(10,3) NOT NULL,
  `home` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_keys`
--

CREATE TABLE IF NOT EXISTS `user_keys` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `insdate` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_messages`
--

CREATE TABLE IF NOT EXISTS `user_messages` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id_from` int(255) DEFAULT NULL,
  `user_id_to` int(255) NOT NULL,
  `user_transfer_id` int(255) DEFAULT NULL,
  `subject` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `date` int(11) NOT NULL,
  `read` int(1) NOT NULL DEFAULT '0',
  `saved` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_notifications`
--

CREATE TABLE IF NOT EXISTS `user_notifications` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `user_id` int(255) NOT NULL,
  `message` text NOT NULL,
  `type` varchar(32) NOT NULL,
  `node_id` int(255) DEFAULT NULL,
  `system_id` int(255) DEFAULT NULL,
  `dt` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_ships`
--

CREATE TABLE IF NOT EXISTS `user_ships` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `stub` varchar(32) NOT NULL,
  `au` int(5) DEFAULT NULL,
  `user_id` int(255) NOT NULL,
  `hull_id` int(255) NOT NULL,
  `star_id` int(255) DEFAULT NULL,
  `node_id` int(255) DEFAULT NULL,
  `fleet_id` int(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
