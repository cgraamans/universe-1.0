<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

	// Never go below this amount of population.
	$config['_universe_worker_min_pop'] = 1600;

	// How long is a tick (100 days) in seconds?
	$config['_universe_worker_tick'] = 30;

	// What is the percentage population increase every year?
	$config['_universe_worker_increase_pop'] = array('min'=>80,'max'=>140,'fact'=>100); 

	// What is the percentage population increase every year?
	$config['_universe_worker_increase_data_stats'] = array('min'=>80,'max'=>150,'fact'=>100); 

	// What is the default percentage of tax levied per population unit per planet
	$config['_universe_worker_wealth_fluctuation'] = array('min'=>100,'max'=>300,'fact'=>100,'offset'=>200);
  
?>