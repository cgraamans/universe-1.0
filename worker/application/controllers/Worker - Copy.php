<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends CI_Controller {

  private $debug = true;

  public function __construct() {
    parent::__construct();
    
    if (!$this->input->is_cli_request()) {
          
      die('CLI ONLY');
    
    }
    $this->load->model('m_worker');
    $this->load->model('m_worker_global');
    $this->config->load('_universe_worker');
    
  }

  private function _log($msg,$arr = false) {

    if ($this->debug !== false) {

      if ($arr === false) {

        echo $msg;

      } else {

        var_dump($msg);

      }
      

    }

    return false;

  }

  public function run($waitTimer = 10, $time = false) {

    $startDT = date('U');
    while(1) {

      $this->_log("-!- Start (".date('Y-m-d H:i:s').")\r\n");
      
      $startDTrun = date('U');
      $this->init($time);

      $this->_log("-!- End (".date('Y-m-d H:i:s')." / runtime ".(date('U') - $startDT)."s / thisrun: ".(date('U') - $startDTrun).")\r\n\r\n");

      sleep($waitTimer);

    }

  }

  private function init($time = false) {
  
    // timestamp
    $timestamp = date('U');
    if ($time !== false) {
    
      // Proc the passed tock
      if ((strlen($time) == 10) && (ctype_digit($time) !== false)) {
      
        $timestamp = $time;
            
      }
      
    }
    $this->_log(" - init ($timestamp)\n");
    
    //
    $yearpart = 3600;
    $yp = $this->m_worker_global->get_conf_keyval('year');
    if ($yp !== false) {
      $yearpart = (int)$yp;
    }
    
    $incr = 0.1;
    $yearpart_incr = $this->m_worker_global->get_conf_keyval('year_incr');
    if ($yearpart_incr !== false) {
      $incr = (float)$yearpart_incr;
    } 

    $tickRes = $this->tick($timestamp,$yearpart,$incr);
    if ($tickRes !== false) {

        $this->_log(" - tick complete ($timestamp)\n");
        // $this->_log($tickRes,true);

    }

    // ship outside tick

  } // init

  private function _tick_xp_level($xp,$level,$user_id) {

    $r = false;

    $xpVals = $this->_keyval(array('level_xp_base','level_xp_multiply'));
    if (($xpVals !== false) && (count($xpVals) >0)) {

      $xpForNextLevel = $xpVals['level_xp_multiply'] * pow($xpVals['level_xp_base'],$level-1);
      echo 'level: ';
      var_dump($xpForNextLevel);
      if ($xpForNextLevel <= $xp) {

        // update user level;
        $levelUp = $this->m_worker->set_userLevelUp($user_id);
        if ($levelUp !== false) {

          $r = true;

        }

      }

    }

    return $r;

  }

  private function _tick_xp($user_id,$un_id) {

    $rtn = false;

    $totalXP = $this->m_worker->get_userNodeBuildingXP($un_id);
    if ($totalXP !== false) {
      $incrXP = 0;
      foreach($totalXP as $xpBuilding) {

        if ($xpBuilding->total > 0) {

          $incrXP += $xpBuilding->total;

        }

      }

      if ($incrXP > 0) {

        $insBuildXP = $this->m_worker->set_userXP($user_id,$incrXP);
        if ($insBuildXP !== false) {

          $totalXPForUser = $this->m_worker->get_userXPLvl($user_id);
          if ($totalXPForUser !== false) {

            $isLvl = $this->_tick_xp_level($totalXPForUser->xp,$totalXPForUser->level,$user_id);
            if ($isLvl === true) {

              $totalXPForUser->level++;
              $this->_log('LEVEL-UP FOR '.$user_id);
            }

            $rtn = array('ok'=>true,'level'=>$totalXPForUser->level,'levelled'=>$isLvl,'xp'=>$totalXPForUser->xp);

          }

        }

      }

    }

    return $rtn;

  }


  private function tick($timestamp,$yearpart,$incr) {

    // CLUSTER IN RTN
    $return = array(
      'clusters'=>$this->_tick_cluster($timestamp,$yearpart,$incr),
      'colonies'=>array()
    );

    // COLONY
    $listOfColonies = $this->_tick_colony($timestamp,$yearpart,$incr);
    if ($listOfColonies !== false) {

      $this->_log(" - colony tick (".count($listOfColonies)." colonies)\n");
      foreach($listOfColonies as $colony) {

        // get node of colony
        $colony->maxStats = $this->m_worker->get_node_maxStats($colony->node_id);
        if ($colony->maxStats !== false) {

          $tickPopulation = $this->_tick_population($colony,(int)$colony->maxStats->max_pop,$incr);

          $tickBuildings = $this->_tick_building($colony->user_id,$colony->meta_id);

          $tickXP = $this->_tick_xp($colony->user_id,$colony->meta_id);

          $return['colonies'][] = array('data'=>$colony,'proc'=>array('population'=>$tickPopulation,'xp'=>$tickXP,'buildings'=>$tickBuildings));
        
          // BUILDINGS GO HERE

        }

      }
      
    }

    return $return;

  } // tick

  private function _keyval($array) {

    $return = false;
    $ok = true;

    $opt = $this->m_worker_global->get_conf_keyval($array);
    if (($opt !== false) && (is_array($opt))) {

      foreach($array as $ov) {

        if (!array_key_exists($ov, $opt)) {

          $ok = false;

        }

      }

      if ($ok === true) {

        $return = $opt;

      }
      
    }

    return $return;

  } // _keyval

  private function _tick_building($user_id,$un_id) {

    $r = false;

    $userBuildings = $this->m_worker->get_userBuildings($un_id);
    if (($userBuildings !== false) && (count($userBuildings) > 0)) {

      $rtn = true;
      foreach($userBuildings as $ub) {

        if ($ub->type == 'crd') {

          $mutation = $this->m_worker->set_user_credits($user_id,$ub->mutate);
          if ($mutation === false) {

            $rtn = false;

          }

        } else {

          $mutation = $this->m_worker->set_user_node_endeavour($un_id,$ub->type,$ub->mutate);
          if ($mutation === false) {

            $rtn = false;

          }          


        }

      }

      $r = $rtn;

    }

    return $r;

  }

  private function _tick_population_factorize($val,$factor){

        $rand = mt_rand(0,1);
        if ($rand == 0) {

          $return = $val * (1 - $factor);

        } else {

           $return = $val * (1 + $factor);

        }

        return $return;

  } // _tick_population_factorize

  private function _tick_population_variant($val,$factor) {

    return (mt_rand(1,(int)$val)/(int)$factor);

  } // _tick_population_variant

  private function _tick_population($colony,$max,$incr) {

    $return = false;
    
    $opt = $this->_keyval(array('usernode_min_pop','usernode_rate_birth','usernode_rate_death','usernode_rate_factor','usernode_rate_variation'));
    if ($opt !== false) {

      $colony->population = $this->m_worker->get_usernode_population($colony->meta_id);
      if ($colony->population === false) {
        $colony->population = (int)$opt['usernode_min_pop'];
      }

      if ($colony->population < (int)$opt['usernode_min_pop']) {
        $colony->population = (int)$opt['usernode_min_pop'];
      }

      $rtn = array(
        'rates'=>array(),
        'delta'=>0,
        'update'=>false,
        'population'=>array(
          'start'=>$colony->population,
          'max'=>0,
          'min'=>$opt['usernode_min_pop'],
          'output'=>$colony->population,
        )
      );

      $totalNodePopulation = $this->m_worker->get_node_population($colony->node_id);
      if (($totalNodePopulation !== false) && ($totalNodePopulation > 0)) {

        $rtn['population']['max'] = (int)$max - (int)$totalNodePopulation;

      }

      if ($rtn['population']['max'] > 0) { // could be 1 could be 30 billion

        $br = $this->_tick_population_variant($opt['usernode_rate_variation'],$opt['usernode_rate_factor']);
        $dr = $this->_tick_population_variant($opt['usernode_rate_variation'],$opt['usernode_rate_factor']);

        $rtn['rates']['birth'] = round($this->_tick_population_factorize($opt['usernode_rate_birth'],$br) * $incr);
        $rtn['rates']['death'] = round($this->_tick_population_factorize($opt['usernode_rate_death'],$dr) * $incr);
        $rtn['delta'] = (int)$rtn['rates']['birth'] - (int)$rtn['rates']['death'];

        $newPopulation = $colony->population+round(((int)$colony->population/$opt['usernode_rate_factor']) * $rtn['delta']);
        if ($newPopulation > (int)$rtn['population']['max']) {

          $newPopulation = (int)$rtn['population']['max'];

        }

        if ($newPopulation != (int)$colony->population) {

          $popRes = $this->m_worker->set_un_population($colony->meta_id,$newPopulation);
          if ($popRes !== false) {

            $msg = json_encode($rtn);
            $userNotify = $this->m_worker->ins_user_notification($colony->user_id,$msg,'population',$colony->node_id);
            if ($userNotify !== false) {

              $rtn['update'] = true;
              $rtn['population']['output'] = $newPopulation;

            } else {

              $this->_log(' - No notification');

            }

          } else {

            $this->_log(' - PROBLEM WITH INSERT');

          }

        } else {

          $this->_log(' - PROBLEM');

        }

      }

      $return = $rtn;
      
    } else {

      $this->_log(' - OPT LOOKUP FAILED');

    }

    return $return;

  }

  private function _tick_cluster($timestamp,$yearpart,$incr) {

    $return = false;
 
    $res = $this->m_worker->get_tick_cluster($timestamp);
    if ($res !== false) {

      if ($res != 0) {
      
        $tick_clusternode = array();
        foreach($res as $cluster) {

          $start_tick = $this->m_worker->put_tick_cluster($cluster,($timestamp+$yearpart),$incr);
          if ($start_tick !== false) {
           
            $tick_clusternode[] = $cluster;

          } else {

             $this->_log(" - _tick_cluster PUT Error\n");

          }
            
        }
        
        if (count($tick_clusternode) >0) {
          
          $return = $tick_clusternode;
          
        }

      }
      
    } else {
    
      $this->_log(" - _tick_cluster Error\n");
    
    }

    return $return;

  }

  private function _tick_colony($timestamp,$yearpart,$incr) {

    $return = false;
 
    $res = $this->m_worker->get_tick_usernode($timestamp);
    if ($res !== false) {

      if ($res != 0) {
      
        $tick_usernode = array();
        foreach($res as $usernode_data) {

          $start_tick = $this->m_worker->put_tick_usernode($usernode_data,($timestamp+$yearpart),$incr);
          if ($start_tick !== false) {
           
            $tick_usernode[] = $usernode_data;

          } else {

             $this->_log(" - _tick_colony PUT Error\n");

          }
            
        }
        
        if (count($tick_usernode) >0) {
          
          $return = $tick_usernode;
          
        }

      }
      
    } else {
    
      $this->_log(" - _tick_colony Error\n");
    
    }

    return $return; 

  } // _tick_colony


} // END CLASS
