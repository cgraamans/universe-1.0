<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends CI_Controller {

  private $debug = true;

  public function __construct() {
    parent::__construct();
    
    if (!$this->input->is_cli_request()) {
          
      die('CLI ONLY');
    
    }
    $this->load->model('m_worker');
    $this->load->model('m_worker_global');
    $this->config->load('_universe_worker');
    
  }

  private function _log($msg,$arr = false) {

    if ($this->debug !== false) {

      if ($arr === false) {

        echo $msg;

      } else {

        var_dump($msg);

      }
      

    }

    return false;

  }

  public function run($waitTimer = 10, $time = false) {

    $startDT = date('U');
    while(1) {

      $this->_log("-!- Start (".date('Y-m-d H:i:s').")\r\n");
      
      $startDTrun = date('U');
      $this->init($time);

      $this->_log("-!- End (".date('Y-m-d H:i:s')." / runtime ".(date('U') - $startDT)."s / thisrun: ".(date('U') - $startDTrun).")\r\n\r\n");

      sleep($waitTimer);

    }

  }

  private function init($time = false) {
  
    // timestamp
    $timestamp = date('U');
    if ($time !== false) {
    
      // Proc the passed tock
      if ((strlen($time) == 10) && (ctype_digit($time) !== false)) {
      
        $timestamp = $time;
            
      }
      
    }
    $this->_log(" - init ($timestamp)\n");
    
    //
    $yearpart = 3600;
    $yp = $this->m_worker_global->get_conf_keyval('year');
    if ($yp !== false) {

      $yearpart = (int)$yp;
    
    }
    
    $incr = 0.1;
    $yearpart_incr = $this->m_worker_global->get_conf_keyval('year_incr');
    if ($yearpart_incr !== false) {
      $incr = (float)$yearpart_incr;

    } 

    $tickRes = $this->tick($timestamp,$yearpart,$incr);
    if ($tickRes !== false) {

        $this->_log(" - tick complete ($timestamp)\n");
        // $this->_log($tickRes,true);

    }

    // ship outside tick

  } // init

  private function tick($timestamp,$yearpart,$incr) {

    // CLUSTER IN RTN
    $return = array(
      'clusters'=>$this->_tick_cluster($timestamp,$yearpart,$incr),
      'colonies'=>array()
    );

    // COLONY
    $listOfColonies = $this->_tick_colony($timestamp,$yearpart,$incr);
    if ($listOfColonies !== false) {

      var_dump($listOfColonies);
      
    }

    return $return;

  } // tick

  private function _tick_cluster($timestamp,$yearpart,$incr) {

    $return = false;
 
    $res = $this->m_worker->get_tick_cluster($timestamp);
    if ($res !== false) {

      if ($res != 0) {
      
        $tick_clusternode = array();
        foreach($res as $cluster) {

          $start_tick = $this->m_worker->put_tick_cluster($cluster,($timestamp+$yearpart),$incr);
          if ($start_tick !== false) {
           
            $tick_clusternode[] = $cluster;

          } else {

             $this->_log(" - _tick_cluster PUT Error\n");

          }
            
        }
        
        if (count($tick_clusternode) >0) {
          
          $return = $tick_clusternode;
          
        }

      }
      
    } else {
    
      $this->_log(" - _tick_cluster Error\n");
    
    }

    return $return;

  }

  private function _tick_colony($timestamp,$yearpart,$incr) {

    $return = false;
 
    $res = $this->m_worker->get_tick_usernode($timestamp);
    if ($res !== false) {

      if ($res != 0) {
      
        $tick_usernode = array();
        foreach($res as $usernode_data) {

          $start_tick = $this->m_worker->put_tick_usernode($usernode_data,($timestamp+$yearpart),$incr);
          if ($start_tick !== false) {
           
            $tick_usernode[] = $usernode_data;

          } else {

             $this->_log(" - _tick_colony PUT Error\n");

          }
            
        }
        
        if (count($tick_usernode) >0) {
          
          $return = $tick_usernode;
          
        }

      }
      
    } else {
    
      $this->_log(" - _tick_colony Error\n");
    
    }

    return $return; 

  } // _tick_colony


} // END CLASS
