<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_worker extends CI_Model {
  
  
  public function __construct() {
    parent::__construct();
  }
  
  public function get_tick_usernode($tick = false) {
  
    $return = false;
    
    $this->db->select('id as meta_id,node_id,user_id,year');
    $this->db->from('node_users');

    if ($tick !== false) {

      $this->db->where('tick <=',$tick);

    }
    
    $query = $this->db->get();
    if ($query) {
    
      $return = 0;
      if ($query->num_rows() > 0) {
	
	     $return = $query->result();
	
      }
      
    }
    
    return $return;
    
  } // get_tick_usernode

  public function put_tick_usernode($un,$tick,$incr) {
    
    $return = false;
    
    $passArr = array(
      'tick'=>$tick,
      'year'=>$un->year + $incr
    );

    $this->db->set($passArr);
    $this->db->where('id', $un->meta_id);
    $this->db->update('node_users');
  
    if ($this->db->affected_rows() > 0) {
      
      $return = true;
    
    }
  
    return $return;
  
  } // put_tick_usernode

  public function get_tick_cluster($tick) {

    $return = false;
    
    $this->db->select('id, year');
    $this->db->from('clusters');

    $this->db->where('tick <=',$tick);
    
    $query = $this->db->get();
    if ($query) {
    
      $return = 0;
      if ($query->num_rows() > 0) {
  
       $return = $query->result();
  
      }
      
    }
    
    return $return;

  } // get_tick_cluster

  public function put_tick_cluster($cluster,$tick,$incr) {
    
    $return = false;
    
    $passArr = array(
      'tick'=>$tick,
      'year'=>$cluster->year + $incr
    );
  
    $this->db->set($passArr);
    $this->db->where('id', $cluster->id);
    $this->db->update('clusters');
  
    if ($this->db->affected_rows() > 0) {
      
      $return = true;
    
    }
  
    return $return;

  }

  public function get_node_maxStats($node_id) {
  
    $return = false;

    $this->db->select('n.name as name, nse.*');
    $this->db->from('nodes as n');
    $this->db->join('node_stats ns', 'n.id = ns.node_id', 'inner');
    $this->db->join('node_stat_endeavour nse','nse.node_id = n.id','inner');
    $this->db->where('n.id',$node_id);

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
        $rslt = $query->result();
        $return = $rslt[0];

      }
      
    } 

    return $return;
  
  } // get_node_maxStats


  /* POPULATION SPECIFIC */
  public function get_usernode_population($un_id) {


    $rtn = false;

    $this->db->select('pop');
    $this->db->from('user_node_endeavour');
    $this->db->where('un_id',$un_id);

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
        $rslt = $query->result();
        $rtn = $rslt[0]->pop;

      }
      
    }     

    return $rtn;

  } // get_usernode_population


  public function get_node_population($node_id) {
  
    $return = false;
    
    $this->db->select_sum('une.pop');
    $this->db->from('user_node_endeavour as une');
    $this->db->join('user_nodes un','un.id = une.un_id');
    $this->db->where('un.node_id',$node_id);
    
    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
        $rslt = $query->result();
        $return = $rslt[0]->pop;

      }
      
    }

    return $return;
  
  } // get_node_population

  
  public function set_un_population($id,$value) {

    $return = false;

    $this->db->set(array('pop'=>$value));
    $this->db->where('un_id',$id);
    $this->db->update('user_node_endeavour');
    if ($this->db->affected_rows() > 0) {
      
      $return = true;
    
    }
  
    return $return;      

  } // set_un_population

  public function get_userBuildings($un_id) {


    $r = false;

    $this->db->select('db.name, dbs.`key` as type, (dbs.`value`* unb.amount) as mutate');
    $this->db->from('user_node_buildings as unb');
    $this->db->join('data_buildings db',' unb.building_id = db.id','inner');
    $this->db->join('data_building_stats dbs',' dbs.building_id = db.id','inner');
    $this->db->where('unb.un_id',$un_id);

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
          $r = $query->result();

      }
      
    }

    return $r;    

  }

  public function get_userNodeBuildingXP($un_id) {

    $r = false;

    $this->db->select('(unb.amount * db.xp) as total');
    $this->db->from('user_node_buildings as unb');
    $this->db->join('data_buildings db',' unb.building_id = db.id','inner');    
    $this->db->where('unb.un_id',$un_id);

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
          $r = $query->result();

      }
      
    }

    return $r;

  }

  public function set_userXP($user_id,$amount) {

    $r = false;

    $this->db->where('id', $user_id);
    $this->db->set('xp','xp+'.$amount, FALSE);
    $this->db->update('users');

    if ($this->db->affected_rows() > 0) {
      
      $r = true;
    
    }

    return $r;

  }

  public function get_userXPLvl($user_id) {

    $r = false;

    $this->db->select('xp,level');
    $this->db->from('users');
    $this->db->where('id',$user_id);

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {
  
        $rtn = $query->result();
        $r = $rtn[0];

      }

    }

    return $r;

  }

  public function set_userLevelUp($user_id) {

    $r = false;

    $this->db->where('id', $user_id);
    $this->db->set('level','level+1',FALSE);
    $this->db->update('users');

    if ($this->db->affected_rows() > 0) {
      
      $r = true;
    
    }

    return $r;

  }

  public function set_user_node_endeavour($un_id,$type,$incr) {

    $r = false;

    $this->db->where('un_id', $un_id);
    $this->db->set($type, $type.'+'.$incr, FALSE);
    $this->db->update('user_node_endeavour');

    if ($this->db->affected_rows() > 0) {
      
      $r = true;
    
    }

    return $r;    

  }

  public function set_user_credits($user_id,$incr) {

    $r = false;

    $this->db->where('id', $user_id);
    $this->db->set('credits', 'credits+'.$incr, FALSE);
    $this->db->update('users');

    if ($this->db->affected_rows() > 0) {
      
      $r = true;
    
    }

    return $r;

  }


  public function ins_user_notification($user_id,$msg,$type,$node_id=NULL,$system_id=NULL) {
    
    $insData = array(
      'user_id'=>$user_id,
      'message'=>$msg,
      'node_id'=>$node_id,
      'system_id'=>$system_id,
      'type'=>$type,
      'dt'=>date('U')
    );
    $this->db->insert('user_notifications',$insData);
    return $this->db->insert_id();

  } // ins_user_notification


 }