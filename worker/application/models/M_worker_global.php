<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class M_worker_global extends CI_Model {
  
  
  public function __construct() {
    parent::__construct();
  }
  

  /* TICK TOCK */
  public function get_conf_keyval($key) {
  
    $return = false;
    $useArr = false;

    $this->db->select('key,value');
    if (is_array($key)) {
      $useArr = true;
      $this->db->where_in('key',$key);
    } else {
      $this->db->where('key',$key);
    }
    
    $this->db->from('conf_keyval');

    $query = $this->db->get();
    if ($query) {
    
      if ($query->num_rows() > 0) {

        $rtn = $query->result();
        if ($useArr === false) {

          $return = $rtn[0]->value;

        } else {

          $return = array();
          foreach($rtn as $item) {
            $return[$item->key] = $item->value;
          }

        }
	
      }
      
    }
    
    return $return;
    
  }

 }