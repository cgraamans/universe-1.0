###################
What is Universe
###################

Universe is a scifi game api focussed on colony management, resource gathering and strategic war.

*******************
Release Information
*******************



**************************
Changelog and New Features
**************************


*******************
Server Requirements
*******************


************
Installation
************


*******
License
*******


*********
Resources
*********

***************
Acknowledgement
***************

***************
Brainstorm
***************

## Economics ##
1 unit = 160 ppl = 10 credits income(/30 *100 tax) = 3 credits tax per 160ppl

## Planet Creation Kit ##

- Solar system may contain 3-10 objects
- 70%/30% chance of rocky/jupiter
- Asteroid Belts, Debris fields
- Each object has 0-20 Moons, Asteroids or Debris Fields[debris field +ind]
- Habitable zone differs per star type and size
- 

_Gas Giant jupiter-class_

- atmos		chance	factors
  - toxic	80%	0.3
  - neutral	19%	0.7
  - clean	1%	1

- size			chance	factors
  - s - 50-150		50%	1-1.10
  - m - 150-250		33%	1.10-1.20
  - l - 250-300		17%	1.20-1.30

- distance from sun in au
  - 0.3 - 5 	11%	0.1-0.3
  - 5-10	12%	0.3-0.7
  - 10-100	50%	0.7-1.0
  - 100-200	27%	1.0-1.2

_Rocky planets_

- atmos		chance		factors
  - toxic		33%		0.1-0.3
  - neutral		17%		0.3-0.7
  - clean		13%		0.7-1
  - none		40%		0.01

- surface		chance
  - volcanic 	49%		0.0-0.1
  - barren 	50%		0.1-0.3
  - verdant 	1%		0.3-1.2

- distance from sun	chance		factors
  - close		15%		0.1 X 1/10th of an _au		0.1 - 0.7
  - goldilocks 		5%		1				0.8 - 1.3
  - far			80%		0.3 x _au distance 1-100	

- size factors
  - 0.3 - 1		80%		1
  - 1-5			10%		1 + 0.10 x _au
  - 10-25		7%		1.5 - 0.10 x _au
  - 25-50		3%		1.75 - 0.10 x _au
  
- surface types
  - iceball		0.9
  - lava		0.4
  
_Moons_

- atmos		chance		factors
  - none		75%		0.01-0.1
  - toxic		15%		0.1-0.3
  - neutral		8%		0.3-0.7
  - clean		2%		0.7-1.2

- surface	chance		factors
  - volcanic	20%		0.3
  - broken	14%		0.4
  - unstable	10%		0.5
  - barren	30%		0.6
  - wasteland	15%		0.7
  - desert	10%		0.8
  - algae	3%		0.9
  - temperate	2%		1
  - verdant	1%		1.1

- size factors
  - 0.05 - 0.10		80%		0.
  - 0.10 - 0.20		15%		1
  - 0.30 - 0.40		5%		1

_Asteroids_

- size factors
  - 0.005 - 0.010		80%		0.1-0.3
  - 0.010 - 0.020		15%		0.3-0.7
  - 0.030 - 0.040		5%		0.7-1

## PROC PLANET: GAS ##

1. determine AU
2. determine atmosphere ('none' is not possible)
3. determine size 

## PROC PLANET: ROCKY ##

1. determine AU
2. determine surface - if AU > 1.5 or < 0.7, iceball or lava if broken or barren
3. dtermine atmosphere (volcanic, lava cannot be clean).
4. determine size

## PROC EVENTS ##

1. on tick
2. 

## PROC USER CREATION ##

1. Create user with supplied credentials
2. Give individual user api key
3. Create solar system (Naming conventions)
4. create 6-16 objects (no orbitals, redo solar system object chance if only debris & asteroids) + outer debris field

- type chances:
  - asteroids         10%
  - gas giants        25%
  - rocky planets     50%
  - debris fields     20%

5. populate one planet nearest to goldilocks zone
6. do initial events (2) and message


