define(['app','require','opt','renderbox','topnavmenu','tweenjs','factoryStarMaker','factoryNodeMaker','factoryRenderer','draggable','raycaster','css!/assets/css/skybox','css!/vendors/font-awesome-4.6.3/css/font-awesome.min','modules/threejs/cluster'], function (app,require,opt) {

    'use strict';

    app.controller('StarBoxController', ['$scope','$routeParams','socketer','$route','$q','factoryRenderer','factoryStarMaker','factoryNodeMaker',function ($scope, $routeParams, socketer,$route,$q,fRenderer,fStarMaker,fNodeMaker) {

        var func = {

            _loop: function (iterations, process, exit) { 

                var index = 0,
                    done = false,
                    shouldExit = false;
                var loop = {
                    next:function(){
                        if(done){
                            if(shouldExit && exit){
                                return exit();
                            }
                        }
                        if(index < iterations){
                            
                            index++;
                            process(loop,index);
                        
                        } else {
                        
                            done = true;
                            if(exit) exit();
                        
                        }
                    },
                    iteration:function(){
                        return index - 1;
                    },
                    break:function(end){
                        done = true;
                        shouldExit = end;
                    }
                };
                loop.next();
                return loop;
        
            },

            log: function(msg,type){
                
                var setMsg = {
                    message:msg,
                    timestamp:Date.now(),
                    type:'success'
                };

                if (typeof type !== 'undefined') {

                    setMsg.type = type;

                }

                $scope.$parent.data.interface.log.push(setMsg);

            },
            
            animate: function() {
              
                $scope.spinner = false;
                requestAnimationFrame(func.animate);
                func.render();
        
            },

            render: function () {

                try {

                    if (typeof $scope.data === 'undefined') {
                        $scope.data = $scope.$parent.data
                    }

                    $scope.data.camera.updateProjectionMatrix();
                    $scope.data.controls.update();

                    $scope.data.raycasting.raycaster.setFromCamera( $scope.data.raycasting.mouse, $scope.data.camera );

                    var intersects = $scope.data.raycasting.raycaster.intersectObjects( $scope.data.scene.children,true );
                    $scope.data.raycasting.targets = [];
              
                    if (intersects.length > 0) {

                        $scope.data.raycasting.targets = intersects;

                    }

                    var renderRunList = [
                        fRenderer.init($scope.data.scene),
                        fRenderer.starOrbits($scope.data),
                        fRenderer.shaderTimer($scope.data),
                        fRenderer.nodeOrbits($scope.data),
                    ];

                    var errCheckPass = true,
                        errCheckPassErr = [];

                    renderRunList.forEach(function(v){

                        if (v !== true) {

                            errCheckPass = false,
                            errCheckPassErr.push(v);

                        }

                    });
                    if (errCheckPass === true) {

                        $scope.data.timer = Date.now();
                        $scope.data.renderer.render($scope.data.scene, $scope.data.camera);

                    } else {

                        // error handling plz

                    }

                } catch (err) {

                    console.log('render error');
                    console.log($scope);
                    console.log(err);

                }

            },

            focus: function (star,node) {

                var that = this;

                return $q(function(resolve,reject) {

                    func.log('Retrieving '+star,'debug');
                    fStarMaker.starGet(star)
                        .then(that.setStars)
                        .then(that.setSkyBox)
                        .then(that.setLights)
                        .then(that.setNodes)
                        .then(function() {
                            func.log('Focussing Done','debug');
                            console.log('FOCUS DONE');
                            console.log($scope.$parent.data);                            
                        })
                        .then(function(){
                            resolve();
                        })
                        .catch(function(e){
                           reject(e);
                        });

                });

            },

            setStars: function(star) {

                return $q(function(resolve,reject) {

                    try {

                        func.log('Wrangling Stars');

                        fStarMaker.starMap(star,$scope.$parent.data.loadTexture,function(starList){

                            if (starList.ok !== true) {

                                reject(starList);

                            } else {

                                $scope.$parent.data.tree.cluster = star.cluster;
                                $scope.$parent.data.tree.map = star.map;

                                var isStarsObj = $scope.$parent.data.scene.getObjectByName('Stars'),
                                    starScene;

                                if (typeof isStarsObj === 'undefined') {
            
                                    starScene = new THREE.Object3D();
                                    starScene.name = 'Stars';
                                    $scope.$parent.data.scene.add(starScene);

                                } else {

                                    starScene = isStarsObj;
                                
                                }

                                starList.res.forEach(function(newStar){

                                    var nS = newStar,
                                        isInTree = $scope.$parent.data.tree.stars.find(x=>x.stub == newStar.stub);

                                    if (typeof isInTree === 'undefined') { 
                                        
                                        nS.obj = fStarMaker.starMake(nS,$scope.$parent.data.loadTexture);
                                        
                                        nS.obj.position.x = nS.position.x * $scope.$parent.data.interface.scale.stars.distance,
                                        nS.obj.position.y = nS.position.y * $scope.$parent.data.interface.scale.stars.distance,
                                        nS.obj.position.z = nS.position.z * $scope.$parent.data.interface.scale.stars.distance;

                                        starScene.add(nS.obj);

                                        $scope.$parent.data.tree.stars.push(nS);
                                        $scope.$parent.data.interface.lists.stars.push(nS.stub);

                                    }

                                });

                                resolve();

                            }

                        });

                    } catch (err) {

                        reject(err);

                    }

                });

            },

            setSkyBox: function(){

                return $q(function(resolve,reject) {

                    try {

                        func.log('Painting the Sky');

                        if ($scope.$parent.data.interface.focus.cluster === false) {

                            func.log('New skyBox: '+$scope.$parent.data.tree.cluster.image_starscape,'debug');
                            var skyBox = fStarMaker.skyBox($scope.$parent.data.loadTexture,$scope.$parent.data.tree.cluster.image_starscape);

                            $scope.$parent.data.scene.add(skyBox);

                        } else {

                            if ($scope.$parent.data.tree.cluster.stub != $scope.$parent.data.interface.focus.cluster) {
                            

                                var oldSkyBox = $scope.$parent.data.scene.getObjectByName('skyBox',true);
                                if (typeof oldSkyBox !== 'undefined') {

                                    func.log('Replace skyBox: ' + $scope.$parent.data.tree.cluster.stub + ' -> '+ $scope.$parent.data.interface.focus.cluster,'debug');

                                    var skyBox = fStarMaker.skyBox($scope.$parent.data.loadTexture,$scope.$parent.data.tree.cluster.image_starscape);
                                    $scope.$parent.data.scene.add(skyBox);
                                    $scope.$parent.data.scene.remove(oldSkyBox);

                                }

                            }

                        }
                        
                        $scope.$parent.data.interface.focus.cluster = $scope.$parent.data.tree.cluster.stub;

                        resolve();

                    } catch (err) {

                        reject(err);

                    }

                });

            },

            setSubNodes: function() {

                return $q(function(resolve,reject) {

                    var t=0;
                    func.log('Sculpting Moons');
                    
                    func._loop($scope.$parent.data.tree.nodes.length,function(loop){

                        $scope.$parent.data.interface.lists.nodes.push($scope.$parent.data.tree.nodes[t].stub);
                        var node = $scope.$parent.data.tree.nodes[t];

                        if (node.subnodes.length > 0) {

                            fNodeMaker.create(node.subnodes,$scope.$parent.data.loadTexture,node.type_size_r0,node.type_mass,function(v){

                                if(v) {

                                    for(var u=0,d=v.length;u<d;u++) {
                                      
                                      $scope.$parent.data.interface.lists.nodes.push(node.subnodes[u].stub);
                                      
                                      var nodeListObj = fNodeMaker.positionNode(node.subnodes[u]);

                                      $scope.$parent.data.tree.nodes.push(nodeListObj);
                                      node.obj.add(nodeListObj.obj);

                                    }

                                }
                                t++;
                                loop.next();

                            },true);

                        } else {

                            t++;
                            loop.next();

                        }

                    },function() {

                        resolve();

                    });

                });

            },

            setNodes: function() {

                return $q(function(resolve,reject) {

                    try {

                        var currentStar;
                        $scope.$parent.data.interface.focus.star === false ? currentStar = $routeParams.star : currentStar = $scope.$parent.data.interface.focus.star;

                        if ($scope.$parent.data.interface.focus.starObj.aspects.discovered_on !== null) {
                            
                            var nodesObj = $scope.$parent.data.scene.getObjectByName('Nodes');
                            if (typeof nodesObj !== 'undefined') {

                                $scope.$parent.data.scene.remove(nodesObj);

                            }

                            $scope.$parent.data.tree.nodes = [];
                            func.log('Spinning Up Nodes');

                            fNodeMaker.init(currentStar,function(nodeList) {

                                if (nodeList) {

                                    func.log('Creating Top Nodes','debug');
                                    fNodeMaker.create(nodeList,$scope.$parent.data.loadTexture,$scope.$parent.data.interface.focus.starObj.properties.size,$scope.$parent.data.interface.focus.starObj.aspects.mass,function(rtnNodes) {

                                        if (rtnNodes.length > 0) {

                                            $scope.$parent.data.tree.nodes = rtnNodes;

                                            var nodeListObj = fNodeMaker.encapsulate($scope.$parent.data.interface.focus.starObj,$scope.$parent.data.tree.nodes);
                                            $scope.$parent.data.scene.add(nodeListObj);

                                            func.buildNodeTracks()
                                                .then(func.setSubNodes)
                                                .then(function(){

                                                    resolve();

                                                })
                                                .catch(function(e){

                                                    reject(e);
                                                
                                                });

                                        } else {

                                            resolve();

                                        }

                                    });

                                } else {

                                    resolve();

                                }

                            });

                        } else {

                            resolve();

                        }


                    } catch(e) {

                        reject(e);
                    }

                });

            },

            setLights: function() {

                return $q(function(resolve,reject) {

                    try {

                        var currentStar;
                        $scope.$parent.data.interface.focus.star === false ? currentStar = $routeParams.star : currentStar = $scope.$parent.data.interface.focus.star;

                        var inStarList = $scope.$parent.data.tree.stars.find(x => x.stub == currentStar);
                        if (typeof inStarList !== 'undefined') {

                            $scope.$parent.data.interface.focus.starObj = inStarList; 

                            var starLight = fStarMaker.light(inStarList.aspects.colorhex);
                            starLight.name  = inStarList.stub + "_LIGHT";
                            inStarList.obj.add(starLight);

                            resolve();
            
                        } else {

                            reject(currentStar);

                        }

                } catch(err) {

                  reject(err);
                
                }

              });

            },

            buildNodeTracks: function() {

                return $q(function(resolve,reject) {

                    func.log('Marking Tracks');

                    if ($scope.$parent.data.interface.focus.starObj !== false) {

                      try {

                        fNodeMaker.tracks($scope.$parent.data.tree.nodes,$scope.$parent.data.interface.focus.starObj.position,$scope.$parent.data.interface.focus.starObj.properties.size,function(rtnTracks){

                          if (rtnTracks.ok === true) {

                            $scope.$parent.data.scene.add(rtnTracks.res);
                            resolve();

                          } else {
                            
                            func.log('Error Creating Node Tracks','error');
                            reject(rtnTracks.err);

                          }

                        });
                      
                      } catch(err) {

                        reject(err);

                      }

                    } else {
                        
                      resolve();

                    }

                });

            },

        };

        $scope.DEBUG = true,
        $scope.navOverlay = false,
        $scope.spinner = true;

        $scope.messages = {

            enabled:false,
            timer:false,

            clearTimer: function(){

                if (this.timer !== false) {

                    clearTimeout(this.timer);
                    this.timer = false;   

                } 
            
            },

            enable:function(msg,type){

                var that = this;

                that.message = msg.message,
                that.type = type,
                that.time = msg.timestamp,
                that.clearTimer(),
                that.enabled = true;

                that.timer = setTimeout(function(){

                    that.enabled = false;

                },opt.interface.timers.messages);

            },

            close:function(){

                this.clearTimer();
                this.enabled = false;

            }

        }

        $scope.$parent.$watch('data.interface.log',function (n,o) {

            if (n) {

                console.log("newMessage!")

                var message = $scope.$parent.data.interface.log[$scope.$parent.data.interface.log.length-1];

                if (message.type == 'success') {

                    $scope.messages.enable(message,'message-green');

                }
                if (message.type == 'error') {

                    $scope.messages.enable(message,'message-green');
                
                }            

            }

            

        },true);

        $scope.$watch('data.timestamp',function (n,o) {
                    
            if (n) {

                $scope.$parent.data = $scope.data;
                $scope.navOverlay = true;

                var focus = func.focus($routeParams.star)
                    .then(func.animate)
                    .then(function(){

                        var inStarList = $scope.$parent.data.tree.stars.find(x => x.stub == $routeParams.star);
                        if (typeof inStarList !== 'undefined') {

                            console.log('inStarList');
                            console.log(inStarList);

                            $scope.$parent.data.interface.focus.star = $routeParams.star;

                        }

                    })
                    .catch(function(e){

                        console.log('error');
                        console.log(e);
                    
                    });
                
                // $scope.$parent.$watch('data.interface.focus.node',function(n,o){

                //     if(n) {

                //         if ($scope.$parent.data.interface.focus.node !== false) {

                //             var nodeFound = $scope.$parent.data.tree.nodes.find(x => x.stub == $scope.$parent.data.interface.focus.node);
                //             if (typeof nodeFound === 'undefined') {
                //                 console.log('node not found')
                //                 // node not found
                //             } else {

                //                 $scope.$parent.data.interface.focus.star = false;
                //                 nodeFound.obj.add($scope.$parent.data.camera);
                //                 $scope.$parent.data.interface.focus.nodeObj = nodeFound;
                //                 $scope.$parent.data.interface.last.focus = 'node';

                //             }

                //         }

                //     }
                    
                // },true);




                // if ($scope.data.tree.stars.length > 0) {
                //     $scope.data.interface.focus.star = $scope.data.tree.stars[0].stub;
                // }

                // if (typeof $routeParams.node !== 'undefined') {

                //     var nodeTarget = $scope.data.scene.getObjectByName($routeParams.node,true);
                //     if (nodeTarget) {

                //         $scope.data.interface.focus.node = $routeParams.node,
                //         nodeTarget.add($scope.data.camera);
                    
                //     }

                // }
            }


        },true);
        
        $scope.$parent.$watch('data.interface.focus.star',function(n,o){

            if(n) {

                console.log('star watch triggered!!');

               if ($scope.$parent.data.interface.focus.star !== false) {

                    var starFound = $scope.$parent.data.tree.stars.find(x => x.stub == $scope.$parent.data.interface.focus.star);
                    if (typeof starFound === 'undefined') {

                        console.log('star not found');
                    
                    } else {

                        starFound.obj.add($scope.$parent.data.camera);
                        $scope.$parent.data.interface.last.focus = 'star';

                        if ($routeParams.star != starFound.stub) {

                            var focal = func.focus(starFound.stub).then(function(){
                                
                                console.log('new star');

                            });

                        }

                    }

               }

            }

        },true);
        
        $scope.$parent.$watch('data.interface.focus.node',function(n,o){

            if(n) {

               if ($scope.$parent.data.interface.focus.node !== false) {

                    var nodeFound = $scope.$parent.data.tree.nodes.find(x => x.stub == $scope.$parent.data.interface.focus.node);
                    if (typeof nodeFound === 'undefined') {

                        console.log('star not found');
                    
                    } else {

                        nodeFound.obj.add($scope.$parent.data.camera);
                        $scope.$parent.data.interface.focus.nodeObj = nodeFound;
                        $scope.$parent.data.interface.last.focus = 'node';

                        if ($routeParams.star != nodeFound.stub) {

                            var focal = func.focus(starFound.stub).then(function(){
                                
                                console.log('new star')

                            });

                        }

                    }

               }

            }

        },true);        



        $scope.moveIndex = {

            goto: function(type,list,index) {

                var starFound = $scope.$parent.data.tree[type+'s'].find(x => x.stub == list[index]);
                console.log('list[index]');
                console.log(list[index]);
                console.log($scope.$parent.data.tree[type+'s']);
                if (typeof starFound !== 'undefined') {

                    $scope.$parent.data.interface.focus[type] = list[index];
                    $scope.$parent.data.interface.focus[type+'Obj'] = starFound;
                
                }

            },

            index: function(focal,list) {
                return list.indexOf($scope.$parent.data.interface.focus[focal]);
            },

            increment: function(direction,index,list) {

                switch(direction){
                    case 'n':
                        if (index >=  list.length-1) {

                            index = 0;

                        } else {

                            index += 1;

                        }
                        break;
                    case 'p':
                        if (index <= 0) {

                            index = list.length-1;

                        } else {

                            index--;
                            
                        }
                        break;
                }

                return index;

            },

            star: {

                next: function() {

                    var index = $scope.moveIndex.index('star',$scope.$parent.data.interface.lists.stars);
                    if (typeof index !== 'undefined') {

                        var newIdx = $scope.moveIndex.increment('n',index,$scope.$parent.data.interface.lists.stars);
                        $scope.moveIndex.goto('star',$scope.$parent.data.interface.lists.stars,newIdx);

                    } else {

                        $scope.moveIndex.goto('star',$scope.$parent.data.interface.lists.stars,0);

                    }


                },

                prev: function() {

                    var index = $scope.moveIndex.index('star',$scope.$parent.data.interface.lists.stars);
                    if (typeof index !== 'undefined') {

                        var newIdx = $scope.moveIndex.increment('p',index,$scope.$parent.data.interface.lists.stars);
                        $scope.moveIndex.goto('star',$scope.$parent.data.interface.lists.stars,newIdx);

                    } else {

                        $scope.moveIndex.goto('star',$scope.$parent.data.interface.lists.stars,0);

                    }

                }
            
            },

            node: {
                
                next: function() {

                    var index = $scope.moveIndex.index('node',$scope.$parent.data.interface.lists.nodes);
                    var newIdx = index;
                    if (typeof index !== 'undefined') {

                        newIdx = $scope.moveIndex.increment('n',index,$scope.$parent.data.interface.lists.nodes);
                        $scope.moveIndex.goto('node',$scope.$parent.data.interface.lists.nodes,newIdx);
            
                    } else {

                        $scope.moveIndex.goto('node',$scope.$parent.data.interface.lists.nodes,0);

                    }

                },
                prev: function() {

                    var index = $scope.moveIndex.index('node',$scope.$parent.data.interface.lists.nodes);
                    var newIdx = index;
                    if (typeof index !== 'undefined') {
                        
                        newIdx = $scope.moveIndex.increment('p',index,$scope.$parent.data.interface.lists.nodes);
                        $scope.moveIndex.goto('node',$scope.$parent.data.interface.lists.nodes,newIdx);

                    } else {

                        $scope.moveIndex.goto('nodes',$scope.$parent.data.interface.lists.nodes,0);

                    }

                }

            }

        };

        $scope.trackVisible = function() {

            var tracks = $scope.data.scene.getObjectByName("Tracks");
            if (typeof tracks !== 'undefined') {

                tracks.visible = !tracks.visible;

            }

        }


    }]);
    
});