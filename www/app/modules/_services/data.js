app.factory('dataService', ['$rootScope', function ($rootScope) {

    var service = {

        data:false,

        SaveState: function () {
            sessionStorage.dataService = angular.toJson(service.data);
        },

        RestoreState: function () {
            service.model = angular.fromJson(sessionStorage.dataService);
        }
    }

    $rootScope.$on("savestate", service.SaveState);
    $rootScope.$on("restorestate", service.RestoreState);

    return service;
    
}]);