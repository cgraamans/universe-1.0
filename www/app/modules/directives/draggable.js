/*
https://gist.github.com/siongui/4969457
*/

angular.module('draggableModule', []).
  directive('draggable', ['$document' , function($document) {
    return {
      restrict: 'A',
      link: function(scope, elm, attrs) {
        var startX, 
          startY, 
          initialMouseX, 
          initialMouseY,
          el = elm.parent();

        el.css({position: 'absolute'});
 
        el.bind('mousedown', function($event) {
          startX = el.prop('offsetLeft');
          startY = el.prop('offsetTop');
          initialMouseX = $event.clientX;
          initialMouseY = $event.clientY;
          $document.bind('mousemove', mousemove);
          $document.bind('mouseup', mouseup);
          return false;
        });
 
        function mousemove($event) {
          var dx = $event.clientX - initialMouseX;
          var dy = $event.clientY - initialMouseY;
          el.css({
            top:  startY + dy + 'px',
            left: startX + dx + 'px'
          });
          return false;
        }
 
        function mouseup() {
          $document.unbind('mousemove', mousemove);
          $document.unbind('mouseup', mouseup);
        }
      }
    };
  }]);