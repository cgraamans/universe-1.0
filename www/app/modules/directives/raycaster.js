define(['app'], function (app,opt) {
  'use strict';

	app.directive('raycaster', ['$location','$rootScope',function ($location,$rootScope) {

    return {
      restrict: "EA",
      scope: true,
      link: function (scope, element, attrs) {

      	window.addEventListener( 'click', target, false );

      	function target() {
      		
      		if (scope.data.raycasting.targets.length > 0) {

      			scope.data.raycasting.targets.forEach(function(v){

      				if(v.object.name.endsWith('_CORE_OBJ')){

      					// v.object.parent.add(scope.data.camera);

      					var coreObj = v.object.name.substr(0,v.object.name.length-9);
      					if (coreObj) {

	      					var star = scope.data.tree.stars.find(x => x.stub == coreObj);
	      					if (star) {
								
								scope.$apply(function(){

	      							scope.data.interface.focus.star = coreObj;

      							});

	      					}

	      					var node = scope.data.tree.nodes.find(x => x.stub == coreObj);
		  					if (node) {

								scope.$apply(function(){

									scope.data.interface.focus.node = coreObj;

								});

	      					}

	      					var subnode = scope.data.tree.subnodes.find(x => x.stub == coreObj);
		  					if (subnode) {

								scope.$apply(function(){

									scope.data.interface.focus.node = coreObj;

								});

	      					}

      					}

      				}

      			});

				// console.log('$location');
				// var newLoc = '/v/'+scope.data.tree.cluster.map.cluster_stub+'/'+st;
				// console.log(newLoc);

				// $location.path(newLoc).replace();
				// window.history.pushState({},'',newLoc);


	      		}

	      	}

	    }

	}

	}]);

});