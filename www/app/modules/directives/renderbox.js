define(['app','opt','orbitControls','factoryStarMaker','factoryRenderer','threejs','token','factoryNodeMaker'], function (app,opt,THREEfunctions) {
  'use strict';

  app.directive('renderbox', ['socketer','factoryStarMaker','factoryRenderer','$q','factoryToken','factoryNodeMaker',function(socketer,fBuilder,fRenderer,$q,$token,fNodeMaker) {

    return {
      restrict: "EA",
      scope: true,
      transclude:true,
      link: function (scope, element, attrs) {
        
        var data = {
            tree:{
                cluster:false,
                stars:[],
                nodes:[],
                subnodes:[],
                colonies:[],
            },
            interface:{
                showLog:false,    
                log:[],
                scale:{
                    stars:{
                        size:opt.scale.stars.size,
                        distance:opt.scale.stars.distance
                    },
                    nodes:{
                        size:opt.scale.nodes.size,
                        distance:opt.scale.nodes.distance
                    }

                },
                last:{
                    focus:false
                },
                lists:{
                    stars:[],
                    nodes:[],
                    colonies:[]
                },
                focus: {
                    star:false,
                    cluster:false,
                    node:false,
                    nodeObj:false,
                    starObj:false,
                    colony:false,
                }                
            },

        };

        function init() {
          
          window.addEventListener('resize', onWindowResize, false);
          window.addEventListener('mousemove', onMouseMove, false);

          try {

              data.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true }),
              data.loadTexture = new THREE.TextureLoader(),
              data.raycasting = {
                mouse: new THREE.Vector2(),
                raycaster: new THREE.Raycaster(),
                targets:[]
              };
              data.raycasting.mouse.x = ( window.innerWidth ) * 2 - 1;
              data.raycasting.mouse.y = - ( window.innerHeight ) * 2 + 1;
              data.scene = new THREE.Scene();

              data.loadTexture.crossOrigin = '';

              data.renderer.setSize(element[0].clientWidth, element[0].clientHeight);
              data.renderer.shadowMapEnabled = true;
              // scope.data.renderer.shadowMapType = THREE.PCFSoftShadowMap;
              
              var elPut = angular.element(data.renderer.domElement);
              element.append(elPut);

              data.camera = new THREE.PerspectiveCamera(50, element[0].clientWidth / element[0].clientHeight, 1, 200000000000000000);
              data.camera.position.set(1000,-33,0);

              data.scene.add(data.camera);
              data.controls = new THREE.OrbitControls(data.camera,data.renderer.domElement);
              
              data.timestamp = Date.now();

              scope.$parent.data = data;
              
            } catch(err) {
            
              console.log(err);

            }

        }
            
        function onMouseMove(event) {

          scope.$parent.data.raycasting.mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
          scope.$parent.data.raycasting.mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;   

        }

        function onWindowResize(event) {

          scope.$parent.data.camera.aspect = element[0].clientWidth / element[0].clientHeight;
          scope.$parent.data.camera.updateProjectionMatrix();
          scope.$parent.data.renderer.setSize(element[0].clientWidth, element[0].clientHeight);

        }

        init();

      
      }

    };

  }]);

  return app;

}); 