define(['app'], function (app) {

  'use strict';
	app.factory('AuthenticationService', ['socketer','$cookieStore', '$rootScope','$q',function (socketer,$cookieStore,$rootScope,$q) {

    	var service = {

    		login:function(user,pass,persistent) {

                return $q(function(resolve,reject) {

                	try {

                		var send = {
							"name":user,
							"password:":pass,
                		};
                		(typeof persistent !== 'undefined') ? send.persistent = persistent : send.persistent = false;   
                		console.log("SENDING!");
                		console.log(send);
						socketer.emit('auth',send);

					    socketer.on('auth_data', function (data) {
					      
					      console.log('AUTH DATA RECIEVED!');
					      resolve(data);

						});

					} catch(e) {
						reject(e);
					}
		    	
		    	});

    		}

    	};

    	return service;

    }]);

    return app;

});
