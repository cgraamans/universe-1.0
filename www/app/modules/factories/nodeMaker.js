define(['app','opt','modules/threejs/functions','renderbox','threejs','factoryNodeMaker'], function (app,opt,THREEfunctions) {

  'use strict';

    app.factory('factoryNodeMaker', ['socketer','factoryToken','$q',function factoryScenery(socketer,$token,$q) {

        var create = {

            planetoid: function(type,texture,textureLoader,radius,stub) {
    
                var rtn = {ok:false};

                try {
                    // Set Scene
                    var textureMap = textureLoader.load(opt.service+opt.httpRoutes.planetoid+type+'-'+texture+'-small.jpg'),
                        planetObj;

                    planetObj = new THREE.Mesh(
                            new THREE.SphereGeometry(radius, 64, 64),  new THREE.MeshPhongMaterial(
                                {
                                    map: textureMap
                                }
                            )
                    );

                    planetObj.castShadow = true;
                    planetObj.receiveShadow = true;
                    planetObj.name = stub + '_CORE_OBJ';

                    rtn.res = new THREE.Object3D();
                    rtn.res.name =  stub + '_CORE';

                    rtn.res.add(planetObj);

                    rtn.ok = true;

                } catch(err) {

                    rtn.err = err;
                
                }
                return rtn;

            },

            atmospherics: function(texture,opt,count,startRadius) {

                // create destination canvas
                var canvasResult    = document.createElement('canvas');
                canvasResult.width  = 512;
                canvasResult.height = 256;
                var contextResult   = canvasResult.getContext('2d');

                // load earthcloudmap
                var imageMap    = new Image();
                imageMap.crossOrigin="anonymous";
                imageMap.addEventListener("load", function() {
                    
                    // create dataMap ImageData for earthcloudmap
                    var canvasMap   = document.createElement('canvas')
                    canvasMap.width = imageMap.width
                    canvasMap.height= imageMap.height
                    var contextMap  = canvasMap.getContext('2d')
                    contextMap.drawImage(imageMap, 0, 0)
                    var dataMap = contextMap.getImageData(0, 0, canvasMap.width, canvasMap.height)

                    // load earthcloudmaptrans
                    var imageTrans  = new Image();
                    imageTrans.crossOrigin="anonymous";
                    imageTrans.addEventListener("load", function(){
                        // create dataTrans ImageData for earthcloudmaptrans
                        var canvasTrans     = document.createElement('canvas')
                        canvasTrans.width   = imageTrans.width
                        canvasTrans.height  = imageTrans.height
                        var contextTrans    = canvasTrans.getContext('2d')
                        contextTrans.drawImage(imageTrans, 0, 0)
                        var dataTrans       = contextTrans.getImageData(0, 0, canvasTrans.width, canvasTrans.height)
                        // merge dataMap + dataTrans into dataResult
                        var dataResult      = contextMap.createImageData(canvasMap.width, canvasMap.height)
                        for(var y = 0, offset = 0; y < imageMap.height; y++){
                            for(var x = 0; x < imageMap.width; x++, offset += 4){
                                dataResult.data[offset+0]   = dataMap.data[offset+0]
                                dataResult.data[offset+1]   = dataMap.data[offset+1]
                                dataResult.data[offset+2]   = dataMap.data[offset+2]
                                dataResult.data[offset+3]   = 255 - dataTrans.data[offset+0]
                            }
                        }
                        // update texture with result
                        contextResult.putImageData(dataResult,0,0)  
                        material.map.needsUpdate = true;
                    })
                    imageTrans.src  = global.opt.loc.textures+'atmospheres/'+texture+'trans.jpg';
                }, false);

                imageMap.src = global.opt.loc.textures+'atmospheres/'+texture+'.jpg';

                var material  = new THREE.MeshPhongMaterial({

                    map: new THREE.Texture(canvasResult),
                    opacity: 0.5,
                    transparent: true,
                    depthWrite: false,
                    depthTest:false,
                    // blending: THREE.AdditiveBlending,

                });

                var rtn = new THREE.Object3D();
                rtn.add(new THREE.Mesh(new THREE.SphereGeometry((count + startRadius), 64, 64), material));

                return rtn;  

            },

            pathMap: function(xy,uv,pColor) {

                if (typeof pColor === 'undefined') {
                    pColor = '#337ab7';
                }                   
                var a, 
                    b;
                if (xy < uv) {
                
                    b = xy,
                    a = uv;
                
                } else {

                    a = xy,
                    b = uv;
                
                }

                var curve = new THREE.EllipseCurve(
                    0,  0,
                    a, b,
                    0,  2 * Math.PI,
                    false,
                    0
                );
                var path = new THREE.Path( curve.getPoints( 1000 ) );
                var geometry = path.createPointsGeometry( 1000 );

                        // var customMaterial = new THREE.ShaderMaterial({
                        //     uniforms: 
                        //     { 
                        //         "c":   { type: "f", value: 1.0 },
                        //         "p":   { type: "f", value: 1.4 },
                        //         glowColor: { type: "c", value: new THREE.Color(0xffff00) },
                        //         viewVector: { type: "v3", value: {x:0,y:0,z:0} }
                        //     },
                        //     vertexShader:   document.getElementById( 'shader-vertex-line'   ).textContent,
                        //     fragmentShader: document.getElementById( 'shader-fragment-line' ).textContent,
                        //     side: THREE.FrontSide,
                        //     blending: THREE.AdditiveBlending,
                        //     transparent: false,
                        //     color:pColor
                        // });

                var material = new THREE.LineBasicMaterial( { color:pColor, linewidth:30 });

                var ellipse = new THREE.Line( geometry, material );

                return ellipse;

            },

            floatingRocks: function(numRoids,pAu,pMaxPlane,pSize,pE,textureLoader,pColor){

                var rtn = {ok:false};

                try {
                
                    var roidObj = new THREE.Object3D();

                    if (typeof pColor === 'undefined') {

                        pColor = '#FFFFFF';

                    }

                    var particles = new THREE.Geometry();

                    var pMaterial = new THREE.PointsMaterial({
                      color: pColor,
                      size: pSize,
                      map: textureLoader.load(opt.service+opt.httpRoutes.planetoid+'asteroidsprite.png'),
                      blending: THREE.AdditiveBlending,
                      transparent: true,
                      depthTest: true,
                      depthWrite: false,
                    });

                    for(var p=0;p<numRoids;p++) {

                        var incl = Math.random()*360,
                            plane = Math.random() * pMaxPlane,
                            e = pE * (1+(Math.floor(Math.random() * 1010) + 990)/10000);


                        var pos = THREEfunctions.position(pAu,incl,plane,e);
                        var particle = new THREE.Vector3(pos.x, pos.y, pos.z);
                        
                        particles.vertices.push(particle);

                    }

                    var particleSystem = new THREE.Points(
                        particles,
                        pMaterial);

                    roidObj.add(particleSystem);
                    
                    rtn.res = roidObj;
                    rtn.ok = true;

                } catch (err) {
                    
                   rtn.err = err;

                }
                return rtn;

            },
            
            G: 6.67384e-11, // m3 kg-1 s-2

            getAcceleration: function (distance, starMass) {

                return this.G * starMass / (Math.pow(distance, 2));

            },

            colony: function(colonyData) {

                console.log('colony creator');

                console.log(colonyData);

                var colony = new THREE.Object3D();

                return colony;

            }

        };

    	var factory = {

            getNodes: function(star) {

                return $q(function(resolve,reject) {

                    var nodeGetData = {
                        star:star,
                        token:$token.getToken()
                    };
                    
                    socketer.emit("nodes",nodeGetData);
                    socketer.on("nodes_data", function (socketData) {

                        if(socketData.ok === true) {
                            
                            resolve(socketData.res);

                        } else {

                            reject(socketData.err);

                        }

                    });
                    socketer.on("nodes_error", function (socketData) {

                            reject(socketData.err);

                    });

                });

            },
    	
    		init: function(star,callback) {

                    var nodeData = this.getNodes(star);
                    nodeData.then(function(nodeList){

                        callback(nodeList);

                    });

                    nodeData.catch(function(err){
                        
                        callback();
                    
                    });

            },

            tracks: function(nodes,startingPos,starRadius,callback) {

                var rtn = {
                
                    ok:false,
                    res: new THREE.Object3D()
                
                };

                try {

                    rtn.res.name = 'Tracks';
                    rtn.res.rotateX(THREEfunctions.radian(90));

                    rtn.res.position.set(startingPos.x,startingPos.y,startingPos.z);

                    for(var i=0,c=nodes.length;i<c;i++) {

                        var unp = THREEfunctions.unpackEllipse(nodes[i]._au,nodes[i].orb_ellipse);
                        var pColor = "#CCCCCC";
                        if ((nodes[i].type == 'gas') || (nodes[i].type == 'rock'))  {
                            pColor = "#337ab7";
                        }
                        var pathMap = create.pathMap(unp.a,unp.b,pColor);

                        pathMap.position.set(pathMap.position.x + unp.c, pathMap.position.y, pathMap.position.z); // offset to c
                        pathMap.rotateX(-1 * THREEfunctions.radian(nodes[i].orb_incl)); // X rotation for inclination

                        rtn.res.add(pathMap);
                    }

                    rtn.ok = true;

                } catch(err) {

                    rtn.err = err;
                
                }

                callback(rtn);

            },

            encapsulate: function(star,nodes) {

                var nodeListObj = new THREE.Object3D();
                    nodeListObj.name = "Nodes",
                    nodeListObj.position.set(star.obj.position.x,star.obj.position.y,star.obj.position.z),

                nodes.forEach(function(node){

                    if (node.type == 'rock' || node.type == 'moon' || node.type == 'gas') {

                        var iPos = THREEfunctions.position(node._au,node.orb_angle,node.orb_incl,node.orb_ellipse);
                        if (iPos) {

                            node.obj.position.set(iPos.x,iPos.y,iPos.z);

                        }
                        
                    }
                    nodeListObj.add(node.obj);

                });

                return nodeListObj;

            },

            positionNode: function(node) {

                if (node.type =='rock' || node.type =='moon' || node.type =='gas'){
                    var iPos = THREEfunctions.position(node._au,node.orb_angle,node.orb_incl,node.orb_ellipse);
                    if (iPos) {
    
                        node.obj.position.set(iPos.x,iPos.y,iPos.z);
    
                    }
                }
                return node;

            },

            create: function(nodes,textureLoader,parentRadius,parentMass,callback,isSubnode) {

                var that = this;
                for(var i=0,c=nodes.length;i<c;i++) {

                    nodes[i].obj = new THREE.Object3D();
                    nodes[i].obj.name = nodes[i].stub;

                    nodes[i].obj.castShadow = true;
                    nodes[i].obj.receiveShadow = true;
                    
                    var coreNode = {ok:false},
                        shellType,
                        particleSize = 1,
                        particleColor = "#C7C7C7";


                    // add the au distance of the object multiplied by node distance in options to the star's radius
                    if (typeof isSubnode !== 'undefined') {
                        nodes[i]._au = (nodes[i].au * opt.scale.subnodes.distance) + parentRadius;
                    } else {
                        nodes[i]._au = (nodes[i].au * opt.scale.nodes.distance) + parentRadius;
                    }

                    // surfaces
                    switch(nodes[i].type) {
                        case 'rock':
                        case 'moon':
                            shellType = nodes[i].type_surface;
                            break;
                        case 'gas':
                            shellType = nodes[i].type_atmosphere;
                            break;
                    }

                    // Particle sizes
                    switch(nodes[i].type) {

                        case 'asteroids':
                            if (typeof isSubnode !== 'undefined') {
                                particleSize = opt.scale.subnodes.asteroids;

                            } else {
                                particleSize = opt.scale.nodes.asteroids;
                            }
                            break;
                        case 'debris':

                            particleColor = "#414141"; 
                            if (typeof isSubnode !== 'undefined') {
                                particleSize = opt.scale.subnodes.debris;

                            } else {
                                particleSize = opt.scale.nodes.debris;
                            }
                            break;
                        case 'rock':
                        case 'gas':
                            nodes[i]._r0 = nodes[i].type_size_r0 * opt.scale.nodes.size;
                            break;
                        case 'moon':
                            nodes[i]._r0 = nodes[i].type_size_r0 * opt.scale.subnodes.size;
                            break;
                    }

                    // create initial object(s)
                    switch(nodes[i].type) {
                        
                        case 'rock':
                        case 'gas':
                            coreNode = create.planetoid(nodes[i].type,shellType,textureLoader,nodes[i]._r0,nodes[i].stub);                            
                            break;
                        
                        case 'moon':
                            coreNode = create.planetoid('rock',shellType,textureLoader,nodes[i]._r0,nodes[i].stub);
                            break;

                        case 'asteroids':
                        case 'debris':
                            coreNode = create.floatingRocks(nodes[i].opt_asteroids,nodes[i]._au,nodes[i].opt_angle,particleSize,nodes[i].orb_ellipse,textureLoader,particleColor);
                            break;
                    }

                    if (coreNode.ok === true) {

                        // set speed of node for scope.data.renderer
                        nodes[i].velocity = create.getAcceleration(nodes[i]._au,parentMass);

                        // add coreNode data to object reference
                        nodes[i].obj.add(coreNode.res);

                    }

                }
                callback(nodes);

            },

            colonize: function(colonies,textureLoader,parentType,parentRadius,parentStub,callback) {

                var that = this;
                for(var i=0,c=colonies.length;i<c;i++) {

                    // var obj = create.colony(textureLoader,parentRadius/10);
                    var obj = create.planetoid('rock','ice',textureLoader,parentRadius/10,colonies[i].colony_stub);


                    if (obj.ok === true) {

                        colonies[i].obj = obj.res;
                        colonies[i].obj.name = colonies[i].colony_stub;

                        colonies[i].parent = parentStub,
                        colonies[i].isAsteroid = false;
                        
                        var position = THREEfunctions.spherePosition(parentRadius,colonies[i].loc_angle,colonies[i].loc_plane);
                        if (typeof position.err === 'undefined') {
                            
                            colonies[i].obj.position.set(position.x,position.y,position.z);
                        
                        }
                    
                    }

                    if (parentType === 'asteroids') {
                    
                        colonies[i].isAsteroid = true;
                    
                    }

                }
                callback(colonies);

            }
    		
	    };

		return factory;

    }]);

    return app;

});