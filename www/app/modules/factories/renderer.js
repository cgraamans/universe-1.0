define(['app','opt','modules/threejs/functions','threejs','renderbox'], function (app,opt,THREEf) {

  'use strict';

    app.factory('factoryRenderer',[function factoryRenderer(socketer,$token) {

    	var factory = {

	    	init: function(scene) {

		    	try {

					var object = scene.getObjectByName("skyBox",true);
						object.rotation.y += 0.00005;

					return true;

				} catch(err){

					return err;

				}

	    	},

	    	shaderTimer: function(scopeData) {

	    		try {
	    			
	    			scopeData.tree.stars.forEach(function(v){
	    			
						var shaderTiming = (Date.now() - scopeData.timer) / 1000;
	    				v.properties.sunUniforms.time.value = v.properties.haloUniforms.time.value = v.properties.solarflareUniforms.time.value = shaderTiming;
	    			
	    			});
					return true;

    			} catch(err) {

    				return err;
    			
    			}

	    	},

	    	nodeOrbits: function(scopeData) {

	    		try {

                    var starFound = scopeData.tree.stars.find(x => x.stub == scopeData.interface.focus.star);

	    			if (typeof starFound !== 'undefined') {
	    				var orbitalMass = starFound.aspects.mass;

		    			for(var i=0,c=scopeData.tree.nodes.length;i<c;i++) {

		    				var object = scopeData.scene.getObjectByName(scopeData.tree.nodes[i].stub,true);
		    				if (typeof object !== 'undefined') {

			    				if ((scopeData.tree.nodes[i].type == 'rock') || (scopeData.tree.nodes[i].type == 'gas')) {

			    					var reo = object.getObjectByName(scopeData.tree.nodes[i].stub+"_CORE"),
			    						baseVelocityPow = opt.scale.nodes.velocity;
			    					
			    					reo.rotation.y += scopeData.tree.nodes[i].rotation * opt.scale.nodes.rotation;

			    					if (typeof scopeData.tree.nodes[i].parent_id !== 'undefined') {

										var nodeFound = scopeData.tree.nodes.find(x => x.stub == scopeData.tree.nodes[i].parent_id);
										
										orbitalMass = nodeFound.type_mass,
										baseVelocityPow = opt.scale.subnodes.velocity;

			    					}

			    					var nowPos = THREEf.position(scopeData.tree.nodes[i]._au,scopeData.tree.nodes[i].orb_angle,scopeData.tree.nodes[i].orb_incl,scopeData.tree.nodes[i].orb_ellipse);
			    					var velocity = THREEf.accelerateObj(opt.scale.G,nowPos.d,orbitalMass) * baseVelocityPow;

			    					// console.log('------');
			    					// console.log(scopeData.tree.nodes[i].stub);
			    					// console.log(nowPos.d);
			    					// console.log(velocity);

									scopeData.tree.nodes[i].orb_angle += velocity;

									if (scopeData.tree.nodes[i].orb_angle >= 360) {
										
										scopeData.tree.nodes[i].orb_angle - 360;
									
									}

									var newPos = THREEf.position(scopeData.tree.nodes[i]._au,scopeData.tree.nodes[i].orb_angle,scopeData.tree.nodes[i].orb_incl,scopeData.tree.nodes[i].orb_ellipse);
									object.position.set(newPos.x,newPos.y,newPos.z);

			    				}

		    				}

	    				}

    				}

	    			return true;

	    		} catch(err) {

	    			console.log(err);

	    			return err;

	    		}

	    	},

	    	starOrbits: function(scopeData) {
	    		
	    		try {
	    			
	    			scopeData.tree.stars.forEach(function(v){
    					
    					var core = v.obj.getObjectByName(v.stub+"_CORE_OBJ");
    					if (typeof core !== 'undefined') {
    						core.rotation.y += 0.005;	
    					}

    					var halo = scopeData.scene.getObjectByName(v.stub+"_HALO");
    					if(typeof halo !== 'undefined') {
    						halo.lookAt(scopeData.camera.position);
    					}

    					var glow = scopeData.scene.getObjectByName(v.stub+"_GLOW");
    					if (typeof glow !== 'undefined') {
    						glow.lookAt(scopeData.camera.position);	
    					}
    					

	    			});

	    			return true;

    			} catch(err) {
    				console.log('GYRO ERR');
    				console.log(err);
    				return err;

    			}


	    	}
	    
	    };
		return factory;

    }]);

    return app;

});