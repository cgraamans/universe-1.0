define(['app','opt','modules/threejs/functions','threejs','renderbox','token'], function (app,opt,THREEfunction) {

  'use strict';

    app.factory('factoryStarMaker', ['socketer','factoryToken','$q',function factoryScenery(socketer,$token,$q) {

    	var func = {

    		textureLocation: opt.service+opt.httpRoutes.stars,
    		textureLocationFlares: opt.service+opt.httpRoutes.flares,

    		starList: [],

    		flattenMap: function(obj,callback,counter,startPos) {

    			var that = this;
		    	if (typeof counter === 'undefined') {

		    		counter = 0;

		    	}

		    	if (typeof startPos === 'undefined') {

		    		startPos = {x:0,y:0,z:0};

					that.starList.push({

						stub:obj.stub,
						position:startPos,
						aspects:obj.aspects
					
					});
					
		    	}

    			try {

    				counter++;
    				if ((obj.map.length > 0) && (counter < 10)) {

    					var prom = [];
	    				obj.map.forEach(function(v) {

	    					var treePoint = {

	    						stub:v.stub,
	    						position:THREEfunction.realPosition(v,startPos),
	    						aspects:v.aspects
	    					
	    					};

	    					that.starList.push(treePoint);
	    					prom.push(that.flattenMap(v,function(){

    						},counter,treePoint.position));    					

	    				});
				        
				        $q.all(prom).then(function () {
            				callback();
        				});

    				} else {

    					callback();

    				}


    			}
    			catch(err) {

    				callback(err);

    			}

    		},

			lensflare : {

				make: function(x,y,z,textures,star_color) {

					var flareColor = new THREE.Color(star_color);
					var overallColor = new THREE.Color('#ffffff');

					var lensFlare = new THREE.LensFlare( textures[0], 50, 0.0, THREE.AdditiveBlending, flareColor );

					lensFlare.add( textures[1], 64, 0.0, THREE.AdditiveBlending,flareColor);
					lensFlare.add( textures[1], 64, 0.0, THREE.AdditiveBlending,flareColor);
					lensFlare.add( textures[1], 64, 0.0, THREE.AdditiveBlending,flareColor);

					lensFlare.position.set(0,0,0);
					lensFlare.scale.x = lensFlare.scale.y = lensFlare.scale.z = lensFlare.scale.x * 0.5
					lensFlare.customUpdateCallback = this.update;

					return lensFlare;

				},

				update : function(object) {

					var f, fl = object.lensFlares.length;
					var flare;
					var vecX = -object.positionScreen.x * 2;
					var vecY = -object.positionScreen.y * 2;

					for( f = 0; f < fl; f++ ) {

						   flare = object.lensFlares[ f ];

						   flare.x = object.positionScreen.x + vecX * flare.distance;
						   flare.y = object.positionScreen.y + vecY * flare.distance;

						   flare.rotation = 0;

					}

					object.lensFlares[ 2 ].y += 0.025;
					object.lensFlares[ 3 ].rotation = object.positionScreen.x * 0.5 + THREE.Math.degToRad( 45 );

				}

			},

    	};

    	var factory = {

    		request: {},

    		light: function(color) {

    			var lightObj = new THREE.Object3D(),
    				mainLight = new THREE.PointLight('#'+color,1,0,0);
    				// spotLight = new THREE.SpotLight( 0xffffff,0.6,0);
    				// spotLight.shadow.camera.fov = 30;
    			// spotLight.shadowCameraVisible = true;
				// spotLight.shadow.camera.far = 100000000000000;
				// spotLight.angle = Math.PI/2;
				// spotLight.castShadow = true;
				// spotLight.name = "SPOTLIGHT";

				lightObj.add(mainLight);
				// lightObj.add(spotLight);

				return lightObj;

    		},
    		
		    skyBox: function(loadTexture,image) {

			 	var container = new THREE.Object3D();
				var geometry = new THREE.SphereGeometry(24000000000+(Math.round(Math.random()*100)), 60, 40),
				uniforms = {

				      texture: {

				      	type: 't', 
				      	value: loadTexture.load(opt.service+opt.httpRoutes.skybox+image)
				      
				      }

			    };

			    var material = new THREE.ShaderMaterial({

			      uniforms:       uniforms,
			      vertexShader:   document.getElementById('sky-vertex').textContent,
			      fragmentShader: document.getElementById('sky-fragment').textContent
				    
			    });

			    var skyBox = new THREE.Mesh(geometry, material);
			    skyBox.scale.set(-1, 1, 1);
			    skyBox.rotation.order = 'XZY';
			    skyBox.renderDepth = 1000.0;

			    container.name='skyBox';
			    container.add(skyBox);

			    return container;

		    },

    		starGet: function(stub) {

	          	return $q(function(resolve,reject) {

	    			var starGetData = {
	    				star:stub,
	    				token:$token.getToken()
	    			};

	    			socketer.emit("stars",starGetData);
				    socketer.on("stars_data", function (socketData) {
				    	
				    	if(socketData.ok === true) {

				    		resolve(socketData.res);

				    	} else {

				    		reject(socketData.err);

				    	}

			    	});
				    socketer.on("stars_error", function (socketData) {
				    	console.log('ERR');
				    	console.log(socketData);
			    		reject(socketData.err);

				    });

			    });

    		},

			starMap: function(star,textureLoader,callback) {

				var that = this,
					rtn = {ok:false};

				func.starList = [];
				func.flattenMap(star,function(v){

					if (v) {
						
						rtn.err = v;
						
					} else {

						rtn.ok = true,
						rtn.res = func.starList;

					}
					callback(rtn)

				});

		    },

    		starMake: function(star,textureLoader) {

    			try {

	    			var starObj = [],
	    				that = this;

					var starSphereTexture = func.textureLocation+star.aspects.colorhex+'.jpg';					
					var sunTexture = textureLoader.load(starSphereTexture);
						sunTexture.anisotropy = 1;
						sunTexture.wrapS = sunTexture.wrapT = THREE.RepeatWrapping;
					var sunColorLookupTexture = textureLoader.load(func.textureLocation+"star_colorshift.png");
					var starColorGraph = textureLoader.load(func.textureLocation+'star_color_modified.png');
					
					var sunHaloTexture = textureLoader.load(func.textureLocation+'sun_halo.png');
					var sunHaloColorTexture = textureLoader.load(func.textureLocation+'halo_colorshift.png');

					var solarflareTexture = textureLoader.load(func.textureLocation+'solarflare.png');
					var sunCoronaTexture = textureLoader.load(func.textureLocation+'corona.png');

				} catch(err) {

					console.error(err);

				}

				try {

	    			star.properties = {

						size: star.aspects.r0 * opt.scale.stars.size,
						sunUniforms :{
							texturePrimary:   { type: "t", value: sunTexture },
							textureColor:   { type: "t", value: sunColorLookupTexture },
							textureSpectral: { type: "t", value: starColorGraph },
							time: 			{ type: "f", value: 0 },
							spectralLookup: { type: "f", value: star.aspects.spec_trans },
						},
						solarflareUniforms:{
							texturePrimary:   { type: "t", value: solarflareTexture },
							time: 			{ type: "f", value: 0 },
							textureSpectral: { type: "t", value: starColorGraph },
							spectralLookup: { type: "f", value: star.aspects.spec_trans },						
						},
						haloUniforms:{
							texturePrimary:   { type: "t", value: sunHaloTexture },
							textureColor:   { type: "t", value: sunHaloColorTexture },
							time: 			{ type: "f", value: 0 },
							textureSpectral: { type: "t", value: starColorGraph },
							spectralLookup: { type: "f", value: star.aspects.spec_trans },
						},
						coronaUniforms:{
							texturePrimary:   { type: "t", value: sunCoronaTexture },
							textureSpectral: { type: "t", value: starColorGraph },
							spectralLookup: { type: "f", value: star.aspects.spec_trans },						
						},

	    			};
					var starbox = new THREE.Object3D();

					// CORE
					var sunShaderMaterial = new THREE.ShaderMaterial( {
						uniforms: star.properties.sunUniforms,
						vertexShader: document.getElementById('shader-vertex-starsurface').textContent,
						fragmentShader: document.getElementById('shader-fragment-starsurface').textContent,
					});

					var sunSphere = new THREE.Mesh( new THREE.SphereGeometry(star.properties.size, 64, 64), sunShaderMaterial);
					sunSphere.name = star.stub+'_CORE_OBJ';
					starbox.add(sunSphere);

					// HALO
					var planeSize = star.properties.size*Math.PI;
					var planeGeo = new THREE.PlaneGeometry( planeSize, planeSize );					
					var sunHaloMaterial = new THREE.ShaderMaterial({
							uniforms: star.properties.haloUniforms,
							vertexShader:   document.getElementById('shader-vertex-default').textContent,
							fragmentShader: document.getElementById('shader-fragment-starhalo').textContent,
							blending: 		THREE.AdditiveBlending,
							depthTest: 		true,
							depthWrite: 	false,
							transparent: 	true,
					});

					var sunHalo = new THREE.Mesh(planeGeo,sunHaloMaterial);
					sunHalo.position.set( 0, 0, 0 );
					sunHalo.name = star.stub+'_HALO';
					starbox.add(sunHalo);

					// Star Glow
					var starGlowMat = new THREE.ShaderMaterial(
						{
							map: sunCoronaTexture,
							uniforms: star.properties.coronaUniforms,
							blending: THREE.AdditiveBlending,
							fragmentShader: document.getElementById('shader-fragment-corona').textContent,
							vertexShader:document.getElementById('shader-vertex-default').textContent,
							transparent: true,
							depthTest: true,
							depthWrite: false,
						}
					);

					var sunGlow = new THREE.Mesh( planeGeo, starGlowMat );
					sunGlow.position.set( 0, 0, 0 );
					sunGlow.scale.x = sunGlow.scale.y = sunGlow.scale.z = sunGlow.scale.x * 10;
					sunGlow.name = star.stub+'_GLOW';
					starbox.add(sunGlow);

				    // LENSFLARE
				    var flareTextures = [];
				    flareTextures.push(textureLoader.load(func.textureLocationFlares+'lensflare0.png'));
				    flareTextures.push(textureLoader.load(func.textureLocationFlares+'lensflare1.png'));
				    flareTextures.push(textureLoader.load(func.textureLocationFlares+'lensflare2.png'));
				    var flares = func.lensflare.make(0,0,0,flareTextures,'#'+star.aspects.colorhex);

				    starbox.add(flares);

					// PREP OBJ
					starbox.name = star.stub;


    			} catch(err) {

    				console.error(err);

    			}

				return starbox;			

    		},

	    };

		return factory;

    }]);

    return app;

});