define(['app','ngCookies'], function (app,opt) {

  'use strict';

    app.factory('factoryToken', ['$cookies',function factoryScenery($cookies) {

    	var factory = {
    		getToken:function(){
    			return $cookies.get('token') ? $cookies.get('token') : false;			
    		},
    		showLog:function() {
    			return $cookies.get('showLog') ? true : false;
    		}
    	};
    	return factory;

	}])

    return app;

});;