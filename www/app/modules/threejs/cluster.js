define(['./functions','threejs'],function(_f) {

	return {

		create:function(obj,callback){

			var that = this;

			// run through map
			// var object = scene.getObjectByName( "objectName", true );
			
			rtn = [];
			var star = new THREE.Object3D();
			star.name=obj.stub;

		    var geometry = new THREE.SphereGeometry( 5, 32, 32 );
		    var material = new THREE.MeshBasicMaterial( {color: 0xffff00} );
		    var starObj = new THREE.Mesh( geometry, material );

		    star.add(starObj);
		    rtn.push(star);
		    if (obj.build.map.childNodes.length > 0) {

			    this.nodery(obj.build.map.childNodes,function(nodes){
			    	
			    	if (nodes) {
			    		
			    		nodes.forEach(function(node){
			    			rtn.push(node);
			    		});
			    	
			    	}
					callback(rtn);

			    });


		    }


		},

		nodery: function(obj,opts,count) {

			var that = this,
				options = {
					maxCount:10,
					coords:{
						x:0,
						y:0,
						z:0
					},
					distanceMultiplyer:10,
				},
				rtn = [];

			// Options
			if (typeof count === 'undefined') {

				count = 0;

			}

			if (typeof opts === 'object') {

				if (typeof opts.maxCount !== 'undefined') {

					options.maxCount = opts.maxCount;
				
				}

				if (typeof opts.distanceMultiplyer !== 'undefined') {

					options.distanceMultiplyer = opts.distanceMultiplyer;
				
				}				

				if (typeof opts.coords === 'object') {

					if (typeof opts.coords.x !== 'undefined') {

						options.coords.x = opts.coords.x;
					
					}

					if (typeof opts.coords.y !== 'undefined') {

						options.coords.y = opts.coords.y;
					
					}

					if (typeof opts.coords.z !== 'undefined') {

						options.coords.z = opts.coords.z;
					
					}

				}

			}

		    if (obj.length > 0) {

		    	if (count < options.maxCount) {
    				
    				var childNodes = [];
		    		count++;

			    	for(i=0,c=obj.length;i<c;i++) {

			    		var star = new THREE.Object3D(),
			    			geometry = new THREE.SphereGeometry( 5, 32, 32 ),
					    	material = new THREE.MeshBasicMaterial( {color: 0xffff00} ),
					    	positions = _f.position(obj[i].distance,obj[i].incl,obj[i].plane);
					    
					    var starObj = new THREE.Mesh( geometry, material );
					    star.name = obj[i].stub;
					    star.add(starObj);

						star.position.x = (positions.x + options.coords.x)*options.distanceMultiplyer,
						star.position.y = (positions.y + options.coords.y)*options.distanceMultiplyer,
						star.position.z = (positions.z + options.coords.z)*options.distanceMultiplyer;

					    rtn.push(star);

			    		if (obj[i].childNodes.length > 0) {

			    			obj[i].childNodes.forEach(function(cn){
			    				cn.position = {

			    					x:star.position.x,
			    					y:star.position.y,
			    					z:star.position.z
			    				
			    				};
			    				childNodes.push(cn);
			    			});

			    		}

			    	}
			    	console.log('childNodes');
			    	console.log(childNodes);
			    	console.log(count);
			    
					childNodes.forEach( function(v) {
						console.log(v);
						var nProm = new Promise(

    						function(res,rej){

				    			var passOpts = {

				    				maxCount:options.maxCount,
				    				coords:{
				    					x:v.position.x,
				    					y:v.position.y,
				    					z:v.position.z
				    				},
				    				distanceMultiplyer:options.distanceMultiplyer
		    					
		    					};

				    			that.nodery(v,function(returnNodes){

				    				res(returnNodes);

				    			},passOpts,count);

    						}

						);

    					nProm.then(

    						function(rNodes){

    							if (rNodes) {

	    							rNodes.forEach(function(t){
	    							
	    								rtn.push(t);
	    							
	    							});

    							}		    						
    						}

						);


					});
					callback(rtn);


		    	} else {

		    		callback();

		    	}	    	

		    } else {

		    	callback();

		    }

		},

		star: function(star,opt) {

			var that = this;

			var starbox = new THREE.Object3D();

			var starSize = star.r0 * opt.defaults.size.star;
			star.size_scaled = starSize;
			var starSphereTexture = global.opt.loc.textures+'stars/'+star.color+'.jpg';

			// Textures for main star
			var sunTexture = THREE.ImageUtils.loadTexture(starSphereTexture);
				sunTexture.anisotropy = 1;
				sunTexture.wrapS = sunTexture.wrapT = THREE.RepeatWrapping;
			var sunColorLookupTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+"star_colorshift.png");
			var starColorGraph = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'star_color_modified.png');
			
			// Halo vars
			var sunHaloTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'sun_halo.png');
			var sunHaloColorTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'halo_colorshift.png');

			var solarflareTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'solarflare.png');
			var sunCoronaTexture = THREE.ImageUtils.loadTexture(global.opt.loc.textures+'stars/'+'corona.png');

			var sunUniforms = {
				texturePrimary:   { type: "t", value: sunTexture },
				textureColor:   { type: "t", value: sunColorLookupTexture },
				textureSpectral: { type: "t", value: starColorGraph },
				time: 			{ type: "f", value: 0 },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var solarflareUniforms = {
				texturePrimary:   { type: "t", value: solarflareTexture },
				time: 			{ type: "f", value: 0 },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var haloUniforms = {
				texturePrimary:   { type: "t", value: sunHaloTexture },
				textureColor:   { type: "t", value: sunHaloColorTexture },
				time: 			{ type: "f", value: 0 },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			var coronaUniforms = {
				texturePrimary:   { type: "t", value: sunCoronaTexture },
				textureSpectral: { type: "t", value: starColorGraph },
				spectralLookup: { type: "f", value: star.spec_trans },
			};

			// Sphere
			//
			var sunShaderMaterial = new THREE.ShaderMaterial( {
				uniforms: sunUniforms,
				vertexShader: document.getElementById('shader-vertex-starsurface').textContent,
				fragmentShader: document.getElementById('shader-fragment-starsurface').textContent,
			});

			var sunSphere = new THREE.Mesh( new THREE.SphereGeometry(starSize, 64, 64), sunShaderMaterial);
			sunSphere.name = 'star_CORE';

			starbox.add(sunSphere);

			var planeSize = starSize*Math.PI;
			var planeGeo = new THREE.PlaneGeometry( planeSize, planeSize );

			// sunHalo
			//
			var sunHaloMaterial = new THREE.ShaderMaterial(
				{
					uniforms:haloUniforms,
					vertexShader:   document.getElementById('shader-vertex-default').textContent,
					fragmentShader: document.getElementById('shader-fragment-starhalo').textContent,
					blending: THREE.AdditiveBlending,
					depthTest: 		true,
					depthWrite: 	false,
					transparent: true,
				}
			);

			var sunHalo = new THREE.Mesh(planeGeo,sunHaloMaterial);
			sunHalo.position.set( 0, 0, 0 );
			sunHalo.name = 'star_HALO';

			sunHalo.lookAt(camera.position);
			global.three.render.gyro.push(sunHalo.name);

			starbox.add(sunHalo);

			// Corona
			//
			var starGlowMat = new THREE.ShaderMaterial(
				{
					map: sunCoronaTexture,
					uniforms: coronaUniforms,
					blending: THREE.AdditiveBlending,
					fragmentShader: document.getElementById('shader-fragment-corona').textContent,
					vertexShader:document.getElementById('shader-vertex-default').textContent,
					transparent: true,
					depthTest: true,
					depthWrite: false,
				}
			);

			var sunGlow = new THREE.Mesh( planeGeo, starGlowMat );
			sunGlow.position.set( 0, 0, 0 );
			sunGlow.scale.x = sunGlow.scale.y = sunGlow.scale.z = sunGlow.scale.x *5;
			sunGlow.name = 'star_GLOW';

			global.three.render.gyro.push(sunGlow.name);
			sunGlow.lookAt(camera.position);

			starbox.add(sunGlow);

			starbox.coronaUniforms = coronaUniforms;
			starbox.sunUniforms = sunUniforms;
			starbox.haloUniforms = haloUniforms;
			starbox.solarflareUniforms = solarflareUniforms;

		    // // lensflare
		    // flareTextures = [];
		    // flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare0.png'));
		    // flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare1.png'));
		    // flareTextures.push(THREE.ImageUtils.loadTexture(global.opt.loc.textures+'lensflares/lensflare2.png'));

		    // var flares = that.func.create.lensflare.make(0,0,0,flareTextures,'#'+star.color);

			starbox.name = 'star';

			star.nodeObj = starbox;

	


		},

	}

});