define({

	radian: function(degrees){
	
		return degrees * (Math.PI / 180);
	
	},

	unpackEllipse: function(r,e) {

		var rtn = {a:r,b:r}, // Semi-minor axis
			sAxis = r*e; // Semi-minor or Semi-major axis

		// determine axis'
		if (sAxis < r) {
		
			rtn.b = sAxis,
			rtn.a = r;
		
		} else {

			rtn.a = sAxis,
			rtn.b = r;
		
		}
		rtn.c = Math.sqrt(Math.pow(rtn.a,2) - Math.pow(rtn.b,2));
		rtn.e = rtn.c/rtn.a;

		return rtn;

	},

	normalizeVal: function(val){

		val < 0 ? val *= -1: false;
		return val;
		
	},

	position: function(r,angle,plane,eIn){

		var rtn = {
				x:0,
				y:0,
				z:0
			},
			theta = this.radian(angle);

		if (typeof eIn === 'undefined') {

			eIn = 1;

		}

		var i = r * Math.sin(this.radian(plane)), // inclination at this plane point
			unp = this.unpackEllipse(r,eIn); // use Unpack function

		rtn.x = unp.c + unp.a * Math.sin(theta),
		rtn.z = unp.b * Math.cos(theta);
		rtn.y = i * Math.cos(theta);

		var dX = this.normalizeVal(rtn.x) + unp.c,
			dZ = this.normalizeVal(rtn.z);

		rtn.d = Math.sqrt(Math.pow(dX,2) + Math.pow(dZ,2));

		return rtn;

	},

    accelerateObj: function(G, distance, mass) {

        return G * mass / (Math.pow(distance, 2));

    },

	realPosition: function(data,pos) {

		var rtn = {
			x:pos.x,
			y:pos.y,
			z:pos.z
		},
		newPosition = this.position(data.distance,data.incl,data.plane);

		rtn.x = newPosition.x + pos.x,
		rtn.y = newPosition.y + pos.y,
		rtn.z = newPosition.z + pos.z;

		return rtn;
	},

	spherePosition: function(r,incl,plane) {

		try {

			var rtn = {
					x:0,
					y:0,
					z:0
				},
				theta = this.radian(incl);
				phi = this.radian(90-plane);

			rtn.x = r * Math.sin(theta) * Math.sin(phi),
			rtn.z = r * Math.cos(theta) * Math.sin(phi),
			rtn.y = r * Math.cos(phi);

		} catch (err) {

			rtn.err = err;

		}

		return rtn;

	}

});