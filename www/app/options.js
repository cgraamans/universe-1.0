define({

	// SOCKET.IO
	service: 'http://api.universe', // Location of the Socket.IO service
	
	//HTTP Routes for images etc.
	httpRoutes:{
		"skybox":"/assets/textures/skybox/",
		"stars":"/assets/textures/stars/",
		"planetoid":"/assets/textures/nodes/",
		"atmospherics":"/assets/textures/atmospheres/",
		"flares":"/assets/textures/flares/",
	},
	scale:{
		
		G: 6.67384e-11,

		"stars":{
			size:2,
			distance:10000
		},
		"nodes":{
			size:0.5,
			distance:100,
			rotation:0.02,
			inclination:0.01,
			velocity:1,
			asteroids:30,
			debris:10
		},
		"subnodes": {
			size:0.5,
			distance:1,
			rotation:0.02,
			inclination:0.01,
			velocity:1,
			asteroids:1,
			debris:0.3
		}
	},
	interface: {
		"timers":{
			messages:500
		}
	},

	// ROUTING
	defaultRoute: '/home', // Default location
	routes: [

		{
			name:'/home',
			AMDcall: {
	            templateUrl: '/app/views/home.html',
	            controller: 'HomeController',
	            controllerUrl: '/app/controllers/home'
	        }
		
		},

		// // THREEJS View
		// {
		// 	name:'/v/:cluster',
		// 	AMDcall: {
	 //            templateUrl: '/app/views/starbox.html',
	 //            controller: 'HomeController',
	 //            controllerUrl: '/app/controllers/starbox'
	 //        }
		
		// },

		// THREEJS View
		{
			name:'/v/:star/:node?',
			AMDcall: {
	            templateUrl: '/app/views/starbox.html',
	            controller: 'StarBoxController',
	            controllerUrl: '/app/controllers/starbox',
	        }
		
		},

		// User
		{
			name:'/u/:name',
			AMDcall: {
	            templateUrl: '/app/views/info.html',
	            controller: 'HomeController',
	            controllerUrl: '/app/controllers/home'
	        }
		
		},

		// Storyboard
		{
			name:'/b/:cluster',
			AMDcall: {
	            templateUrl: '/app/views/info.html',
	            controller: 'HomeController',
	            controllerUrl: '/app/controllers/home'
	        }
		
		},

		// Storyboard
		{
			name:'/b/:cluster/:star',
			AMDcall: {
	            templateUrl: '/app/views/info.html',
	            controller: 'HomeController',
	            controllerUrl: '/app/controllers/home'
	        }
		
		},


	]

});