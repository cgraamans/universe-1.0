require.config({
    baseUrl: "/app",
    map: {
      '*': {
        'css': '/node_modules/require-css/css.min'
      }
    },
    paths: {

        "opt":"/app/options",

        "socket": "/node_modules/socket.io-client/socket.io",
        "btford-socket":"/node_modules/angular-socket-io/socket.min",
        "angular": "/node_modules/angular/angular.min",
        "ngRoute":"/node_modules/angular-route/angular-route.min",
        "angularAMD": "/node_modules/angular-amd/angularAMD.min",
        "ngload": "/node_modules/angular-amd/ngload.min",
        "ngCookies":"/node_modules/angular-cookies/angular-cookies.min",
        "ngAnimate":"/node_modules/angular-animate/angular-animate.min",
        
        "threejs":"/node_modules/three/build/three.min",
        "orbitControls":"/vendors/threejs/OrbitControls",
        "tweenjs":"/node_modules/tween.js/src/Tween",

        "token":"/app/modules/factories/token",
        "factoryStarMaker":"/app/modules/factories/starMaker",
        "factoryRenderer":"/app/modules/factories/renderer",
        "factoryNodeMaker":"/app/modules/factories/nodeMaker",

        "raycaster":"/app/modules/directives/raycaster",
        "renderbox":"/app/modules/directives/renderbox",
        "draggable":"/app/modules/directives/draggable",
        "topnavmenu":"/app/modules/directives/topnavmenu",
        
        "jquery": "/node_modules/jquery/dist/jquery.min.js",

    },
    shim: {
        "angularAMD": ["angular"],
        "ngload": ["angularAMD"],
        "ngRoute":["angular"],
        "ngCookies":["angular"],
        "btford-socket":["socket","angular"],
        "socket": { exports: "io" },
        "threejs":["angularAMD"],
        "orbitControls":{
            deps:["angular","threejs"],
            exports:"orbitControls"
        },
        "tweenjs":["threejs"],
        "renderbox":["angular","threejs"],
        "ngAnimate": { deps: ['angularAMD'],exports:"ngAnimate" },
    },
    deps: ["app"]
});